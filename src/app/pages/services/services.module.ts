import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicesListComponent } from './services-list/services-list.component';
import { AddEditServicesComponent } from './add-edit-services/add-edit-services.component';
import { ServicesRoutingModule } from './services.routing.module.';
import { ConfigurationsModule } from '../configurations/configurations.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTableModule, MatProgressSpinnerModule, MatPaginatorModule, MatCardModule, MatToolbarModule, MatRadioModule, MatDialogModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from "@angular/material";
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material';
import { CKEditorModule } from 'ckeditor4-angular';


@NgModule({
  imports: [
    CommonModule,
    ConfigurationsModule,
    FormsModule,
    MatRadioModule,
    NgbModule,
    MatTableModule,
    MatToolbarModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatCardModule,
    MatPaginatorModule,
    ServicesRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatChipsModule,
    MatDialogModule,
    CKEditorModule
  ],
  declarations: [ServicesListComponent, AddEditServicesComponent],

})
export class ServicesModule { }
