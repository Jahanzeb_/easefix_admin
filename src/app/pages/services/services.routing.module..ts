import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServicesListComponent } from './services-list/services-list.component';
import { AddEditServicesComponent } from './add-edit-services/add-edit-services.component';
import { ConfigurationsHeaderComponent } from '../configurations/configurations-header/configurations-header.component';
import { RouterGuard } from '../../guards/route.guard';

const routes: Routes = [{
  path: '',
  component: ConfigurationsHeaderComponent,
  children: [
    {
      path: 'add',
      component: AddEditServicesComponent,
      canActivate: [RouterGuard],
      data: {
        type: 'add',
        configurations: 1,
        configurationsView: 2
      }
    },
    {
      path: 'edit/:id',
      component: AddEditServicesComponent,
      canActivate: [RouterGuard],
      data: {
        type: 'edit',
        configurations: 1,
        configurationsView: 2
      }
    },
    {
      path: '',
      pathMatch: 'full',
      canActivate: [RouterGuard],
      data: { configurations: 1 },
      component: ServicesListComponent
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicesRoutingModule { }
