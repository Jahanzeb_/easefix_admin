import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  OnDestroy,
} from "@angular/core";
import { of as observableOf, Subscription } from "rxjs";
import { startWith, switchMap, map, catchError } from "rxjs/operators";
import { HttpService } from "src/app/services/http-service";
import { Globals } from "src/app/Globals";
import { MatTableDataSource, MatPaginator } from "@angular/material";
import { HttpParams } from "@angular/common/http";

@Component({
  selector: "app-services-list",
  templateUrl: "./services-list.component.html",
  styleUrls: ["./services-list.component.scss"],
})
export class ServicesListComponent implements OnInit, AfterViewInit, OnDestroy {
  globals = Globals;

  displayedColumns = [];
  resultsLength: number = 10;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  // dataSource = new MatTableDataSource<serviceInterface>([])
  dataSource: any = [];

  service$: Subscription = null;

  isLoadingResults: boolean = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public service: HttpService) {
    this.displayedColumns = [
      "number",
      "icon",
      "serviceName",
      "serviceDescription",
      "order",
      "isArchive",
      "action",
    ];
  }
  localStorage: any;

  ngOnInit() {
    if (JSON.parse(localStorage.getItem("TradeMen-admin-data"))) {
      this.localStorage = JSON.parse(
        localStorage.getItem("TradeMen-admin-data")
      );
    }
  }

  ngAfterViewInit() {
    this.paginator.pageIndex = 0;
    this.serviceList();
  }

  serviceList() {
    (this.service$ = this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {
          let offset =
            (this.paginator.pageIndex + 1) * this.paginator.pageSize -
            this.paginator.pageSize;
          let limit = this.paginator.pageSize,
            // let  params = new HttpParams().set('offset', (((this.paginator.pageIndex + 1) * 10) - 10).toString());
            params = {
              limit,
              offset,
            };
          return this.service.getRequest(
            this.globals.urls.settings.services.list,
            params
          );
        }),
        map((res) => {
          if (res.response === 200) {
            this.isLoadingResults = false;
            this.resultsLength = res.data.servicesCount;
            return res.data;
          }
        }),
        catchError(() => {
          this.isLoadingResults = false;
          return observableOf([]);
        })
      )
      .subscribe(
        (data) => {
          if (data) {
            this.dataSource = data.servicesList;
          } else this.dataSource = new MatTableDataSource([]);
          return this.dataSource;
        },
        (err) => {
          this.service.showError(err.error.message);
        }
      )),
      (this.dataSource.paginator = this.paginator);
  }

  deleteService(id) {
    let url: string = this.globals.urls.settings.services.delete,
      params: any = { serviceId: id },
      request: any = this.service.postRequest(url, params);

    this.isLoadingResults = true;

    request.subscribe(
      (res) => {
        if (res.response === 200) {
          this.service.showSuccess("Service deleted successfully", "Service");
          this.serviceList();
          this.isLoadingResults = false;
        } else if (res.response == 400) {
          this.service.showError(res["message"], "Service");
          this.isLoadingResults = false;
        }
      },
      (err) => {
        this.service.showError(err.error.message);
        this.isLoadingResults = false;
      }
    );
  }

  ngOnDestroy(): void {
    if (this.service$) this.service$.unsubscribe();
  }
}

export interface serviceInterface {
  [index: number]: {
    number: number;
    serviceImage: string;
    serviceName: string;
    serviceDescription: string;
    isArchive: string;
  };
}
