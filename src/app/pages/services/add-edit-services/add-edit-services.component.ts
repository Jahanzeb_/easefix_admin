import { Component, OnInit, ViewChild } from "@angular/core";
import { Subscription } from "rxjs";
import { Globals, checkIfOnlySpaces } from "src/app/Globals";
import { HttpService } from "src/app/services/http-service";
import { Router, ActivatedRoute } from "@angular/router";
import { NgForm } from "@angular/forms";
import { MatChipInputEvent } from "@angular/material/chips";
import { COMMA, ENTER } from "@angular/cdk/keycodes";
import { MatDialog, MatTableDataSource } from "@angular/material";


export interface SubCategory {
  name: string;
}

@Component({
  selector: "app-add-edit-services",
  templateUrl: "./add-edit-services.component.html",
  styleUrls: ["./add-edit-services.component.scss"],
})
export class AddEditServicesComponent implements OnInit {
  content = '';
  globals = Globals;

  model: any = {};
  type: any;

  service$: Subscription = null;

  typeOfForm: string = "Add New";

  uploadingImage: boolean = false;
  isLoadingResults: boolean = false;

  fileToUpload: File = null;

  subServices: any = [];
  @ViewChild("addEditVt") form;

  addOnBlur = true;
  // readonly separatorKeysCodes = [ENTER, COMMA] as const;
  separatorKeysCodes = [ENTER, COMMA];
  subCategories: any = [];

  add(event: MatChipInputEvent): void {
    const value = (event.value || "").trim();

    if (value) {
      this.subCategories.push(value);
    }
    console.log(this.subCategories);
    // Clear the input value
    event.input.value = "";
  }

  remove(subCategory): void {
    const index = this.subCategories.indexOf(subCategory);

    if (index >= 0) {
      this.subCategories.splice(index, 1);
    }
  }

  constructor(
    private service: HttpService,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog

  ) {
    this.model.status = "1"; //active : 1
  }

  ngOnInit() {
    this.type = this.route.snapshot.data["type"];

    if (this.type === "add") {
      this.typeOfForm = "Add";
    } else if (this.type === "edit") {
      this.typeOfForm = "Edit";
      this.fetchService();
    }
  }

  addService(form: NgForm) {
    let url: string,
      request: any,
      params: any = {
        serviceName: this.model.serviceName,
        isArchive: parseInt(this.model.status) === 1 ? false : true,
        serviceImage: this.model.serviceImage,
        dashboardImage: this.model.dashboardImage,
        serviceDescription: this.model.serviceDescription,
        serviceNameEstonian: this.model.serviceNameET,
        serviceDescriptionEstonian: this.model.serviceDescriptionET,
        serviceNameRussian: this.model.serviceNameRU,
        serviceDescriptionRussian: this.model.serviceDescriptionRU,
        subCategories: this.subCategories || [],
        order: parseInt(this.model.order),
        longDescription: this.model.longDescription || ''
      };

    if (this.type === "add") {
      url = this.globals.urls.settings.services.add;
      request = this.service.postRequest(url, params);
    } else if (this.type === "edit") {
      url = this.globals.urls.settings.services.update;
      params.serviceId = this.route.snapshot.paramMap.get("id");
      params.slug = this.model.slug;
      request = this.service.putRequest(url, params);
    }

    this.isLoadingResults = true;

    request.subscribe(
      (res) => {
        if (res.response === 200) {
          this.isLoadingResults = false;

          if (this.type === "add") {
            form.resetForm();
            form.controls["status"].setValue("1");

            this.model.serviceImage = "";
            this.model.dashboardImage = "";
            this.service.showSuccess("Service Successfully Created", "Service");
          } else if (this.type === "edit") {
            this.service.showSuccess(
              "Service updated successfully.",
              "Service"
            );
            this.fetchService();
          }
          this.router.navigate(["services"]);
        } else if (res.response == 400) {
          this.isLoadingResults = false;
          this.service.showError(res["message"], "Service");
        }
      },
      (err) => {
        this.isLoadingResults = false;
        this.service.showError(err);
      }
    );
  }

  fetchService() {
    let url: any = this.globals.urls.settings.services.get,
      request: any = this.service.getRequest(
        url + "/" + this.route.snapshot.paramMap.get("id")
      );

    this.isLoadingResults = true;

    this.service$ = request.subscribe(
      (res) => {
        if (res.response === 200) {
          this.model = res.data;
          this.model.serviceNameET = res.data.serviceNameEstonian;
          this.subCategories = this.model.subCategories || [];
          (this.model.serviceDescriptionET =
            res.data.serviceDescriptionEstonian),
            (this.model.serviceNameRU = res.data.serviceNameRussian),
            (this.model.serviceDescriptionRU =
              res.data.serviceDescriptionRussian),
            (this.model.status = !res.data.isArchive ? "1" : "0");
        } else if (res.response == 400) {
          this.service.showError(res["message"], "Service");
        }

        this.isLoadingResults = false;
      },
      (err) => {
        this.service.showError(err.error.message);
        this.isLoadingResults = false;
      }
    );
  }

  uploadImage(files: FileList, type) {
    if (files[0].type === "image/gif") {
      this.service.showError("Gif images are not allowed", "Profile details");
      return;
    }

    this.uploadingImage = true;
    this.fileToUpload = files.item(0);

    this.isLoadingResults = true;

    this.service
      .uploadImage(this.globals.urls.uploadImage, this.fileToUpload)
      .subscribe(
        (res) => {
          if (type === 1) {
            this.model.serviceImage = res.data.url;
          } else {
            this.model.dashboardImage = res.data.url;
          }

          this.uploadingImage = false;
          this.isLoadingResults = false;
        },
        (err) => {
          this.service.showError(err.error.message);
          this.isLoadingResults = false;
        }
      );
  }

  public checkIfOnlySpaces(control: string) {
    return checkIfOnlySpaces(this.form, control);
  }

  ngOnDestroy() {
    if (this.service$) this.service$.unsubscribe();
  }

}
