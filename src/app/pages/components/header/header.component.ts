import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http-service';
import { StorageService } from '../../../services/localStorage.service';
import { Globals } from '../../../Globals';
import { of as observableOf, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { TimerObservable } from "rxjs/observable/TimerObservable";
import 'rxjs/add/operator/takeWhile';
import * as moment from 'moment';
// import { Socket } from 'ngx-socket-io';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  // Public Functions
  public globals = Globals;

  // store the logged in user credentials
  loggedInUser: any;
  userData: any;
  token:any;
  notificationsData: any; // notifications data
  routGurd:string;

  // Class Subscription
  notifications$: Subscription = null;
  httpSub$: Subscription = null;

  // number
  resultsLength: number = 0;
  activeTab:string
  activateRoute:boolean=false;
  isNavbarCollapsed=true;

  doNotShow:boolean=false;
  hideTooltip:boolean=false;
  isReadBit:boolean=false;
  // boolean
  // alive: boolean; // used to unsubscribe from the TimerObservable // when OnDestroy is called.
  // interval: number

  constructor(public storageService: StorageService,public service: HttpService,private router: Router, private route: ActivatedRoute) {
    // this.alive = true;
    // this.interval = 40000; // every 40 seconds
    this.storageService.changes.subscribe(res => {
      this.activeTab = this.storageService.getItem('activeTab');
      // if(activeTab == 'Packages') {
      //   this.activeTab = true;;
      // }
    });
  }

  ngOnInit() {
    this.loggedInUser = JSON.parse(localStorage.getItem('TradeMen-admin-data'));
    // this.userData = JSON.parse(sessionStorage.getItem('Aha-User-data'));
    // this.token=JSON.parse(localStorage.getItem('token'));
    // this.routGurd=this.userData.userType;
    if(this.userData && this.token){
      this.activateRoute=true;
    }
    else{
      this.activateRoute=false;
    }
    // Get notifications after every 40 seconds
    // TimerObservable.create(0, this.interval)
    //   .takeWhile(() => this.alive)
    //   .subscribe(() => {
    //     this.getNotifications()
    //   });

  }
  reRoute(){
    let userData = localStorage.getItem('TradeMen-admin-data');
    let redirectAdmin = '';
    if (userData) {
      let adminData = JSON.parse(userData);
      if (adminData.dashboard === 1) {
        redirectAdmin = 'map';
        this.router.navigate(['/map']);
      }
      if (adminData.map === 1) {
        redirectAdmin = 'workers';
        this.router.navigate(['/workers']);
        console.log('workers', redirectAdmin);
      }
      if (adminData.workers === 1) {
        redirectAdmin = 'users';
        this.router.navigate(['/users']);
        console.log('users', redirectAdmin);
      }
      if (adminData.users === 1) {
        redirectAdmin = 'jobs';
        this.router.navigate(['/jobs']);
        console.log('jobs', redirectAdmin);
      }
      if (adminData.jobs === 1) {
        redirectAdmin = 'profile';
        this.router.navigate(['/profile']);
        console.log('profile', redirectAdmin);
      }
      else {
        redirectAdmin = 'dashboard';
        this.router.navigate(['/dashboard']);
        console.log('dashboard', redirectAdmin);
      }
    }
  }
  logOut(){
    this.httpSub$ = this.service.getRequest(this.globals.urls.authentication.logout)
    .subscribe(
      res => {
        sessionStorage.clear();
        localStorage.clear();
        this.service.showSuccess(res['message'],'LogOut')
        this.router.navigate(['/login']);
      },
      err => this.service.showError(err['message']),
    );
    // this.router.navigate(['/login']);
  }

  // get header notifications
  getNotifications() {
    // // this.socket.emit("message", msg);
    // let url: string = this.globals.urls.notification.getNotifications;
    // url = url.replace(':isRead', this.isReadBit.toString());
    // this.notifications$ = this.service.getRequest(url)
    //   .pipe(
    //     map(res => {
    //       return res.data;
    //     }),
    // ).subscribe(
    //   data => {
    //     this.resultsLength = data.count;
    //     this.notificationsData = data['notifications'];
    //   },
    //   err => {
    //     // this.service.showError(err.error.message);
    //   }
    // );
  }
  isRead(){
    this.isReadBit=true;
  }
  goto(notification){
    if(notification.redirection==1){
      this.router.navigate(['/packages']);
    }
    else{
      this.router.navigate(['/trips/active']);
    }
  }

  // socketNotify() {
  //   this.socket.on("packageAcceptedAgainstPickupFranchise", res => {
  //     this.getNotifications();
  //     // this.service.showSuccess(res.socket.resource.text,'');
  //   });
  //   this.socket.on("packageAcceptedAgainstDropoffFranchis", res => {
  //     this.getNotifications();
  //     // this.service.showSuccess(res.socket.resource.text,'');
  //   });
  //   // this.socket.on("takerRejectedOffer", res => {
  //   //   this.getNotifications();
  //   //   // this.service.showSuccess(res.socket.resource.text,'');
  //   // });
  //   // this.socket.on("franchiserReceivedPackage", res => {
  //   //   this.getNotifications();
  //   //   // this.service.showSuccess(res.socket.resource.text,'');
  //   // });
  //   // this.socket.on("deliveryInProgress", res => {
  //   //   this.getNotifications();
  //   //   // this.service.showSuccess(res.socket.resource.text,'');
  //   // });
  //   // this.socket.on("packageDeliveredAtFranchise", res => {
  //   //   this.getNotifications();
  //   //   // this.service.showSuccess(res.socket.resource.text,'');
  //   // });
  //   // this.socket.on("packageDelivered", res => {
  //   //   this.getNotifications();
  //   //   // this.service.showSuccess(res.socket.resource.text,'');
  //   // });
  // }
  // ng on destroy
  ngOnDestroy(): void {
    if (this.notifications$) this.notifications$.unsubscribe();
    // this.alive = false;
  }

}
