import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Globals } from "src/app/Globals";
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import { TimerObservable } from "rxjs/observable/TimerObservable";
import { HttpService } from "src/app/services/http-service";
import { map } from "rxjs/internal/operators/map";
import { MatDialog } from "@angular/material";
// import { MapPopupComponent } from "../../modals/map-popup/map-popup.component";
import { MapDialogComponent } from "../map-dialog/map-dialog.component";

@Component({
  selector: "app-workers-map",
  templateUrl: "./workers-map.component.html",
  styleUrls: ["./workers-map.component.scss"],
})
export class WorkersMapComponent implements OnInit {
  globals = Globals;

  model: any = {};
  routeType: any = 1;

  // Maps
  origin: any = { lat: 33.550391595657814, lng: 73.1233336776495 };
  destination: any = { lat: 33.56015003764716, lng: 73.11544496566057 };

  zoom = 15;
  lat: number;
  lng: number;
  marker: string = "../.../../../../assets/images/icon_dropoff_location.png";

  // Dropdowns
  serviceTypes: any;

  acInWorkers: number = 1;
  countWorkers: any;
  protected map: any;
  isLoadingResults: boolean = true;
  activeWorkers$: Subscription = null;
  workersList: any;
  typeOfList: string = "active"; // show workers list by default

  private alive: boolean; // used to unsubscribe from the TimerObservable // when OnDestroy is called.
  private interval: number;

  jobFilterText = { serviceName: "Service", value: "", _id: "" };
  jobType: any = [];


  @ViewChild("total") total?: ElementRef<HTMLDivElement>;

  constructor(
    public service: HttpService,
    public router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.setCurrentPos();

    // this.route.data.subscribe(data => {
    //   if(data['resolvedData']) {
    //     this.countWorkers = data['resolvedData'].data
    //   }
    // });

    // console.log(this.router.url);
    if (this.router.url == "/map/inactive") {
      this.routeType = 2;
    } else if (this.router.url == "/map/active") {
      this.routeType = 1;
    } else if (this.router.url == "/map/offline") {
      this.routeType = 3;
    } else if (this.router.url == "/map/worker") {
      this.routeType = 4;
    }
    this.alive = true;
    this.interval = 40000; // every 40 seconds
  }
  public mapReady(map) {
    this.map = map;
  }

  ngOnInit() {
    this.serviceTypes = [
      { name: "Waiting for Bids", id: 1 },
      { name: "Waiting for etc", id: 2 },
    ];

    // this.setCurrentPos();

    // Get active job drivers list after every 40 seconds
    TimerObservable.create(this.interval, this.interval)
      .takeWhile(() => this.alive)
      .subscribe(() => {
        this.getWorkersList();
      });

    this.getJobTypes();
  }

  getWorkersList() {
    console.log('sdsadsadsad', this.typeOfList, this.routeType)
    this.isLoadingResults = true;
    let url = "";

    switch (this.routeType) {
      case 1: {
        url = this.globals.urls.map.activeJobs;
        break;
      }
      case 2: {
        url = this.globals.urls.map.onlineSp;
        break;
      }
      case 3: {
        url = this.globals.urls.map.offlineSp;
        break;
      }
      case 4: {
        url = this.globals.urls.map.workers;
        break;
      }
      default: {
        break;
      }
    }

    let params = { latitude: this.lat, longitude: this.lng, serviceId: this.jobFilterText._id },
      serviceType =
        this.routeType === 1
          ? this.service.postRequest(url, params)
          : this.service.getRequest(url, params);
    if (this.routeType === 4) {
      serviceType = this.service.getRequest(url, params)
    }

    console.log('params---', params);
    this.activeWorkers$ = serviceType
      .pipe(
        map((res) => {
          return res.data;
        })
      )
      .subscribe(
        (data) => {
          this.isLoadingResults = false;
          this.workersList = data["foundSpList"] || data;

          console.log('asjndkjkladsjnadls', this.workersList);
          if (this.workersList && this.workersList.length > 0) {
            this.workersList.forEach((worker) => {
              // if (this.typeOfList === 'active') {
              //   worker.isSpOnline = true;
              // }
              if (this.routeType === 1) {
                worker.isSpOnline = true;
              }
              worker.isClicked = false;
              worker.image = '../.../../../../assets/images/Offline.png';
              if (worker.isSpOnline) {
                worker.image = "../.../../../../assets/images/Online.png";

              }
              // worker.image = "../.../../../../assets/images/mapIcon.png";
            });
          }
        },
        (err) => {
          this.workersList = [];
          this.isLoadingResults = false;
        }
      );
  }

  showDriverInfo(item, indexI) {
    // this.isInfoWindowOpen = true;

    this.workersList.forEach((packages) => {
      if (packages._id == item._id) {
        packages.isClicked = true;
        // packages.image = "../.../../../../assets/images/mapIconSelected.png";
        packages.image = packages.isSpOnline ? "../.../../../../assets/images/GreenSelected.png" : "../.../../../../assets/images/RedSelected.png"

      } else {
        packages.isClicked = false;
        packages.image = packages.isSpOnline ? "../.../../../../assets/images/Online.png" : "../.../../../../assets/images/Offline.png"
        // packages.image = "../.../../../../assets/images/mapIcon.png";
      }
    });
    this.zoom = 20;
    this.map.panTo({
      lat: this.workersList[indexI].latitude,
      lng: this.workersList[indexI].longitude,
    });
  }

  getWorkersCount(lat, lng) {
    this.service
      .getRequest(Globals.urls.map.count + "?lat=" + lat + "&lng=" + lng, {})
      .pipe(map((res) => res.data))
      .subscribe(
        (data) => {
          this.countWorkers = data;
        },
        (err) => { }
      );
  }

  setCurrentPos() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.getWorkersCount(this.lat, this.lng);
        this.getWorkersList();
      });
    }
  }

  changeType(type) {
    this.routeType = type;
    this.acInWorkers = type;
  }

  viewJob(id) {
    this.router.navigate(["/jobs/job-detail/" + id, "workerMaps"]);
  }

  ngOnDestroy(): void {
    if (this.activeWorkers$) this.activeWorkers$.unsubscribe();
    this.alive = false;
  }

  markerClicked(e, data, directive: HTMLElement) {
    let spFound = {};
    this.workersList.forEach((packages) => {
      if (packages._id == data._id) {
        spFound = packages;
        packages.isClicked = true;
        packages.image = packages.isSpOnline ? "../.../../../../assets/images/GreenSelected.png" : "../.../../../../assets/images/RedSelected.png"
        // packages.image = "../.../../../../assets/images/mapIconSelected.png";
      } else {
        packages.isClicked = false;
        packages.image = packages.isSpOnline ? "../.../../../../assets/images/Online.png" : "../.../../../../assets/images/Offline.png"
      }
    });

    console.log(spFound);
    let dataToSend = {
      name: spFound["name"],
      profileImage: spFound["profileImage"],
      status: spFound["isSpOnline"] ? "Online" : "offline",
      service: spFound["service"] || "",
      jobId: spFound["JobIdIdentifier"] || "",
      distance: spFound["distanceAway"] || "",
      routeType: this.routeType,
      rating: spFound["avgRating"] || 5,
      phoneNumber: '0044' + spFound["phoneNumber"]
    };

    if (this.routeType == 4) {
      dataToSend.name = spFound['companyName'];
    }
    let dialogRef = this.dialog.open(MapDialogComponent, {
      // width: "400px",
      data: dataToSend
    });
    // console.log(dialogRef)
    dialogRef.afterClosed().subscribe((result) => {
      this.workersList.forEach((packages) => {
        if (packages._id == data._id) {
          packages.isClicked = false;
          packages.image = packages.isSpOnline ? "../.../../../../assets/images/Online.png" : "../.../../../../assets/images/Offline.png"
        }
      });
    });
  }

  getJobTypes() {
    let url = this.globals.urls.TradeAdmin.workers.fetchJobTypes

    this.activeWorkers$ = this.service
      .getRequest(url)
      .subscribe(
        (res) => {
          this.isLoadingResults = false;
          // this.jobType = res.data.servicesList;

          this.jobType = res.data.servicesList.filter(element => { return element.isArchive == false });

        },
        (err) => {
          this.isLoadingResults = false;
          this.service.showError(err);
        }
      );
  }

  filter(job) {
    this.jobFilterText = job;
    this.getWorkersList();
  }


}
