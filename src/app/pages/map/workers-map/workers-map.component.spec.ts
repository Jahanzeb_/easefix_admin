import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkersMapComponent } from './workers-map.component';

describe('WorkersMapComponent', () => {
  let component: WorkersMapComponent;
  let fixture: ComponentFixture<WorkersMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkersMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkersMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
