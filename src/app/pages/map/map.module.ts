import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MapRoutingModule } from './map-routing.module';
import { WorkersMapComponent } from './workers-map/workers-map.component';
import { AgmCoreModule } from '@agm/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { MapPopupComponent } from '../modals/map-popup/map-popup.component';
import { MapDialogComponent } from './map-dialog/map-dialog.component';
import { MatDialogModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material';



@NgModule({
  imports: [
    CommonModule,
    AgmCoreModule,
    NgbModule,
    MapRoutingModule,
    MatDialogModule,
    MatProgressSpinnerModule
  ],
  declarations: [WorkersMapComponent, MapDialogComponent],
  entryComponents: [MapDialogComponent]
})
export class MapModule { }
