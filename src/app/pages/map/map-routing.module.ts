import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkersMapComponent } from './workers-map/workers-map.component';
import { Globals } from 'src/app/Globals';
import { RouteResolverService } from 'src/app/services/route-resolver.service';
import { RouterGuard } from '../../guards/route.guard';

const routes: Routes = [{
  path: '',
  canActivate: [RouterGuard],
  data: { map: 1 },
  children: [
    {
      path: 'active',
      component: WorkersMapComponent,
      // resolve: {
      //   resolvedData: RouteResolverService
      // },
      // data: { apiUrl: Globals.urls.map.count, methodType: 'GET', type: 'active', getCurrentLocation: true }
    },
    {
      path: 'inactive',
      component: WorkersMapComponent,
      resolve: {
        resolvedData: RouteResolverService
      },
      data: { apiUrl: Globals.urls.map.count, methodType: 'GET', type: 'inactive' }
    },
    {
      path: 'offline',
      component: WorkersMapComponent,
      resolve: {
        resolvedData: RouteResolverService
      },
      data: { apiUrl: Globals.urls.map.count, methodType: 'GET', type: 'inactive' }
    },
    {
      path: 'worker',
      component: WorkersMapComponent,
      resolve: {
        resolvedData: RouteResolverService
      },
      data: { apiUrl: Globals.urls.map.count, methodType: 'GET', type: 'inactive' }
    },
    {
      path: '',
      pathMatch: 'full',
      redirectTo: 'active'
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MapRoutingModule { }
