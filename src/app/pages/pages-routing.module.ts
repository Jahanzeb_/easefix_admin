import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { AuthGuard } from '../guards/auth.guard';
import { RouterGuard } from '../guards/route.guard';
import { Router } from '@angular/router';

// let userData =  JSON.parse(localStorage.getItem('TradeMen-admin-data'));

let userData = localStorage.getItem('TradeMen-admin-data');
let redirectAdmin = '';
if (userData) {
  let adminData = JSON.parse(userData);
  if (adminData.dashboard === 1) {
    redirectAdmin = 'map';
    console.log('map', redirectAdmin);
  }
  if (adminData.map === 1) {
    redirectAdmin = 'workers';
    console.log('workers', redirectAdmin);
  }
  if (adminData.workers === 1) {
    redirectAdmin = 'users';
    console.log('users', redirectAdmin);
  }
  if (adminData.users === 1) {
    redirectAdmin = 'jobs';
    console.log('jobs', redirectAdmin);
  }
  if (adminData.jobs === 1) {
    redirectAdmin = 'profile';
    console.log('profile', redirectAdmin);
  }
  else {
    redirectAdmin = 'dashboard';
    console.log('dashboard', redirectAdmin);
  }
}
// else {
//   console.log('login')
//   window.location.href = '/login';
// }
const routes: Routes = [{
  path: '',
  component: PagesComponent,
  canActivate: [AuthGuard],
  children: [
    {
      path: '',
      redirectTo: redirectAdmin,
      pathMatch: 'full'
    },
    {
      path: 'dashboard',
      loadChildren: './dashboard/dashboard.module#DashboardModule',
      canActivate: [RouterGuard],
      data: { dashboard: 1 },
      // canActivate:[userData.dashboard == 10 || userData.dashboard == 4]
    },
    {
      path: 'map',
      loadChildren: './map/map.module#MapModule',
    },
    {
      path: 'workers',
      loadChildren: './workers/workers.module#WorkersModule',
      // canActivate: [RouteGuard],
      // data: {roles: 'viewDrivers'}
    },
    {
      path: 'users',
      loadChildren: './users/users.module#UsersModule',
      canActivate: [RouterGuard],
      data: { users: 1 },
    },
    {
      path: 'profile',
      loadChildren: './profile/profile.module#ProfileModule'
    },
    {
      path: 'promocodes',
      loadChildren: './promocodes/promocodes.module#PromocodesModule'
    },
    {
      path: 'notifications',
      loadChildren: './notifications/notifications.module#NotificationsModule'
    },
    {
      path: 'referral',
      loadChildren: './referral-settings/referral-settings.module#ReferralSettingsModule'
    },
    {
      path: 'services',
      loadChildren: './services/services.module#ServicesModule'
    },
    {
      path: 'commission',
      loadChildren: './commission/commission.module#CommissionModule'
    },
    {
      path: 'reasons',
      loadChildren: './reasons/reasons.module#ReasonsModule'
    },
    {
      path: 'service-charges',
      loadChildren: './service-charges/service-charges.module#ServiceChargesModule'
    },
    {
      path: 'roles-security',
      loadChildren: './roles-security/roles-security.module#RolesSecurityModule'
    },
    {
      path: 'banks',
      loadChildren: './banks/banks.module#BanksModule'
    },
    {
      path: 'payouts',
      loadChildren: './payouts/payouts.module#PayoutsModule'
    },
    {
      path: 'jobs',
      loadChildren: './jobs/jobs.module#JobsModule'
    },
    {
      path: 'terms-condition',
      loadChildren: './terms-condition/terms-condition.module#TermsConditionModule'
    },
    {
      path: 'privacy-policy',
      loadChildren: './privacy-policy/privacy-policy.module#PrivacyPolicyModule'
    },
    {
      path: 'logs',
      loadChildren: './loging/loging.module#LogingModule'
    },
    {
      path: '**',
      redirectTo: '/login',
    },

  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
