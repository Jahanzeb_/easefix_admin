import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { of as observableOf, Subscription } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { HttpService } from 'src/app/services/http-service';
import { Globals } from 'src/app/Globals';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-promocodes-list',
  templateUrl: './promocodes-list.component.html',
  styleUrls: ['./promocodes-list.component.scss']
})
export class PromocodesListComponent implements OnInit, AfterViewInit, OnDestroy {

  globals = Globals;

  displayedColumns = [];
  resultsLength: number = 10;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  // dataSource = new MatTableDataSource<promocodeInterface>([])
  // dataSource: MatTableDataSource<any>;
  dataSource:any=[];
  promocode$: Subscription = null;

  isLoadingResults: boolean = false;
  localStorage: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public service: HttpService) {
    this.displayedColumns = ['number', 'name', 'value', 'startDate', 'endDate', 'action'];
  }

  ngOnInit() { 
    if (JSON.parse(localStorage.getItem('TradeMen-admin-data'))) {
      this.localStorage = JSON.parse(localStorage.getItem('TradeMen-admin-data'))
  }
  }

  ngAfterViewInit() {
    this.paginator.pageIndex = 0;
    this.promocodeList();
  }

  promocodeList() {

    this.isLoadingResults = true;

    this.promocode$ = this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {
          let params = new HttpParams().set('offset', (((this.paginator.pageIndex + 1) * 10) - 10).toString());
          return this.service.getRequest(this.globals.urls.settings.promocodes.list, params);
        }),
        map(
          res => {
            if (res.response === 200) {

              this.isLoadingResults = false;
              this.resultsLength = res.data.count;
              return res.data.promoCodes;

            }
          }),
        catchError(() => {

          this.isLoadingResults = false;
          return observableOf([]);

        })
      ).subscribe(
        data => {
          if (data) {
            this.dataSource = data;
          }
          else this.dataSource = new MatTableDataSource([]);
          return this.dataSource
        },
        err => {
          this.service.showError(err.error.message);
        }),

      this.dataSource.paginator = this.paginator;
  }

  deleteService(id) {

    let url: string = this.globals.urls.settings.promocodes.delete,
      params: any = { status: 2, _id: id },
      request: any = this.service.putRequest(url, params);

    this.isLoadingResults = true;

    request.subscribe(
      res => {
        if (res.response === 200) {

          this.service.showSuccess('Promocode deleted successfully', 'Promocodes');
          this.promocodeList();
          this.isLoadingResults = false;

        }
        else if (res.response == 400) {
          this.service.showError(res['message'], 'Promocodes');
          this.isLoadingResults = false;

        }
      },
      err => {
        this.service.showError(err.error.message);
        this.isLoadingResults = false;

      },
    );
  }

  ngOnDestroy(): void {
    if (this.promocode$) this.promocode$.unsubscribe();
  }

}


export interface promocodeInterface {
  [index: number]: {
    number: number;
    promoCodeName: string;
    promoText: string;
    code: string;
    type: number;
    percentage: number;
    startDate: number;
    expiryDate: number;
  }
}