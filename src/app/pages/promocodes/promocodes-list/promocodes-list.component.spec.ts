import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromocodesListComponent } from './promocodes-list.component';

describe('PromocodesListComponent', () => {
  let component: PromocodesListComponent;
  let fixture: ComponentFixture<PromocodesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromocodesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromocodesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
