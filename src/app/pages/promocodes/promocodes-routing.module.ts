import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PromocodesListComponent } from './promocodes-list/promocodes-list.component';
import { AddEditPromocodeComponent } from './add-edit-promocode/add-edit-promocode.component';
import { RouterGuard } from '../../guards/route.guard';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'add',
      component: AddEditPromocodeComponent,
      canActivate: [RouterGuard],
      data: {
        type: 'add',
        promoCodes:1,
        promoCodesView:2,


      }
    },
    {
      path: 'edit/:id',
      component: AddEditPromocodeComponent,
      canActivate: [RouterGuard],
      data: {
        type: 'edit',
        promoCodes:1,
        promoCodesView:2,
      }
    },
    {
      path: '',
      pathMatch: 'full',
      component: PromocodesListComponent,
      canActivate: [RouterGuard],
      data: {
        promoCodes:1,
      }
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PromocodesRoutingModule { }
