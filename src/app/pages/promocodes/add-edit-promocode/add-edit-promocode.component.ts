import { Component, OnInit, ViewChild } from "@angular/core";
import { MatDatepicker } from "@angular/material/datepicker/typings/datepicker";
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import { Globals, checkIfOnlySpaces } from "src/app/Globals";
import { HttpService } from "src/app/services/http-service";
import { NgForm } from "@angular/forms";
import * as moment from "moment";

@Component({
  selector: "app-add-edit-promocode",
  templateUrl: "./add-edit-promocode.component.html",
  styleUrls: ["./add-edit-promocode.component.scss"],
})
export class AddEditPromocodeComponent implements OnInit {
  globals = Globals;

  todaydate: Date = new Date();
  model: any = {};
  type: any;

  promocode$: Subscription = null;

  typeOfForm: string = "Add New";

  uploadingImage: boolean = false;
  isLoadingResults: boolean = false;

  fileToUpload: File = null;

  promoTypes = [
    {
      name: "For New Signups",
      value: 1,
    },
    {
      name: "Frequecy of Jobs",
      value: 2,
    },
    {
      name: "In-Active Users",
      value: 3,
    },
    {
      name: "All Users",
      value: 4,
    },
  ];

  promoTextt = { name: "Voucher Type", value: 1 };
  @ViewChild("addEditPromocode") form;

  constructor(
    private router: Router,
    private service: HttpService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.type = this.route.snapshot.data["type"];

    if (this.type === "add") {
      this.typeOfForm = "Add";
    } else if (this.type === "edit") {
      this.typeOfForm = "Edit";
      this.fetchPromocode();
    }
  }

  savePromocode(form: NgForm) {
    let url: string,
      request: any,
      params: any = {
        _id: this.route.snapshot.paramMap.get("id"),
        code: this.model.code,
        promoText: this.model.promoText,
        promoCodeName: this.model.promoCodeName,
        startDate: moment(this.model.startDate).valueOf() / 1000,
        expiryDate: moment(this.model.expiryDate).valueOf() / 1000,
        percentage: this.model.percentage,
        type: this.promoTextt.value,
        // type: parseInt(this.model.type),
      };
    console.log(params);
    // return;
    if (this.type === "add") {
      url = this.globals.urls.settings.promocodes.add;
      request = this.service.postRequest(url, params);
    } else if (this.type === "edit") {
      url = this.globals.urls.settings.promocodes.update;
      params.serviceId = this.route.snapshot.paramMap.get("id");
      request = this.service.postRequest(url, params);
    }

    if (params.startDate > params.expiryDate) {
      this.service.showError(
        "Start Date must be less than End Date.",
        "PromoCode"
      );
      return;
    }

    this.isLoadingResults = true;

    request.subscribe(
      (res) => {
        if (res.response === 200) {
          this.isLoadingResults = false;

          if (this.type === "add") {
            form.resetForm();
            // form.controls["type"].setValue("1");

            this.model.serviceImage = "";
            this.service.showSuccess(
              "Promocode Successfully Created",
              "Promocode"
            );
          } else if (this.type === "edit") {
            this.service.showSuccess(
              "Promocode updated successfully.",
              "Promocode"
            );
            this.fetchPromocode();
          }

          this.router.navigate(["/promocodes"]);
        } else if (res.response == 400) {
          this.isLoadingResults = false;
          this.service.showError(res["message"], "Promocode");
        }
      },
      (err) => {
        this.isLoadingResults = false;
        // this.service.showError(err);
      }
    );
  }

  fetchPromocode() {
    let url: any = this.globals.urls.settings.promocodes.fetch,
      request: any = this.service.getRequest(
        url + "/" + this.route.snapshot.paramMap.get("id")
      );

    this.isLoadingResults = true;

    this.promocode$ = request.subscribe(
      (res) => {
        if (res.response === 200) {
          this.model = res.data.promoCode;
          this.model.startDate = new Date(this.model.startDate * 1000);
          this.model.expiryDate = new Date(this.model.expiryDate * 1000);
          if (this.model.type === 1) {
            this.promoTextt = {
              name: "For New Signups",
              value: 1,
            };
          } else if (this.model.type === 2) {
            this.promoTextt = {
              name: "Frequecy of Jobs",
              value: 2,
            };
          } else if (this.model.type === 3) {
            this.promoTextt = {
              name: "In-Active Users",
              value: 3,
            };
          } else if (this.model.type === 4) {
            this.promoTextt = {
              name: "All Users",
              value: 4,
            };
          }
        } else if (res.response == 400) {
          this.service.showError(res["message"], "Promocode");
        }

        this.isLoadingResults = false;
      },
      (err) => {
        this.service.showError(err.error.message);
        this.isLoadingResults = false;
      }
    );
  }

  public checkIfOnlySpaces(control: string) {
    return checkIfOnlySpaces(this.form, control);
  }

  _openCalendar(
    picker: MatDatepicker<Date>,
    bindCalendar: string = "startDate"
  ) {
    picker.open();
    if (bindCalendar == "startDate") {
      setTimeout(() => this.model.startDate);
    } else {
      setTimeout(() => this.model.endDate);
    }
  }

  _closeCalendar(e, bindCalendar: string = "endDate") {
    if (bindCalendar == "endDate") {
      setTimeout(() => this.model.startDate);
    } else {
      setTimeout(() => this.model.endDate);
    }
  }

  back() {
    this.router.navigate(["promocodes"]);
  }

  ngOnDestroy() {
    if (this.promocode$) this.promocode$.unsubscribe();
  }

  select(promo) {
    this.promoTextt = promo;
  }
}
