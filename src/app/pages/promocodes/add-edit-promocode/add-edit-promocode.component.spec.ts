import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditPromocodeComponent } from './add-edit-promocode.component';

describe('AddEditPromocodeComponent', () => {
  let component: AddEditPromocodeComponent;
  let fixture: ComponentFixture<AddEditPromocodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditPromocodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditPromocodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
