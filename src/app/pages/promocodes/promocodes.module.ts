import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddEditPromocodeComponent } from './add-edit-promocode/add-edit-promocode.component';
import { PromocodesRoutingModule } from './promocodes-routing.module';
import { PromocodesListComponent } from './promocodes-list/promocodes-list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatToolbarModule, MatProgressSpinnerModule, MatTableModule, MatPaginatorModule, MatCardModule, MatRadioModule, MatDatepickerModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { SharedPipesModule } from '../shared-pipes/shared-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    MatRadioModule,
    MatTableModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatCardModule,
    SharedPipesModule,
    PromocodesRoutingModule
  ],
  declarations: [AddEditPromocodeComponent, PromocodesListComponent]
})
export class PromocodesModule { }
