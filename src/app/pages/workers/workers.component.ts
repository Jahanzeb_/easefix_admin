import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-workers',
  template: `
  <router-outlet></router-outlet>
  `,
})
export class WorkersComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
