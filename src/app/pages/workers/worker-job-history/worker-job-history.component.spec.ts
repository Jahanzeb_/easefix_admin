import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerJobHistoryComponent } from './worker-job-history.component';

describe('WorkerJobHistoryComponent', () => {
  let component: WorkerJobHistoryComponent;
  let fixture: ComponentFixture<WorkerJobHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkerJobHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkerJobHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
