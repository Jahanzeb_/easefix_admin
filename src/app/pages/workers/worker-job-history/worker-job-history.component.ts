import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { of as observableOf, fromEvent, Subscription } from 'rxjs';
import { HttpService } from '../../../services/http-service';
import { Globals } from '../../../Globals';
import { map, switchMap, startWith, debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-worker-job-history',
  templateUrl: './worker-job-history.component.html',
  styleUrls: ['./worker-job-history.component.scss']
})
export class WorkerJobHistoryComponent implements OnInit {

  // Public Depedency
  public globals = Globals;

  // Table settings
  displayedColumns = [];
  // dataSource: MatTableDataSource<any>;
  dataSource: any = [];

  // Observables/Subscriptions
  private httpSub$: Subscription = null;
  previewData: any = {};

  // Type number variables
  resultsLength: number = 10;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  totalRecords: number = 0;
  packageType: number = 1;  // package type by default is active
  workerId: string; // driver id from url

  // boolean
  isLoadingResults = false;

  // Type any
  packagesData: any = [];
  localStorage: any;
  exportDataURL: string;
  GdprDData: any;
  showData: any = {
    mobile: false,
    email: false,
    IdImage: false,
  }
  showEye: any = {
    mobile: true,
    email: true,
    IdImage: true,
  }
  jobStatusText = [
    { name: 'All', value: '' },
    { name: 'Assigned', value: 2 },
    { name: 'InProgress', value: 3 },
    { name: 'Completed', value: 4 },
    // { name: 'Rated By User', value: 5 },
    { name: 'Cancellation', value: 6 },
    { name: 'Rejected', value: 7 },
    { name: 'Time Out', value: 8 },

  ];


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('input') input: ElementRef;

  constructor(public service: HttpService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    // this.localStorage = JSON.parse(localStorage.getItem('boon4-admin-data'));.
    if (JSON.parse(localStorage.getItem('TradeMen-admin-data'))) {
      this.localStorage = JSON.parse(localStorage.getItem('TradeMen-admin-data'))
  }
    this.workerId = this.route.snapshot.paramMap.get('workerId');
    this.displayedColumns = ['id', 'jobHours', 'user', 'jaddress', 'amount', 'date','status' ,'action'];
    this.exportUser();
  }

  ngAfterViewInit() {

    this.httpSub$ = fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.search({ searchText: this.input.nativeElement.value });
        })
      )
      .subscribe();

    this.getListing();
  }
  Enablegdpr(from) {
    this.isLoadingResults = true;
    if (from == 'mobile') {
      this.showData.mobile = true;
    }
    else if (from == 'email') {
      this.showData.email = true;
    }
    else if (from == 'IdImage') {
      this.showData.IdImage = true;
    }
  }

  GotGDPRDATA(event) {
    this.isLoadingResults = false;
    if (event.data.phoneNumber && event.data.phonePreFix) {
      this.packagesData.spProfile.phone = event.data.phonePreFix.toString() + event.data.phoneNumber.toString()
      this.showEye.mobile = false;
    }
    else if (event.data.email) {
      this.packagesData.spProfile.email = event.data.email
      this.showEye.email = false;
    }
  }
  
  search(input) {
    this.isLoadingResults = true;
    let offset = ((this.paginator.pageIndex + 1) * this.paginator.pageSize) - this.paginator.pageSize;
    let limit = this.paginator.pageSize;
    
    let url = this.globals.urls.TradeAdmin.workers.fetchSpJobList;
    url = url.replace(':limit', limit.toString());
    url = url.replace(':offset', offset.toString());
    url = url.replace(':workerId', this.workerId.toString());
    url = url.replace(':search_text', input.searchText || '');
    url = url.replace(':status', this.packageType.toString());
    this.service.getRequest(url)
      .pipe(
        map(res => {
          return res.data;
        })
      )
      .subscribe(
        data => {
          this.isLoadingResults = false;

          this.dataSource = data.jobsList;
          this.packagesData = data;
          this.totalRecords = data.count;
          this.resultsLength = data.jobsList.length;
        },
        err => {
          this.isLoadingResults = false;
          if (err.error)
            this.service.showError(err.error.message);
          else
            this.service.showError(err);
        },
    )
  }

  exportUser() {
    this.isLoadingResults = true;
    let params = {
      userProfileId: this.workerId,
      userType: 'sp'
    }
    this.httpSub$ = this.service.postRequest(this.globals.urls.getCSVJob, params)
      .subscribe(
        res => {
          this.isLoadingResults = false;
          this.exportDataURL = res.data.userFileUrl
        },
        err => {
          this.isLoadingResults = false;
          this.service.showError(err);
        }
      );
  }

  getListing() {
    this.isLoadingResults = true;
    this.httpSub$ = this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {

          let offset = ((this.paginator.pageIndex + 1) * this.paginator.pageSize) - this.paginator.pageSize;
          let limit = this.paginator.pageSize;
          let url = this.globals.urls.TradeAdmin.workers.fetchSpJobList;
          url = url.replace(':limit', limit.toString());
          url = url.replace(':offset', offset.toString());
          url = url.replace(':workerId', this.workerId.toString());
          url = url.replace(':search_text', '');
          url = url.replace(':status', this.packageType.toString());
          return this.service.getRequest(url);
        }),
        map(res => {
          return res.data;
        }),
    ).subscribe(
      data => {
        this.isLoadingResults = false;
        // this.previeDataFromServer = data
        data.jobsList.forEach((element, index) => {
          if (element.status == 1) {
            element.statusText = 'open'
          }
          else if (element.status == 2) {
            element.statusText = 'Assigned'
          }
          else if (element.status == 3) {
            element.statusText = 'InProgress'
          }
          else if (element.status == 4) {
            element.statusText = 'Completed'
          }
          else if (element.status == 5) {
            element.statusText = 'Rated By User'
          }
          else if (element.status == 6) {
            element.statusText = 'Cancelled'
          }
          else if (element.status == 7) {
            element.statusText = 'Rejected'
          }
          else if (element.status == 8) {
            element.statusText = 'Time Out'
          }
          else if (element.status == 9) {
            element.statusText = 'Discarded'
          }
        });
        

        this.dataSource = data.jobsList;
        this.packagesData = data;
        this.totalRecords = data.count;
        this.resultsLength = data.count;

      },
      err => {
        this.dataSource = null;
        this.isLoadingResults = false;
        this.service.showError(err.error.message);
      }
    );
  }

  listPackages(packageToShow) {
    if (packageToShow == 'active') {
      this.packageType = 1;
      this.paginator.pageSize = 10;
      this.paginator.pageIndex = 0;
      this.getListing();
    } else if (packageToShow == 'past') {
      this.packageType = 2;
      this.paginator.pageSize = 10;
      this.paginator.pageIndex = 0;
      this.getListing();
    }
  }

  ngOnDestroy(): void {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }

}
