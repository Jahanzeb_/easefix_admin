import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerPackageDetailComponent } from './worker-package-detail.component';

describe('WorkerPackageDetailComponent', () => {
  let component: WorkerPackageDetailComponent;
  let fixture: ComponentFixture<WorkerPackageDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkerPackageDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkerPackageDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
