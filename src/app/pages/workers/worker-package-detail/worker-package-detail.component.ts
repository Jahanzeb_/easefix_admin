import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpService } from 'src/app/services/http-service';
import { Globals } from '../../../Globals';
import { of as observableOf, Subscription } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-worker-package-detail',
  templateUrl: './worker-package-detail.component.html',
  styleUrls: ['./worker-package-detail.component.scss']
})
export class WorkerPackageDetailComponent implements OnInit {

  // Public Functions
  public globals = Globals;

  // variable type ANY
  model: any = {}
  locationTags: any = [];

  // type boolean
  isLoadingResults: boolean = true;

  // subscription observable
  packageDetail$: Subscription = null;

  // Type number
  jobId: string;
  workerId: string;

  // maps routes
  origin: any = {};
  destination: any = {};
  waypoints: Array<any> = [];

  // public renderOptions = {
  //   suppressMarkers: true,
  // }

  // public markerOptions = {
  //   origin: {
  //     icon: '../../../../assets/images/origin.png',
  //   },
  //   waypoints: {
  //     icon: '../../../../assets/images/dropoff.png',
  //   },
  //   destination: {
  //     icon: '../../../../assets/images/dropoff.png',
  //   },
  // };
  public renderOptions = {
    suppressMarkers: true,
  }

  public markerOptions = {
    origin: {
      icon: '../../../../assets/images/icon_dropoff_location.png',
    },
    destination: {
      icon: '../../../../assets/images/map_distance_pointer.png',
    },
  };
  GdprDData: any;
   showData: any = {
     mobileUser: false,
     emailUser: false,
     mobileWorker: false,
     emailWorker: false,
     IdImage: false,
   }
   showEye: any = {
     mobileUser: true,
     emailUser: true,
     mobileWorker: true,
     emailWorker: true,
     IdImage: true,
   }
   checkType:string;
  


  constructor(private route: ActivatedRoute, private router: Router, private httpService: HttpService) {

    this.locationTags = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
      'Aa', 'Ab', 'Ac', 'Ad', 'Ae', 'Af', 'Ag', 'Ah', 'Ai', 'Aj', 'Ak', 'Al', 'Am', 'An', 'Ao', 'Ap', 'Aq', 'Ar', 'As', 'At', 'Au', 'Av', 'Aw', 'Ax', 'Ay', 'Az'];

    this.jobId = this.route.snapshot.params.jobId;
    this.workerId = this.route.snapshot.params.workerId;
  }

  ngOnInit() {
    this.getPackageDetails();
  }

  // unblockDropOffLocation(): void {
  //   this.packageDetail$ = this.httpService.postRequest(this.globals.urls.post.unblockDropOff, { packageId: this.packageId })
  //     .pipe(
  //     )
  //     .subscribe(
  //       res => {
  //         this.httpService.showSuccess('Location unblock successfully', 'Unblock Drop Off');
  //         this.getPackageDetails();
  //       },
  //       err => {
  //         this.httpService.showError(err.error.message);
  //       }
  //     );
  // }

  Enablegdpr(from,type) {
    this.isLoadingResults = true;
    this.checkType=type;
    if (from == 'mobileUser') {
      this.showData.mobileUser = true;
    }
    else if (from == 'emailUser') {
      this.showData.emailUser = true;
    }
    else if (from == 'mobileWorker') {
      this.showData.mobileWorker = true;
    }
    else if (from == 'emailWorker') {
      this.showData.emailWorker = true;
    }
    else if (from == 'IdImage') {
      this.showData.IdImage = true;
    }
  }

  GotGDPRDATA(event) {
    this.isLoadingResults = false;
    if(this.checkType=='user'){
      if (event.data.phoneNumber && event.data.phonePreFix) {
        this.model.userData.userProfileId.phoneNumber = event.data.phoneNumber.toString()
        this.showEye.mobile = false;
      }
      else if (event.data.email) {
        this.model.userData.userProfileId.email = event.data.email
        this.showEye.email = false;
      }
    }
    else if(this.checkType=='worker'){
      if (event.data.phoneNumber && event.data.phonePreFix) {
        this.model.spData.spProfileId.phoneNumber =  event.data.phoneNumber.toString()
        this.showEye.mobile = false;
      }
      else if (event.data.email) {
        this.model.spData.spProfileId.email = event.data.email
        this.showEye.email = false;
      }
    }
  
    console.log('I got this', event);
  }

  // get Package details
  getPackageDetails() {
    this.isLoadingResults = true;

    let spDetailsUrl = this.globals.urls.TradeAdmin.workers.fetchSpJobDetails;
    spDetailsUrl = spDetailsUrl.replace(':jobId', this.jobId.toString());
    this.packageDetail$ = this.httpService.getRequest(spDetailsUrl)
      .pipe(
        map(res => res.data),
    ).subscribe(
      data => {
        this.isLoadingResults = false;
        this.model = data;

        this.origin = { lat: data.pickUpJobLocation.coordinates[1], lng: data.pickUpJobLocation.coordinates[0] };
        
        if (this.model.spJobStatus == 1 || this.model.jobStatus ==1 ) {
          this.model.jobStatusText = 'Open'
        }
        else if (this.model.spJobStatus == 2 || this.model.jobStatus == 2) {
          this.model.jobStatusText = 'Assigned'
        }
        else if (this.model.spJobStatus == 3 || this.model.jobStatus == 3) {
          this.model.jobStatusText = 'In-progress'
        }
        else if (this.model.spJobStatus == 4 || this.model.jobStatus == 4) {
          this.model.jobStatusText = 'Completed'
        }
        else if (this.model.spJobStatus == 5  || this.model.jobStatus == 5 ) {
          this.model.jobStatusText = 'Rated By User'
        }
        else if (this.model.spJobStatus == 6  || this.model.jobStatus ==6 ) {
          this.model.jobStatusText = 'Cancelled'
        }
        else if (this.model.spJobStatus == 7  || this.model.jobStatus ==7 ) {
          this.model.jobStatusText = 'Rejected'
        }
        else if (this.model.spJobStatus == 8 || this.model.jobStatus ==8 ) {
          this.model.jobStatusText = 'TimeOut'
        }
        else if (this.model.spJobStatus == 9  || this.model.jobStatus ==9 ) {
          this.model.jobStatusText = 'Discarded'
        }
        // waypoints, exclude the last dropoff as it will be the destination
        // if (data.dropOffDetail && data.dropOffDetail.length > 1) {
        //   let groupLength = data.dropOffDetail.length;
        //   for (let i = 0; i < groupLength; i++) {
        //     let item = data.dropOffDetail[i];
        //     if ((i + 1) < (groupLength)) {
        //       this.waypoints.push({
        //         location: { lat: item.latitude, lng: item.longitude },
        //         // stopover: false,
        //       })
        //     }
        //   }
        // }
        // this.destination = { lat: data.dropOffDetail[data.dropOffDetail.length - 1].latitude, lng: data.dropOffDetail[data.dropOffDetail.length - 1].longitude };
        this.destination = { lat: data.jobLocation.coordinates[1], lng: data.jobLocation.coordinates[0] };
      },
      err => {
        this.model = null;
        this.isLoadingResults = false;
        this.httpService.showError(err.error.message);
      }
    );
  }

  // assignDriver() {
  //   this.router.navigate(['/packages/assign-package', this.packageId]);
  // }

  // ng on destroy
  ngOnDestroy(): void {
    if (this.packageDetail$) this.packageDetail$.unsubscribe();
  }
}
