import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommisionCheckComponent } from './commision-check.component';

describe('CommisionCheckComponent', () => {
  let component: CommisionCheckComponent;
  let fixture: ComponentFixture<CommisionCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommisionCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommisionCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
