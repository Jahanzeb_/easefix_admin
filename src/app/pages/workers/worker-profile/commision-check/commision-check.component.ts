import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Globals } from 'src/app/Globals';
import { Subscription, fromEvent } from 'rxjs';
import { HttpService } from 'src/app/services/http-service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-commision-check',
  templateUrl: './commision-check.component.html',
  styleUrls: ['./commision-check.component.scss']
})
export class CommisionCheckComponent implements OnInit {
  reasons: any = [];
  cancellationText: any;
  // Public Depedency
  public globals = Globals;
  isLoadingResults = false;
  favoriteReason: any
  showTextBox: boolean = false;
  // Observables/Subscriptions
  httpSub$: Subscription = null;
  model: any = {};
  disabled:boolean=true;
  constructor(
    public service: HttpService,
    public dialogRef: MatDialogRef<CommisionCheckComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.model.commission=this.data.percentage;
    this.model.terms=true;
    // this.getinitialReasons();
  }
  somethingChanged(reason) {
    if (reason == true) {
      this.disabled = true;
      this.model.terms=reason;
    }
    else if(reason == false) {
      this.disabled = false;
    }
  }

  Approve(model){
    this.dialogRef.close(model);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  // submit cancel
  // cancel() {
  //   let params = {
  //     "reasonId": this.favoriteReason._id,
  //     "packageId": this.data.packageId,
  //     "additionalCancellationText": this.model.reason
  //   }

  //   let requestUrl = this.globals.urls.fetchPackages.cancleSubmit;
  //   // this.typeOfRequestSend = this.service.postRequest(this.requestUrl, this.dataToSend);
  //   this.isLoadingResults = true;
  //   this.httpSub$ = this.service.putRequest(requestUrl, params)
  //     .pipe(
  //       map(res => res.data),
  //   )
  //     .subscribe(
  //       data => {
  //         this.isLoadingResults = false;
  //         this.service.showSuccess('Package cancelled Successfully', 'Package Cancelled');
  //         this.onNoClick();
  //       },
  //       err => {
  //         this.isLoadingResults = false;
  //         this.service.showError(err);
  //       }
  //     );
  // }
}
