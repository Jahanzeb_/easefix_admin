import { Component, OnInit, OnDestroy, AfterViewInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { HttpService } from "src/app/services/http-service";
import { Subscription, forkJoin } from "rxjs";
import { map } from "rxjs/operators";
import { Globals } from "../../../Globals";
import { MatDialog } from "@angular/material";
import * as moment from "moment";
import { CommisionCheckComponent } from "./commision-check/commision-check.component";
import { ConfirmDialougComponent } from "../../common-pages/confirm-dialoug/confirm-dialoug.component";
import { BlockWorkerComponent } from "../block-worker/block-worker.component";
import { WorkerFeedbackComponent } from "../worker-feedback/worker-feedback.component";
@Component({
  selector: "app-worker-profile",
  templateUrl: "./worker-profile.component.html",
  styleUrls: ["./worker-profile.component.scss"],
})
export class WorkerProfileComponent implements OnInit, AfterViewInit {
  // Public
  globals = Globals;
  public previewImage: boolean = false;
  public previewImageUrl: string = "";

  // variable type ANY
  previewData: any = {};
  localStorage: any;
  dataToSend: any;
  previous: any;

  // variable type boolean
  isLoadingResults: boolean = false;
  isReject: boolean = false;
  locateDriver: boolean = false;

  // variable type string
  spId: string = null;
  workerType: string = null;
  rejectionReason: string = null;
  blockDriverText: string = "Block Worker";
  vehicleOwnerShip: string = "Vehicle's OwnerShip";
  exportDataURL: string;

  // Observables/Subscriptions
  httpSub$: Subscription = null;

  // Map settings
  zoom: number = 15;
  GdprDData: any;
  showData: any = {
    mobile: false,
    email: false,
    IdImage: false,
    bank: false,
  };
  showEye: any = {
    mobile: true,
    email: true,
    IdImage: true,
    bank: true,
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private httpService: HttpService,
    public dialog: MatDialog
  ) {
    this.spId = this.route.snapshot.params.workerId;
    this.workerType = this.route.snapshot.params.WorkerType;
  }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem("TradeMen-admin-data"))) {
      this.localStorage = JSON.parse(
        localStorage.getItem("TradeMen-admin-data")
      );
    }

    this.getProfile();
    this.exportUser();
  }
  ngAfterViewInit() {}
  // populateData(){
  //   this.GdprDData={
  //     id:this.workerType,
  //     mobile:this.previewData.spProfile.phone,
  //     email:this.previewData.spProfile.email,
  //     image:'',
  //   }
  // }
  getProfile() {
    this.isLoadingResults = true;
    let spDetailsUrl = this.globals.urls.TradeAdmin.workers.fetchSpDetails;
    spDetailsUrl = spDetailsUrl.replace(":workerId", this.spId.toString());
    let spProfileDetailsUrl =
      this.globals.urls.TradeAdmin.workers.fetchProfileData;
    spProfileDetailsUrl = spProfileDetailsUrl.replace(
      ":workerId",
      this.spId.toString()
    );

    this.httpSub$ = this.httpService.getRequest(spDetailsUrl).subscribe(
      (res) => {
        this.previewData = res.data;
        this.blockDriverText = this.previewData.spProfile.isBlocked
          ? "Unblock Worker"
          : "Block Worker";
        this.isLoadingResults = false;
        // this.populateData();
      },
      (err) => {
        this.isLoadingResults = false;
        this.httpService.showError(err);
      }
    );
  }

  exportUser() {
    this.isLoadingResults = true;
    let params = {
      userProfileId: this.spId,
      userType: "sp",
    };
    this.httpSub$ = this.httpService
      .postRequest(this.globals.urls.getCSVDetails, params)
      .subscribe(
        (res) => {
          this.isLoadingResults = false;
          this.exportDataURL = res.data.userFileUrl;
        },
        (err) => {
          this.isLoadingResults = false;
          this.httpService.showError(err);
        }
      );
  }
  // https://stackoverflow.com/questions/286141/remove-blank-attributes-from-an-object-in-javascript
  // removeEmpty(obj) {
  //   return Object.keys(obj)
  //     .filter(k => obj[k] !== null && obj[k] !== undefined && obj[k] !== '')  // Remove undef. and null. and empty strings
  //     .reduce((newObj, k) =>
  //       typeof obj[k] === 'object' ?
  //         Object.assign(newObj, { [k]: this.removeEmpty(obj[k]) }) :  // Recurse.
  //         Object.assign(newObj, { [k]: obj[k] }),  // Copy value.
  //       {});
  // }

  editWorker() {
    if (this.workerType == "current") {
      this.router.navigate(["/workers/edit"], {
        queryParams: { workerId: this.spId, from: "oldWorker" },
        queryParamsHandling: "merge",
      });
    } else {
      this.router.navigate(["/workers/edit"], {
        queryParams: { workerId: this.spId, from: "newWorker" },
        queryParamsHandling: "merge",
      });
    }
  }

  verifyCertificate() {
    this.isLoadingResults = true;
    let verifyCertificateUrl =
      this.globals.urls.TradeAdmin.workers.editWorker.verifyCertificate;
    verifyCertificateUrl = verifyCertificateUrl.replace(
      ":workerId",
      this.spId.toString()
    );
    this.httpService.getRequest(verifyCertificateUrl).subscribe(
      (res) => {
        this.isLoadingResults = false;
        this.httpService.showSuccess(res.message, "Worker");
        this.getProfile();
      },
      (err) => {
        this.isLoadingResults = false;
        this.httpService.showError(err);
      }
    );
  }

  userStatusUpdate(status) {
    this.isLoadingResults = true;
    let params = {
      userId: this.spId,
      isBlocked: !status,
      userType: "sp",
    };
    this.httpService
      .putRequest(this.globals.urls.TradeAdmin.activeBlockUser, params)
      .subscribe(
        (res) => {
          this.isLoadingResults = false;
          this.previewData.spProfile.isBlocked = !status;
          this.blockDriverText = status ? "Block Worker" : "Unblock Worker";
          this.httpService.showSuccess(
            "Worker status updated successfully.",
            "Worker"
          );
        },
        (err) => {
          this.isLoadingResults = false;
          this.httpService.showError(err);
        }
      );
  }
  Enablegdpr(from) {
    this.isLoadingResults = true;
    if (from == "mobile") {
      this.showData.mobile = true;
    } else if (from == "email") {
      this.showData.email = true;
    } else if (from == "IdImage") {
      this.showData.IdImage = true;
    } else if (from == "bank") {
      this.showData.bank = true;
    }
  }

  GotGDPRDATA(event) {
    this.isLoadingResults = false;
    if (event.data.phoneNumber && event.data.phonePreFix) {
      this.previewData.spProfile.phone =
        event.data.phonePreFix.toString() + event.data.phoneNumber.toString();
      this.showEye.mobile = false;
    } else if (event.data.email) {
      this.previewData.spProfile.email = event.data.email;
      this.showEye.email = false;
    } else if (event.data.bankIbnNumber) {
      this.previewData.bankInfo.bankIbnNumber = event.data.bankIbnNumber;
      this.showEye.bank = false;
    }
  }

  commisionDial(): void {
    let dialogRef = this.dialog.open(CommisionCheckComponent, {
      width: "400px",
      panelClass: "ratingDialoug",
      data: {
        WorkerId: this.spId,
        percentage: this.previewData.spProfile.commissionPercentage,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      let commission = this.previewData.spProfile.commissionPercentage;
      if (result.terms == true) {
        commission = this.previewData.spProfile.commissionPercentage;
      } else {
        commission = result.commission;
      }
      this.approve(commission);
    });
  }

  approve(commission) {
    this.isLoadingResults = true;
    this.httpSub$ = this.httpService
      .putRequest(this.globals.urls.TradeAdmin.workers.approveWorker, {
        workerId: this.spId,
        commissionPercentage: commission,
      })
      .subscribe(
        (res) => {
          this.isLoadingResults = false;
          this.httpService.showSuccess("Worker approved successfully", null);
          this.previewData.spProfile.isVerifiedByAdmin = true;
          this.workerType = "current";
          // this.getProfile();
        },
        (err) => {
          this.isLoadingResults = false;
          this.httpService.showError(err);
        }
      );
  }

  update(): void {
    let dialogRef = this.dialog.open(ConfirmDialougComponent, {
      width: "500px",
      panelClass: "ratingDialoug",
      data: {
        callingFromFunction: "DeleteWorker",
        DialougHeaderText: "Delete Worker",
        DialougBodyText:
          "Are you sure you want to permenantly delete worker data?",
        ButtonCancelText: "Cancel",
        ButtonSubmitText: "Delete",
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result == true) {
        this.DeleteInfo();
      }
    });
  }

  DeleteInfo() {
    this.isLoadingResults = true;
    let params = {
      _id: this.spId,
      userType: "sp",
    };
    this.httpSub$ = this.httpService
      .postRequest(this.globals.urls.deleteUserCompletely, params)
      .subscribe(
        (res) => {
          this.isLoadingResults = false;
          this.httpService.showSuccess("All Worker data deleted", "Worker");
          if (this.workerType == "current") {
            this.router.navigate(["/workers/listing"]);
          } else {
            this.router.navigate(["/workers/new-requests"]);
          }
        },
        (err) => {
          this.isLoadingResults = false;
          // this.service.showError(err);
        }
      );
  }

  viewJobHistory() {
    this.router.navigate(["/workers/job", this.spId]);
  }
  trackByFn(index, item) {
    return index; // or item.id
  }

  reject() {
    this.isLoadingResults = true;
    const params = {
      workerId: this.spId,
      // action: 'reject',
      reason: this.rejectionReason,
    };

    this.httpSub$ = this.httpService
      .putRequest(this.globals.urls.TradeAdmin.workers.rejectWorker, params)
      .subscribe(
        (res) => {
          this.isLoadingResults = false;
          this.httpService.showSuccess("Worker rejected successfully", null);
          this.isReject = false;
          this.getProfile();
        },
        (err) => {
          this.isLoadingResults = false;
          // this.httpService.showError(err);
        }
      );
  }

  previewImageViewer(image) {
    this.previewImageUrl = image;
    this.previewImage = true;
  }

  closePreview() {
    this.previewImage = false;
  }

  // delete(data) {
  //   this.dataToSend = {
  //     spId: data.id
  //   }
  //   this.httpService.postRequest(this.globals.urls.post.dltAccDriver, this.dataToSend).subscribe(
  //     res => {
  //       if (res.response === 200) {
  //         this.httpService.showSuccess('Driver Status updated successfully.', 'Driver Status');
  //         this.router.navigate(['drivers/new-requests'])
  //       }
  //       else if (res.response == 400) {
  //         this.httpService.showError(res['message'], 'Driver Status');
  //       }
  //     },
  //     err => {
  //       this.httpService.showError(err.error.message);
  //     },
  //   );
  // }

  // show one info window at a time (drivers)
  clickedMarker(infowindow) {
    if (this.previous) {
      this.previous.close();
    }
    this.previous = infowindow;
  }

  // show location of driver
  showLocation() {
    this.locateDriver = true;
  }

  ngOnDestroy() {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }

  blockUserDial(): void {
    let dialogRef = this.dialog.open(BlockWorkerComponent, {
      width: "400px",
      height: "300px",
      panelClass: "blockUserDialogue",
      data: {
        userId: this.spId,
        userType: "sp",
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.previewData.spProfile.isBlocked = result.isBlocked;
        this.previewData.spProfile.reason = result.blockReason;
      }
      // this.getListing();
    });
  }

  feedbackDial(): void {
    let className = "feedbackDialogue";
    if (!this.previewData.ratings.length) {
      className = "noDataDialogue";
    }
    let dialogRef = this.dialog.open(WorkerFeedbackComponent, {
      width: "600px",
      // height: "700px",
      maxHeight: "600px",
      panelClass: className,
      data: {
        ratings: this.previewData.ratings,
      },
    });
  }
}
