import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkersComponent } from './workers.component';
import { WorkerProfileComponent } from './worker-profile/worker-profile.component';
import { WorkerJobHistoryComponent } from './worker-job-history/worker-job-history.component';
import { WorkerPackageDetailComponent } from './worker-package-detail/worker-package-detail.component';
import { RouterGuard } from '../../guards/route.guard';

const routes: Routes = [
  {
    path: '',
    component: WorkersComponent,
    children: [
      {
        path: '',
        loadChildren: './workers-listing/workers-listing.module#WorkersListingModule'
      },
      {
        path: 'profile/:WorkerType/:workerId',
        component: WorkerProfileComponent,
        canActivate: [RouterGuard],
        data: { workers: 1 },
      },
      // {
      //   path: 'edit-profile/:driverId',
      //   component: EditDriverComponent
      // },
      {
        path: 'job/:workerId',
        component: WorkerJobHistoryComponent,
        canActivate: [RouterGuard],
        data: { workers: 1 },
      },
      {
        path: 'job-detail/:jobId/:workerId',
        component: WorkerPackageDetailComponent,
        canActivate: [RouterGuard],
        data: { workers: 1 },
      },

      {
        path: 'edit',
        loadChildren: './edit-worker/edit-worker.module#EditWorkerModule',
      },
    ]
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkersRoutingModule { }
