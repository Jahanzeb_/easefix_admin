import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ViewChild,
} from "@angular/core";
import { Globals, checkIfOnlySpaces } from "src/app/Globals";
import { HttpService } from "src/app/services/http-service";
import { Subscription } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";
import { map } from "rxjs/operators";

@Component({
  selector: "app-edit-certificates",
  templateUrl: "./edit-certificates.component.html",
  styleUrls: ["./edit-certificates.component.scss"],
})
export class EditCertificatesComponent implements OnInit, AfterViewInit {
  // Public functions
  public previewImage: boolean = false;
  public previewImageUrl: string = "";
  globals = Globals;

  // Observables/Subscriptions
  httpSub$: Subscription = null;

  // Type any
  model: any = [];
  localStorage: any;

  @ViewChild("form") form;

  isLoadingResults: boolean = false;

  // variable type null
  fileToUpload: File = null;

  spId: string = null;
  buttonText: string = "Next";

  viewAsCM: any;
  whenAdminViewPrefix: any;
  GdprDData: any;
  comingFrom: string;
  newTitle: string = '';
  // pattern regex
  public emailPattern = this.globals.regex.email;

  constructor(
    private httpService: HttpService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    // when editing, snapshot params will be required from '/:spId'
    this.spId =
      this.route.snapshot.queryParams.spId ||
      this.route.snapshot.queryParams.workerId;
    this.comingFrom = this.route.snapshot.queryParams.from;
  }

  ngOnInit() {
    // on step one completed, store spId in localStorage
    if (this.spId) {
      this.isLoadingResults = true;
      let url = this.globals.urls.TradeAdmin.workers.editWorker.getProfDoc;
      url = url.replace(":workerId", this.spId.toString());

      this.httpSub$ = this.httpService
        .getRequest(url)
        .pipe(map((res) => res.data))
        .subscribe(
          (res) => {
            this.isLoadingResults = false;
            // console.log(res);
            if (res) {
              this.model = res.professionalLicenseDocuments;
            }
          },
          (err) => {
            this.isLoadingResults = false;
            // this.httpService.showError(err);
          }
        );
    }
  }

  ngAfterViewInit() {
    this.GdprDData = {
      val1: "asdasd",
    };
  }

  GotGDPRDATA(event) {
    console.log("I got this", event);
  }

  checkIndex(fileIndex) {
    console.log("------", fileIndex);
  }
  // Upload Package Image
  uploadPackageImage(files: FileList, type: any, index: any) {
    if (files[0].type === "image/gif") {
      this.httpService.showError(
        "Gif images are not allowed",
        "Profile details"
      );
      return;
    }

    this.fileToUpload = files.item(0);
    this.isLoadingResults = true;
    this.httpSub$ = this.httpService
      .uploadImage(this.globals.urls.uploadImage, this.fileToUpload)
      .subscribe(
        (res) => {
          this.isLoadingResults = false;
          let title = this.newTitle;
          if (type == "old") {
            this.model[index].professionalDocsFront = res.data.url;

          } else if (type == "new") {
            let number = this.model.length + 1;
            let obj = {
              professionalDocsBack: "",
              professionalDocsFront: res.data.url,
              professionalDocsTitle: title,
            };
            this.model.push(obj);
          }
          this.newTitle = '';
          this.model["imageString"] = res.data.url;
        },
        (err) => {
          this.isLoadingResults = false;
          // this.httpService.showError(err.error.message)
        }
      );
  }

  countryCodePrefix: any;

  submit() {
    let params = {
      workerId: this.spId,
      docsArray: this.model,
    };
console.log('-----', params);
// return;
    this.isLoadingResults = true;
    let url = this.globals.urls.TradeAdmin.workers.editWorker.updateProfDoc;
    // url=url.replace(":workerId",this.spId.toString())
    this.httpSub$ = this.httpService
      .putRequest(url, params)
      .pipe(map((res) => res.data))
      .subscribe(
        (data) => {
          this.isLoadingResults = false;
          this.httpService.showSuccess(
            data.message,
            "Service certificates updated"
          );
          this.router.navigate(["/workers/edit/portfolio"], {
            queryParams: { spId: this.spId, from: this.comingFrom },
            queryParamsHandling: "merge",
          });
        },
        (err) => {
          this.isLoadingResults = false;
          this.httpService.showError(err);
        }
      );
  }

  previewImageViewer(image) {
    this.previewImageUrl = image;
    this.previewImage = true;
  }

  closePreview() {
    this.previewImage = false;
  }

  ngOnDestroy(): void {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }

  deleteImage(e, image, index) {
    e.preventDefault();

    this.model.splice(index, 1);
    // let params = {
    //   workerId: this.spId,
    //   certificateId: image._id,
    //   docsArray: this.model,
    //   deleteNew: true
    // };

    // this.isLoadingResults = true;
    // let url = this.globals.urls.TradeAdmin.workers.editWorker.updateProfDoc;

    // this.httpSub$ = this.httpService
    //   .putRequest(url, params)
    //   .pipe(map((res) => res.data))
    //   .subscribe(
    //     (data) => {
    //       this.isLoadingResults = false;
    //       this.httpService.showSuccess(
    //         data.message,
    //         "Service certificates updated"
    //       );
    //       this.model = data.professionalLicenseDocuments;
    //     },
    //     (err) => {
    //       this.isLoadingResults = false;
    //       this.httpService.showError(err);
    //     }
    //   );
  }
}
