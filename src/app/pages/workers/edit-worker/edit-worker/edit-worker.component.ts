import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpService } from 'src/app/services/http-service';
import { Globals } from 'src/app/Globals';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { ViewRef_ } from '@angular/core/src/view';

@Component({
  selector: 'app-edit-worker',
  templateUrl: './edit-worker.component.html',
  styleUrls: ['./edit-worker.component.scss']
})
export class EditWorkerComponent implements OnInit {
  public currentStep = 1;

  private httpSub$: Subscription = null;
  private showSubmitBtnSub: Subscription = null;
  private globals = Globals;

  // Type Boolean
  showSubmitBtn: boolean = false;
  disableOtherTabs: boolean = false;

  // Type  null
  spId: string = null;
  localStorage = null;
  workerId: string = null;
  comingFrom: string;

  constructor(private cdref: ChangeDetectorRef, private httpService: HttpService, private router: Router, private route: ActivatedRoute) {
    this.workerId = this.route.snapshot.queryParams.spId || this.route.snapshot.queryParams.workerId;
    this.comingFrom = this.route.snapshot.queryParams.from;
  }

  ngOnInit() {


  }
}
