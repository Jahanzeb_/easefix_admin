import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditWorkerComponent } from './edit-worker/edit-worker.component';
import { ProfileDetailsComponent } from './profile-details/profile-details.component';
import { EditServicesComponent } from './edit-services/edit-services.component';
import { EditIdentityDocComponent } from './edit-identity-doc/edit-identity-doc.component';
import { EditCertificatesComponent } from './edit-certificates/edit-certificates.component';
import { EditPersonalInfoComponent } from './edit-personal-info/edit-personal-info.component';
import { EditLanguagesComponent } from './edit-languages/edit-languages.component';
import { RouterGuard } from '../../../guards/route.guard';
import { EditPortfolioComponent } from './edit-portfolio/edit-portfolio.component';

const routes: Routes = [
  {
    path: '',
    component: EditWorkerComponent,
    canActivate: [RouterGuard],
    data: { 
      workers: 1, 
      workersView: 2, 
    },
    children: [
      {
        path: 'profile-details',
        component: ProfileDetailsComponent,
      },
      {
        path: 'edit-services',
        component: EditServicesComponent,
      },
      {
        path: 'Id-doc',
        component: EditIdentityDocComponent,
      },
      {
        path: 'certificate-doc',
        component: EditCertificatesComponent,
      },
      {
        path: 'personal',
        component:  EditPersonalInfoComponent
      },
      {
        path: 'languages',
        component:  EditLanguagesComponent
      },
      // {
      //   path: 'preview',
      //   component: PreviewComponent,
      // },
      {
        path: 'portfolio',
        component: EditPortfolioComponent,
      },
      {
        path: '',
        redirectTo: 'profile-details',
        pathMatch: 'full',
      },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditWorkerRoutingModule { }
