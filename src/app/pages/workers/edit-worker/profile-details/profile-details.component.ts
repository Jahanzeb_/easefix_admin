import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Globals, checkIfOnlySpaces } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html',
  styleUrls: ['./profile-details.component.scss']
})
export class ProfileDetailsComponent implements OnInit {
  // Public functions
  public previewImage: boolean = false;
  public previewImageUrl: string = '';
  globals = Globals;

  // Observables/Subscriptions
  httpSub$: Subscription = null;

  // Type any
  model: any = {};
  localStorage: any;

  @ViewChild('form') form;

  isLoadingResults: boolean = false;

  // variable type null
  fileToUpload: File = null;

  spId: string = null;
  buttonText: string = 'Next';

  viewAsCM: any;
  whenAdminViewPrefix: any;
  comingFrom: string;


  // pattern regex
  public emailPattern = this.globals.regex.email;

  constructor(private service: HttpService, private route: ActivatedRoute, private router: Router) {

    // when editing, snapshot params will be required from '/:spId'
    this.spId = this.route.snapshot.queryParams.spId || this.route.snapshot.queryParams.workerId;
    this.comingFrom = this.route.snapshot.queryParams.from;
  }

  ngOnInit() {
    // on step one completed, store spId in localStorage
    if (this.spId) {
      this.isLoadingResults = true;
      let url=this.globals.urls.TradeAdmin.workers.editWorker.getProfile;
      url=url.replace(":workerId",this.spId.toString())

      this.httpSub$ = this.service.getRequest(url)
        .pipe(
          map(res => res.data),
        )
        .subscribe(
          res => {
            this.isLoadingResults = false;
            if (res) {
              this.model = {
                ...res,
                imageString: res.profileImage || '',
                phone: res.phone || '',
              }
            }
          },
          err => {
            this.isLoadingResults = false;
            // this.service.showError(err);
          }
        );
    }
  }

  // Upload Package Image
  uploadPackageImage(files: FileList) {
    if (files[0].type === 'image/gif') {
      this.service.showError('Gif images are not allowed', 'Profile details');
      return;
    }
    this.fileToUpload = files.item(0);
    this.isLoadingResults = true;
    this.httpSub$ = this.service.uploadImage(this.globals.urls.uploadImage, this.fileToUpload).subscribe(
      res => {
        this.isLoadingResults = false;
        this.model['imageString'] = res.data.url;
      },
      err => {
        this.isLoadingResults = false;
        // this.service.showError(err.error.message)
      },
    )
  }

  countryCodePrefix: any;

  submit() {
    const params = {
      firstName:this.model['firstName'],
      lastName:this.model['lastName'],
      profileImage: this.model['imageString'],
      about: this.model['about'],
      commissionPercentage: this.model['commissionPercentage']
    };

    this.isLoadingResults = true;
    let url=this.globals.urls.TradeAdmin.workers.editWorker.updateWorkerProfile;
    url=url.replace(":workerId",this.spId.toString())
    this.httpSub$ = this.service.putRequest(url, params)
      .pipe(
        map(res => res.data),
      )
      .subscribe(
        data => {
          this.isLoadingResults = false;
          this.service.showSuccess(data.message,"Profile Updated");
          // this.router.navigate(['/workers/edit/edit-services', this.spId]);
          this.router.navigate(['/workers/edit/edit-services'], { queryParams: {spId: this.spId, from: this.comingFrom}, queryParamsHandling: 'merge' });
        },
        err => {
          this.isLoadingResults = false;
          this.service.showError(err);
        }
      );
  }

  previewImageViewer(image) {
    this.previewImageUrl = image;
    this.previewImage = true;
  }

  closePreview() {
    this.previewImage = false;
  }

  ngOnDestroy(): void {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }
}
