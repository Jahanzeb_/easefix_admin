import { Component, OnInit, ViewChild, Inject, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpService } from 'src/app/services/http-service';
import { ToastrService } from 'ngx-toastr';
import { Globals } from '../../../../Globals';
import { Subscription, fromEvent, forkJoin } from 'rxjs';
import { catchError, map, switchMap, startWith } from 'rxjs/operators';
import { HttpParams } from '@angular/common/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-edit-services',
  templateUrl: './edit-services.component.html',
  styleUrls: ['./edit-services.component.scss']
})
export class EditServicesComponent implements OnInit {
  // Public Depedency
  public globals = Globals;
  // Observables/Subscriptions
  private httpSub$: Subscription = null;
  workerId: string = null;
  jobType: any = [];
  previeDataFromServer: any = [];
  jobFilterText = { serviceName: 'job Type' };

  // Type boolean variables
  isLoadingResults = false;
  comingFrom: string;
  disableOverAll:boolean=false;

  constructor(public service: HttpService, private route: ActivatedRoute, private router: Router ) {
    this.workerId = this.route.snapshot.queryParams.spId || this.route.snapshot.queryParams.workerId;
    this.comingFrom = this.route.snapshot.queryParams.from;
  }

  ngOnInit() {
    this.initialService();
  }

  initialService() {
    let spListUrl = this.globals.urls.TradeAdmin.workers.editWorker.getWorkerServices;
    spListUrl = spListUrl.replace(':workerId', this.workerId);
    this.isLoadingResults = true;
    this.httpSub$ = forkJoin(
      this.service.getRequest(this.globals.urls.TradeAdmin.workers.fetchJobTypes),
      this.service.getRequest(spListUrl),
    )
      .pipe(
        map(([jobTypes, splist]) => {
          this.jobType = jobTypes.data.servicesList
          this.previeDataFromServer = splist.data.services
        }),
    )
      .subscribe(
        res => {
          this.isLoadingResults = false;
          this.previeDataFromServer.forEach((element, index) => {
            element.disabled = true;
            element.newServiceName = { serviceName: 'job Type' };
            this.jobType.forEach((job, indexLang) => {
              if(element.serviceId && job._id==element.serviceId._id){
                this.jobType.splice(indexLang,1);
              }
            });
          });

          this.isLoadingResults = false;
        },
        err => {
          this.isLoadingResults = false;
          this.service.showError(err);
        }
      );
  }

  // enable edit
  enableEdit(id, job) {
    this.previeDataFromServer[id].disabled = false;
    this.previeDataFromServer[id].newServiceName = job;
    this.disableOverAll=true;
  }
  // select type
  filter(id,job) {
    this.previeDataFromServer[id].newServiceName = job;
  }

  
  deleteService(id,job,newJob) {
    if( this.disableOverAll==true && job.isNew==true){
      this.previeDataFromServer.splice(id,1);
      this.disableOverAll=false;
    }
    else{
    this.isLoadingResults = true;
    let url: string = this.globals.urls.TradeAdmin.workers.editWorker.updateWorkerServices,
     params = {
      "workerId":  this.workerId,
      "serviceId":job.serviceId._id,
      "oldServiceId":'',
      "hourlyRate": job.serviceHourlyRate,
      "isDelete":true,
      "isUpdate":false,
    };
    if(job.newServiceName._id==job.serviceId._id && job.isNew==true){
      params.oldServiceId=''
    }
    else{
      params.oldServiceId=job.newServiceName._id
    }
      this.service.putRequest(url, params)
      .subscribe(
      res => {
        this.isLoadingResults = false;
        this.previeDataFromServer.splice(id,1);

        if (res.response === 200) {
          this.initialService();
          this.service.showSuccess('Service Deleted Successfully.', 'Worker');
          this.disableOverAll=false;
        }
        else if (res.response == 400) {
          this.service.showError(res['message'], 'Worker');
        }
      },
      err => {
        this.service.showError(err.error.message);
        this.isLoadingResults = false;
      },
    );

  }
  }

  addNew(){
    let NewObjToPush={
      disabled:false,
      serviceHourlyRate:'',
      newServiceName:{ serviceName: 'job Type' },
      serviceId:{},
      isNew:true,
    }
    this.previeDataFromServer.push(NewObjToPush);
    this.disableOverAll=true;
  }
  submit(){
    // this.router.navigate(['/workers/edit/Id-doc', { queryParams: { workerId: this.workerId} }]);
    // this.router.navigate(['/workers/edit/Id-doc'], { queryParams: {workerId: this.workerId, from: this.comingFrom}, queryParamsHandling: 'merge' });
    this.router.navigate(['/workers/edit/certificate-doc'], { queryParams: {spId: this.workerId ,  from: this.comingFrom}, queryParamsHandling: 'merge' });
  }
  saveSettings(id, job) {
    this.isLoadingResults = true;
    let params = {
      "workerId":  this.workerId,
      "serviceId":job.newServiceName._id,
      "oldServiceId":'',
      "isUpdate":false,
      "hourlyRate": job.serviceHourlyRate,
      "isDelete": false,
    }
    // if(job.newServiceName._id==job.serviceId._id && job.isNew==true){
    if(job.isNew==true){
      params.oldServiceId='';
      params.isUpdate=false;
    }
    else{
      params.oldServiceId=job.serviceId._id;
      params.isUpdate=true;
    }
    this.service.putRequest(this.globals.urls.TradeAdmin.workers.editWorker.updateWorkerServices, params).subscribe(
      res => {
        this.isLoadingResults = false;

        this.previeDataFromServer[id].disabled = true;
        if (res.response === 200) {
          this.initialService();
          this.service.showSuccess('Services Updated.', 'Worker');
          this.disableOverAll=false;

        }
        else if (res.response == 400) {
          this.service.showError(res['message'], 'Reasons');
        }
      },
      err => {
        this.service.showError(err.error.message);
      },
    );
  }



}
