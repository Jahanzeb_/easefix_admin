import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ViewChild,
} from "@angular/core";
import { Globals, checkIfOnlySpaces } from "src/app/Globals";
import { HttpService } from "src/app/services/http-service";
import { Subscription } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";
import { map } from "rxjs/operators";

@Component({
  selector: "app-edit-portfolio",
  templateUrl: "./edit-portfolio.component.html",
  styleUrls: ["./edit-portfolio.component.scss"],
})
export class EditPortfolioComponent implements OnInit {
  // Public functions
  public previewImage: boolean = false;
  public previewImageUrl: string = "";
  globals = Globals;
  type: any = 0;
  // Observables/Subscriptions
  httpSub$: Subscription = null;

  // Type any
  model: any = [];
  localStorage: any;

  @ViewChild("form") form;

  isLoadingResults: boolean = false;

  // variable type null
  fileToUpload: File = null;

  spId: string = null;
  buttonText: string = "Next";

  viewAsCM: any;
  whenAdminViewPrefix: any;
  GdprDData: any;
  comingFrom: string;

  // pattern regex
  public emailPattern = this.globals.regex.email;

  constructor(
    private httpService: HttpService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.spId =
      this.route.snapshot.queryParams.spId ||
      this.route.snapshot.queryParams.workerId;
    this.comingFrom = this.route.snapshot.queryParams.from;
  }

  ngOnInit() {
    // on step one completed, store spId in localStorage
    if (this.spId) {
      this.isLoadingResults = true;
      let url = this.globals.urls.TradeAdmin.workers.editWorker.getPortfolio;
      url = url.replace(":workerId", this.spId.toString());

      this.httpSub$ = this.httpService
        .getRequest(url)
        .pipe(map((res) => res.data))
        .subscribe(
          (res) => {
            this.isLoadingResults = false;
            // console.log(res);
            if (res) {
              this.model = res.portfolio;
            }
          },
          (err) => {
            this.isLoadingResults = false;
            // this.httpService.showError(err);
          }
        );
    }
  }

  ngAfterViewInit() {
    this.GdprDData = {
      val1: "asdasd",
    };
  }

  GotGDPRDATA(event) {
    console.log("I got this", event);
  }

  uploadPackageImage(files: FileList, type: any, index: any) {
    if (files[0].type === "image/gif") {
      this.httpService.showError(
        "Gif images are not allowed",
        "Profile details"
      );
      return;
    } else if (files[0].size > 5000000) {
      this.httpService.showError(
        "File should not be greater than 5 MB",
        "Profile details"
      );
      return;
    }

    if (files[0].type === "video/mp4") {
      this.type = 1;
    }
    this.fileToUpload = files.item(0);
    // let url = this.globals.urls.uploadPortfolio;
    // url = url.replace(":type", this.type);
    this.isLoadingResults = true;
    this.httpSub$ = this.httpService
      .uploadImage(this.globals.urls.uploadPortfolio, this.fileToUpload)
      .subscribe(
        (res) => {
          this.isLoadingResults = false;
          if (type == "old") {
            this.model[index].url = res.data.url;
          } else if (type == "new") {
            let number = this.model.length + 1;
            let obj = {
              url: res.data.url,
              type: this.type,
            };
            this.model.push(obj);
          }
          console.log("-------", this.model);
          this.model["imageString"] = res.data.url;
        },
        (err) => {
          this.isLoadingResults = false;
          // this.httpService.showError(err.error.message)
        }
      );
  }

  countryCodePrefix: any;

  submit() {
    let params = {
      workerId: this.spId,
      portfolio: this.model,
    };

    this.isLoadingResults = true;
    let url = this.globals.urls.TradeAdmin.workers.editWorker.updatePortfolio;
    // url=url.replace(":workerId",this.spId.toString())
    this.httpSub$ = this.httpService
      .putRequest(url, params)
      .pipe(map((res) => res.data))
      .subscribe(
        (data) => {
          this.isLoadingResults = false;
          this.httpService.showSuccess(data.message, "Portfolio updated");
          this.router.navigate(["/workers/edit/personal"], {
            queryParams: { spId: this.spId, from: this.comingFrom },
            queryParamsHandling: "merge",
          });
        },
        (err) => {
          this.isLoadingResults = false;
          this.httpService.showError(err);
        }
      );
  }

  deleteImage(e, image, index) {
    e.preventDefault();
    this.model.splice(index, 1);
    // let params = {
    //   workerId: this.spId,
    //   portfolioId: image._id,
    //   portfolio: this.model,
    //   deleteNew: true
    // };

    // this.isLoadingResults = true;
    // let url = this.globals.urls.TradeAdmin.workers.editWorker.updatePortfolio;

    // this.httpSub$ = this.httpService
    //   .putRequest(url, params)
    //   .pipe(map((res) => res.data))
    //   .subscribe(
    //     (data) => {
    //       this.isLoadingResults = false;
    //       this.httpService.showSuccess(data.message, "Portfolio updated");
    //       this.model = data.portfolio
    //     },
    //     (err) => {
    //       this.isLoadingResults = false;
    //       this.httpService.showError(err);
    //     }
    //   );
  }

  previewImageViewer(image) {
    this.previewImageUrl = image;
    this.previewImage = true;
  }

  closePreview() {
    this.previewImage = false;
  }

  ngOnDestroy(): void {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }
}
