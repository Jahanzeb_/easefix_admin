import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Globals, checkIfOnlySpaces } from 'src/app/Globals';
import { Subscription, forkJoin } from 'rxjs';
import { HttpService } from 'src/app/services/http-service';
import { Router, ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-edit-personal-info',
  templateUrl: './edit-personal-info.component.html',
  styleUrls: ['./edit-personal-info.component.scss']
})
export class EditPersonalInfoComponent implements OnInit {

  model: any = {};

  fileToUpload: File = null;
  isLoadingResults: boolean = false;
  globals = Globals;
  httpSub$: Subscription = null;

  buttonText: string = 'Next';
  spId: string = null;
  banks = [];
  comingFrom: string;
  @ViewChild('form') form;

  constructor(private httpService: HttpService, private router: Router, private route: ActivatedRoute, ) {
    this.spId = this.route.snapshot.queryParams.spId || this.route.snapshot.queryParams.workerId;
    this.comingFrom = this.route.snapshot.queryParams.from;
  }

  ngOnInit() {
    // if (JSON.parse(localStorage.getItem('editDriver'))) {
    //   this.spId = JSON.parse(localStorage.getItem('editDriver')).spId;

    //   if (JSON.parse(localStorage.getItem('editDriver')).review) this.buttonText = 'Update';
    // }
    // else if (JSON.parse(localStorage.getItem('newDriver'))) this.spId = JSON.parse(localStorage.getItem('newDriver')).spId;

    if (this.spId) {
      this.isLoadingResults = true;
      let url = this.globals.urls.TradeAdmin.workers.editWorker.getBankDetails;
      url = url.replace(":workerId", this.spId.toString())

      this.httpSub$ = this.httpService.getRequest(url)
        .pipe(
          map(res => res.data),
      )
        .subscribe(
          data => {
            this.isLoadingResults = false;
            if (data) {
              this.model = {
                bankHolder: data.accountHolderName,
                bankAccNo: data.bankIbnNumber,
                bankRoutingNo: data.bankRoutingNumber,
              };
            }
          },
          err => {
            this.isLoadingResults = false;
            // this.httpService.showError(err);
          }
        );
    }
  }


  submit() {
    const params = {
      accountHolderName: this.model['bankHolder'],
      bankIbnNumber: this.model['bankAccNo'],
      bankRoutingNumber: this.model['bankRoutingNo'],
    };
    let url = this.globals.urls.TradeAdmin.workers.editWorker.updateBankDetails;
    url = url.replace(":workerId", this.spId.toString())

    this.isLoadingResults = true;
    this.httpSub$ = this.httpService.putRequest(url, params)
      .subscribe(
        res => {
          this.isLoadingResults = false;
          this.httpService.showSuccess("Bank Info Updated","Profile Updated");
          this.router.navigate(['/workers/edit/languages'], { queryParams: {spId: this.spId, from: this.comingFrom}, queryParamsHandling: 'merge' });
        },
        err => {
          this.isLoadingResults = false;
          this.httpService.showError(err);
        }
      );
  }

  public checkIfOnlySpaces(control: string) {
    return checkIfOnlySpaces(this.form, control);
  }


  ngOnDestroy() {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }

  onKeydown(event) {
    if (event.which == 64 || event.which == 16) {
      // to allow numbers and F5 to refresh page
      return false;
    } else if (event.which >= 48 && event.which <= 57) {
      // to allow numbers
      return true;
    } else if (event.which >= 96 && event.which <= 105) {
      // to allow numpad number
      return true;
    } else if ([8, 9, 13, 27, 37, 38, 39, 40, 46, 107].indexOf(event.which) > -1) {
      // to allow backspace, tab, enter, escape, arrows, numPad+
      return true;
    } else if (event.which == 65 && event.ctrlKey === true) {
      // Ctrl+A
      return true;
    } else if (event.which == 187 && event.shiftKey === true) {
      // Shift +
      return true;
    }
    else if (event.which == 86 && event.ctrlKey === true) {
      // Ctrl+C
      return true;
    }
    else if (event.which == 67 && event.ctrlKey === true) {
      // Ctrl+V
      return true;
    }
    else {
      event.preventDefault();
      // to stop others
      return false;
    }
  }

}
