import { Component, OnInit, ViewChild, Inject, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpService } from 'src/app/services/http-service';
import { ToastrService } from 'ngx-toastr';
import { Globals } from '../../../../Globals';
import { Subscription, fromEvent, forkJoin } from 'rxjs';
import { catchError, map, switchMap, startWith } from 'rxjs/operators';
import { HttpParams } from '@angular/common/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-edit-languages',
  templateUrl: './edit-languages.component.html',
  styleUrls: ['./edit-languages.component.scss']
})
export class EditLanguagesComponent implements OnInit {
  // Public Depedency
  public globals = Globals;
  // Observables/Subscriptions
  private httpSub$: Subscription = null;
  workerId: string = null;
  allLanguages: any = [];
  filteredAlllanguages:any=[];
  languagesLevel: any = [];
  previeDataFromServer: any = [];
  jobFilterText = { languageName: 'Language Name' };
  proficiencyFilterText = { prof: 'proficiency Level' };
  comingFrom: string;
  disbaleButton:boolean=false;

  // Type boolean variables
  isLoadingResults = false;
  disableOverAll:boolean=false;

  constructor(public service: HttpService, private route: ActivatedRoute, private router: Router) {
    this.workerId = this.route.snapshot.queryParams.spId || this.route.snapshot.queryParams.workerId;
    this.comingFrom = this.route.snapshot.queryParams.from;

  }

  ngOnInit() {
    this.initialService();
  }

  initialService() {
    let spListUrl = this.globals.urls.TradeAdmin.workers.editWorker.getUserLanguageDetails;
    spListUrl = spListUrl.replace(':workerId', this.workerId);
    this.isLoadingResults = true;
    this.httpSub$ = forkJoin(
      this.service.getRequest(this.globals.urls.getAllLanguages),
      this.service.getRequest(this.globals.urls.getLanguageLevels),
      this.service.getRequest(spListUrl),
    )
      .pipe(
        map(([allLanguages, languagesLevel, workerLanguages]) => {
          this.allLanguages = allLanguages.data.languageList
          this.languagesLevel = languagesLevel.data.languageLevelList
          this.previeDataFromServer = workerLanguages.data.languages
        }),
    )
      .subscribe(
        res => {
          this.isLoadingResults = false;
          // this.resultsLength = data.count;
          this.previeDataFromServer.forEach((element, index) => {
            element.disabled = true;
            element.newLanguageName = { label: 'Language Name' };
            element.newProficiencyFilterText = { label: 'proficiency Level' };

            // this.filteredAlllanguages = this.allLanguages.filter(function (value, index, arr) {
            //   return element.languageId._id != value._id;
            // });
            this.allLanguages.forEach((lang, indexLang) => {
              if(element.languageId && lang._id==element.languageId._id){
                this.allLanguages.splice(indexLang,1);
              }
            });

          });
          this.isLoadingResults = false;
        },
        err => {
          this.isLoadingResults = false;
          this.service.showError(err);
        }
      );
  }

  // enable edit
  enableEdit(id, item) {
    this.previeDataFromServer[id].disabled = false;
    this.previeDataFromServer[id].newLanguageName = item.languageId;
    this.previeDataFromServer[id].newProficiencyFilterText = item.proficiencyId;
    this.disbaleButton=true;
    this.disableOverAll=true;
  }
  // select type
  filter(id, job, type) {
    if (type == 1) {
      this.previeDataFromServer[id].newLanguageName = job;
    }
    else if (type == 2) {
      this.previeDataFromServer[id].newProficiencyFilterText = job;
    }

  }

  deleteService(id,job,newlangIndex) {
    if( this.disableOverAll==true && job.isNew==true){
      this.previeDataFromServer.splice(id,1);
      this.disableOverAll=false;
    }
    else{

    let url: string = this.globals.urls.TradeAdmin.workers.editWorker.updateUserLanguages,
     params = {
      "workerId": this.workerId,
      "lngRecordId": job._id,
      "languageId": job.newLanguageName._id,
      "proficiencyId": job.newProficiencyFilterText._id,
      "isDelete":true
    },
      request: any = this.service.putRequest(url, params);

    this.isLoadingResults = true;

    request.subscribe(
      res => {
        this.isLoadingResults = false;
        this.previeDataFromServer.splice(id,1);
        // this.previeDataFromServer[id].languageId = this.previeDataFromServer[id].newLanguageName;
        // this.previeDataFromServer[id].proficiencyId = this.previeDataFromServer[id].newProficiencyFilterText;
        if (res.response === 200) {
          // this.allLanguages.splice(newlangIndex,1);
          this.initialService();
          this.service.showSuccess('Language Deleted Successfully.', 'Worker');
          this.disbaleButton=false;
          this.disableOverAll=false;
        }
        else if (res.response == 400) {
          this.service.showError(res['message'], 'Reasons');
        }
      },
      err => {
        this.service.showError(err.error.message);
        this.isLoadingResults = false;
      },
    );
  }
  }

  addNew() {
    let NewObjToPush = {
      disabled: false,
      serviceHourlyRate: '',
      newLanguageName: { label: 'Language Name' },
      newProficiencyFilterText: { label: 'proficiency Level' },
      languageId: {},
      proficiencyId: {},
      isNew: true,
    }
    this.previeDataFromServer.push(NewObjToPush);
    this.disbaleButton=true;
    this.disableOverAll=true;
  }
  saveSettings(id, job, newlangIndex) {
    this.isLoadingResults = true;
    let params = {
      "workerId": this.workerId,
      "lngRecordId": job._id,
      "languageId": job.newLanguageName._id,
      "proficiencyId": job.newProficiencyFilterText._id,
    }
    if (job.newLanguageName._id == job.languageId._id && job.isNew == true) {
      params.lngRecordId = ''
    }
    else {
      params.lngRecordId = job.newLanguageName._id
    }
    this.service.putRequest(this.globals.urls.TradeAdmin.workers.editWorker.updateUserLanguages, params).subscribe(
      res => {
        this.isLoadingResults = false;
        this.previeDataFromServer[id].disabled = true;
        this.previeDataFromServer[id].languageId = this.previeDataFromServer[id].newLanguageName;
        this.previeDataFromServer[id].proficiencyId = this.previeDataFromServer[id].newProficiencyFilterText;
        if (res.response === 200) {
          this.allLanguages.splice(newlangIndex,1);
          this.service.showSuccess('Language Updated.', 'Worker');
          this.disbaleButton=false;
          this.disableOverAll=false;
        }
        else if (res.response == 400) {
          this.service.showError(res['message'], 'Reasons');
        }
      },
      err => {
        this.service.showError(err.error.message);
      },
    );
  }

  goBackToList() {
    if (this.comingFrom == 'newWorker') {
      this.router.navigate(['/workers/profile', 'new', this.workerId]);
    }
    else if (this.comingFrom == 'oldWorker') {
      this.router.navigate(['/workers/profile', 'current', this.workerId]);
    }
  }

}
