import { EditWorkerModule } from './edit-worker.module';

describe('EditWorkerModule', () => {
  let editWorkerModule: EditWorkerModule;

  beforeEach(() => {
    editWorkerModule = new EditWorkerModule();
  });

  it('should create an instance', () => {
    expect(editWorkerModule).toBeTruthy();
  });
});
