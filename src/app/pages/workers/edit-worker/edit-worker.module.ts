import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { EditWorkerRoutingModule } from "./edit-worker-routing.module";
import { EditWorkerComponent } from "./edit-worker/edit-worker.component";
import { ProfileDetailsComponent } from "./profile-details/profile-details.component";
import { CommonPagesModule } from "../../common-pages/common-pages.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { EditServicesComponent } from "./edit-services/edit-services.component";
import { EditIdentityDocComponent } from "./edit-identity-doc/edit-identity-doc.component";
import { EditCertificatesComponent } from "./edit-certificates/edit-certificates.component";
// import { GdprDirective } from '../../../directives/gdpr.directive';
import { EditPersonalInfoComponent } from "./edit-personal-info/edit-personal-info.component";
import { EditLanguagesComponent } from "./edit-languages/edit-languages.component";
import { SharedPipesModule } from "../../shared-pipes/shared-pipes.module";
import { MatFormFieldModule, MatInputModule } from "@angular/material";
import { EditPortfolioComponent } from './edit-portfolio/edit-portfolio.component';

@NgModule({
  imports: [
    CommonModule,
    CommonPagesModule,
    SharedPipesModule,
    EditWorkerRoutingModule,
    NgbModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  declarations: [
    EditWorkerComponent,
    ProfileDetailsComponent,
    EditServicesComponent,
    EditIdentityDocComponent,
    EditCertificatesComponent,
    EditPersonalInfoComponent,
    EditLanguagesComponent,
    EditPortfolioComponent,
  ],
})
export class EditWorkerModule {}
