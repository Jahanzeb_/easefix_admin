import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Globals, checkIfOnlySpaces } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-edit-identity-doc',
  templateUrl: './edit-identity-doc.component.html',
  styleUrls: ['./edit-identity-doc.component.scss']
})
export class EditIdentityDocComponent implements OnInit {

  // Public functions
  public previewImage: boolean = false;
  public previewImageUrl: string = '';
  globals = Globals;

  // Observables/Subscriptions
  httpSub$: Subscription = null;

  // Type any
  model: any = {};
  localStorage: any;

  @ViewChild('form') form;

  isLoadingResults: boolean = false;

  // variable type null
  fileToUpload: File = null;

  spId: string = null;
  buttonText: string = 'Next';

  viewAsCM: any;
  whenAdminViewPrefix: any;
  comingFrom: string;
  // pattern regex
  public emailPattern = this.globals.regex.email;

  constructor(private httpService: HttpService, private route: ActivatedRoute, private router: Router) {

    // when editing, snapshot params will be required from '/:spId'
    this.spId = this.route.snapshot.queryParams.spId || this.route.snapshot.queryParams.workerId;
    this.comingFrom = this.route.snapshot.queryParams.from;
  }

  ngOnInit() {

    // on step one completed, store spId in localStorage
    if (this.spId) {
      this.isLoadingResults = true;
      let url=this.globals.urls.TradeAdmin.workers.editWorker.getIdDoc;
      url=url.replace(":workerId",this.spId.toString())

      this.httpSub$ = this.httpService.getRequest(url)
        .pipe(
          map(res => res.data),
        )
        .subscribe(
          res => {
            this.isLoadingResults = false;
            if (res) {
              this.model = {
                ...res.identityVerificationDocuments,
                imageString: res.identityVerificationDocuments.identityFront || res.identityVerificationDocuments.identityBack || '',
              }
            }
          },
          err => {
            this.isLoadingResults = false;
            // this.httpService.showError(err);
          }
        );
    }
  }

  // Upload Package Image
  uploadPackageImage(files: FileList) {
    if (files[0].type === 'image/gif') {
      this.httpService.showError('Gif images are not allowed', 'Profile details');
      return;
    }
    this.fileToUpload = files.item(0);
    this.isLoadingResults = true;
    this.httpSub$ = this.httpService.uploadImage(this.globals.urls.uploadImage, this.fileToUpload).subscribe(
      res => {
        this.isLoadingResults = false;
        this.model['imageString'] = res.data.url;
      },
      err => {
        this.isLoadingResults = false;
        // this.httpService.showError(err.error.message)
      },
    )
  }

  countryCodePrefix: any;

  submit() {
    let params = {
      "workerId": this.spId,
      "identityFront": "",
      "identityBack" : ""
    };
    if(this.model.identityFront){
      params.identityFront=this.model.imageString
    }
    else{
      params.identityBack=this.model.imageString
    }

    this.isLoadingResults = true;
    let url=this.globals.urls.TradeAdmin.workers.editWorker.updateIdDoc;
    // url=url.replace(":workerId",this.spId.toString())
    this.httpSub$ = this.httpService.putRequest(url, params)
      .pipe(
        map(res => res.data),
      )
      .subscribe(
        data => {
          this.isLoadingResults = false;
          this.httpService.showSuccess("Worker Identity Documents updated","Worker Updated");
          this.router.navigate(['/workers/edit/certificate-doc'], { queryParams: {spId: this.spId ,  from: this.comingFrom}, queryParamsHandling: 'merge' });
        },
        err => {
          this.isLoadingResults = false;
          this.httpService.showError(err);
        }
      );
  }

  previewImageViewer(image) {
    this.previewImageUrl = image;
    this.previewImage = true;
  }

  closePreview() {
    this.previewImage = false;
  }

  ngOnDestroy(): void {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }

}
