import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditIdentityDocComponent } from './edit-identity-doc.component';

describe('EditIdentityDocComponent', () => {
  let component: EditIdentityDocComponent;
  let fixture: ComponentFixture<EditIdentityDocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditIdentityDocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditIdentityDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
