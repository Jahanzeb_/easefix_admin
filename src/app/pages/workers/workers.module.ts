import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonPagesModule } from '../common-pages/common-pages.module';
import { WorkersRoutingModule } from './workers-routing.module';
import { WorkersComponent } from './workers.component';
import { WorkerProfileComponent } from './worker-profile/worker-profile.component';
import { WorkerJobHistoryComponent } from './worker-job-history/worker-job-history.component';
import { MatFormFieldModule, MatInputModule, MatToolbarModule } from '@angular/material';
import { WorkerPackageDetailComponent } from './worker-package-detail/worker-package-detail.component';
import { AgmMapOrignalModule } from '../agm-map-orignal/agm-map-orignal.module';
import { SharedPipesModule } from '../shared-pipes/shared-pipes.module';
// import { GdprDirective } from '../../directives/gdpr.directive';
import { CommisionCheckComponent } from './worker-profile/commision-check/commision-check.component';
import { BlockWorkerComponent } from './block-worker/block-worker.component';
import { WorkerFeedbackComponent } from './worker-feedback/worker-feedback.component';
// import { FeedbackListingComponent } from '../modals/feedback-listing/feedback-listing.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
@NgModule({
  imports: [
    CommonModule,
    WorkersRoutingModule,
    CommonPagesModule,
    MatToolbarModule,
    SharedPipesModule,
    AgmMapOrignalModule,
    CommonPagesModule,
    MatToolbarModule,
    AgmMapOrignalModule,
    SharedPipesModule,
    MatFormFieldModule,
    MatInputModule,
    NgbModule
  ],
  declarations: [WorkersComponent, WorkerProfileComponent, WorkerJobHistoryComponent, WorkerPackageDetailComponent, CommisionCheckComponent, BlockWorkerComponent, WorkerFeedbackComponent],
  entryComponents: [CommisionCheckComponent, BlockWorkerComponent, WorkerFeedbackComponent]
})
export class WorkersModule { }
