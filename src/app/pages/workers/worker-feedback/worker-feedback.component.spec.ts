import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerFeedbackComponent } from './worker-feedback.component';

describe('WorkerFeedbackComponent', () => {
  let component: WorkerFeedbackComponent;
  let fixture: ComponentFixture<WorkerFeedbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkerFeedbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkerFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
