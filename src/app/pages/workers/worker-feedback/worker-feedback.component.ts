import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Globals } from "src/app/Globals";
@Component({
  selector: "app-worker-feedback",
  templateUrl: "./worker-feedback.component.html",
  styleUrls: ["./worker-feedback.component.scss"],
})
export class WorkerFeedbackComponent implements OnInit {
  ratingsFound: any = [];
  globals = Globals;
  constructor(
    public dialogRef: MatDialogRef<WorkerFeedbackComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.ratingsFound = this.data.ratings;
  }
}
