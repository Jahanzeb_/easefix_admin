import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockWorkerComponent } from './block-worker.component';

describe('BlockWorkerComponent', () => {
  let component: BlockWorkerComponent;
  let fixture: ComponentFixture<BlockWorkerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockWorkerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockWorkerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
