import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  OnDestroy,
  ElementRef,
} from "@angular/core";
import { MatTableDataSource, MatPaginator } from "@angular/material";
import { Globals } from "src/app/Globals";
import { HttpService } from "src/app/services/http-service";
import {
  map,
  switchMap,
  startWith,
  debounceTime,
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import { Subscription, fromEvent, forkJoin } from "rxjs";
// import { Subscription, forkJoin } from 'rxjs';
import { Router } from "@angular/router";
import { ExcelService } from "src/app/services/excel.service";

@Component({
  selector: "app-verified-list",
  templateUrl: "./verified-list.component.html",
  styleUrls: ["./verified-list.component.scss"],
})
export class VerifiedListComponent implements OnInit {
  // Public Depedency
  public globals = Globals;

  // Observables/Subscriptions
  private httpSub$: Subscription = null;

  // Public arrays / boolean / any / number
  public drivers: any[];
  public listing: boolean = true;
  public newListing: boolean = false;
  dataToSend: any;
  driversExportData: any = [];

  // Table settings
  displayedColumns = [];
  // dataSource: MatTableDataSource<any>;
  dataSource: any = [];
  vehicleTypes = [];

  // Type number
  selectedVehicleTypeId: number; // selected vehicle type id from dropdown

  // Type boolean variables
  isLoadingResults = true;

  // Type string variables
  name: string;
  jobFilterText = { serviceName: "Service", value: "", _id: "" };
  statusFilterText = { name: "Status", value: "" };
  jobStatusText = [
    { name: "Verified", value: true },
    { name: "Unverified", value: false },
  ];
  inputString: string;
  // global limit and offset by default for exports
  offset: number = 0;
  limit: number = 100;
  jobType: any = [];
  previeDataFromServer: any = [];

  // Type number variables
  resultsLength: number = 10;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  totalRecords: number = 0;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("input") input: ElementRef;

  localStorage: any;
  constructor(
    public service: HttpService,
    private router: Router,
    private excelService: ExcelService
  ) {}

  ngOnInit() {
    this.displayedColumns = [
      "Time",
      "Photo",
      "Name",
      "Job",
      "phone",
      "email",
      "status",
      "action",
    ];
  }
  ngAfterViewInit() {
    this.getListOnstart(0, 10);

    this.httpSub$ = fromEvent(this.input.nativeElement, "keyup")
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.inputString = this.input.nativeElement.value;
          this.search({ searchText: this.input.nativeElement.value });
        })
      )
      .subscribe();

    // this.httpSub$ = this.paginator.page
    //   .pipe(
    //     startWith({}),
    //     switchMap(() => {
    //       let params = {
    //         offset: ((this.paginator.pageIndex + 1) * this.paginator.pageSize) - this.paginator.pageSize,
    //         limit: this.paginator.pageSize,
    //         statusFilter: 0,
    //       };

    //       return this.service.postRequest(this.globals.urls.driver.getDrivers, params);
    //     }),
    //     map(res => {
    //       return res.data;
    //     }),
    //   ).subscribe(
    //     data => {
    //       this.isLoadingResults = false;
    //       this.resultsLength = data.count;
    //       this.dataSource = data.drivers;
    //       this.totalRecords = data.drivers ? data.drivers.length : 0;

    //     },
    //     err => {
    //       this.dataSource = null;
    //       this.isLoadingResults = false;
    //       this.service.showError(err.error.message);
    //     }
    //   );
  }
  pagination(event) {
    this.getListOnstart(event.pageIndex * event.pageSize, event.pageSize);
  }
  getListOnstart(pageIndex, limit) {
    this.isLoadingResults = true;
    // let offset = ((this.paginator.pageIndex + 1) * this.paginator.pageSize) - this.paginator.pageSize;
    // let limit = this.paginator.pageSize;
    let spListUrl = this.globals.urls.TradeAdmin.workers.fetchNewSpUser;
    spListUrl = spListUrl.replace(":limit", limit.toString());
    spListUrl = spListUrl.replace(":offset", pageIndex.toString());
    spListUrl = spListUrl.replace(":search_text", "");
    spListUrl = spListUrl.replace(":status", "");
    spListUrl = spListUrl.replace(":jobTypeId", "");
    // spListUrl = spListUrl.replace(':status','');

    this.httpSub$ = forkJoin(
      this.service.getRequest(
        this.globals.urls.TradeAdmin.workers.fetchJobTypes
      ),
      this.service.getRequest(spListUrl)
    )
      .pipe(
        map(([jobTypes, splist]) => {
          this.jobType = jobTypes.data.servicesList;
          this.previeDataFromServer = splist.data;
        })
      )
      .subscribe(
        (res) => {
          this.isLoadingResults = false;
          // this.resultsLength = data.count;
          this.previeDataFromServer.respObj.forEach((element, index) => {
            if (element.status == true) {
              element.statusText = "Verified";
            } else {
              element.statusText = "unverified";
            }
            if (element.rejectdStatus) {
              element.statusText = "Rejected";
            }
          });
          this.dataSource = this.previeDataFromServer.respObj;
          this.isLoadingResults = false;
          this.resultsLength = this.previeDataFromServer.count;
          // this.dataSource = data.drivers;
          this.totalRecords = this.previeDataFromServer.respObj
            ? this.previeDataFromServer.respObj.length
            : 0;
        },
        (err) => {
          this.isLoadingResults = false;
          this.service.showError(err);
        }
      );
  }

  filter(job, typeId) {
    this.paginator.pageIndex = 0;
    if (typeId == 1) {
      this.jobFilterText = job;
      // this.selectedVehicleTypeId = job.id;
      this.search({ jobTypeId: job._id });
    } else {
      this.statusFilterText = job;
      // this.selectedVehicleTypeId = job.id;
      this.search({ statusType: job.value.toString() });
    }
  }

  search(input) {
    this.isLoadingResults = true;
    this.isLoadingResults = true;
    let offset =
      (this.paginator.pageIndex + 1) * this.paginator.pageSize -
      this.paginator.pageSize;
    let limit = this.paginator.pageSize;
    let spListUrl = this.globals.urls.TradeAdmin.workers.fetchNewSpUser;
    spListUrl = spListUrl.replace(":limit", limit.toString());
    spListUrl = spListUrl.replace(":offset", offset.toString());
    // spListUrl = spListUrl.replace(':search_text', input.searchText || this.inputString || '');
    // spListUrl = spListUrl.replace(':status',input.statusType||'');
    // spListUrl = spListUrl.replace(':jobTypeId',input.jobTypeId||'');
    spListUrl = spListUrl.replace(
      ":search_text",
      input.searchText || this.inputString || ""
    );
    spListUrl = spListUrl.replace(
      ":status",
      this.statusFilterText.value.toString() || input.statusType || ""
    );
    spListUrl = spListUrl.replace(
      ":jobTypeId",
      this.jobFilterText._id.toString() || input.jobTypeId || ""
    );
    // spListUrl = spListUrl.replace(':status','');

    this.service
      .getRequest(spListUrl)
      .pipe(
        map((res) => {
          return res.data;
        })
      )
      .subscribe(
        (data) => {
          this.previeDataFromServer = data.respObj;
          this.previeDataFromServer.forEach((element, index) => {
            if (element.status == true) {
              element.statusText = "Verified";
            } else {
              element.statusText = "unverified";
            }
          });
          this.dataSource = this.previeDataFromServer;
          this.isLoadingResults = false;
          this.resultsLength = data.count;
          // this.dataSource = data.drivers;
          this.totalRecords = data.respObj ? data.respObj.length : 0;
        },
        (err) => {
          this.isLoadingResults = false;
          if (err.error) this.service.showError(err.error.message);
          else this.service.showError(err);
        }
      );
  }

  // filterByVehicle(vehicle) {
  //   this.jobFilterText = vehicle;
  //   this.selectedVehicleTypeId = vehicle.id;
  //   this.search({ vehicleTypeId: this.selectedVehicleTypeId});
  // }

  // get data for exporting all drivers
  getDriversExportData(offset) {
    this.dataToSend = {
      limit: this.limit,
      offset: offset || 0,
    };

    // this.httpSub$ = this.service.postRequest(this.globals.urls.post.driversExportsData, this.dataToSend).subscribe(
    //   res => {
    //     if (res.response === 200) {

    //       let driversData = res.data.usersData.rows;

    //       if (driversData.length > 0) {

    //         driversData.forEach(singleUser => {
    //           this.driversExportData.push(singleUser);
    //         });

    //         if (driversData.count > this.limit) {
    //           this.getDriversExportData(this.limit);
    //         }
    //       }
    //     }

    //     else if (res.response == 400) {
    //       this.service.showError(res['message'], 'Dashboard Statistics');
    //     }
    //   },
    //   err => {
    //     this.service.showError(err.error.message);
    //   },
    // );
  }

  // export file
  exportAsXLSX(): void {
    this.dataToSend = {};
    this.dataToSend = this.driversExportData;
    this.excelService.exportAsExcelFile(this.dataToSend, "Drivers");
  }

  ngOnDestroy(): void {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }
}
