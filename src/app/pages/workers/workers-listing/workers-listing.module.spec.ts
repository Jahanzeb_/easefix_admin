import { WorkersListingModule } from './workers-listing.module';

describe('WorkersListingModule', () => {
  let workersListingModule: WorkersListingModule;

  beforeEach(() => {
    workersListingModule = new WorkersListingModule();
  });

  it('should create an instance', () => {
    expect(workersListingModule).toBeTruthy();
  });
});
