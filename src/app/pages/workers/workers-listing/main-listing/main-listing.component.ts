import { Component, OnInit } from "@angular/core";
import { of as observableOf, fromEvent, Subscription, forkJoin } from "rxjs";
import { HttpService } from "../../../../services/http-service";
import { Globals } from "../../../../Globals";

@Component({
  selector: "app-main-listing",
  templateUrl: "./main-listing.component.html",
  styleUrls: ["./main-listing.component.scss"],
})
export class MainListingComponent implements OnInit {
  constructor(public service: HttpService) {}
  activeDriversCount = 0;
  verifiedCount = 0;
  pendingCertificateCount = 0;
  public globals = Globals;
  // boolean
  isLoadingResults = false;
  // Observables/Subscriptions
  private httpSub$: Subscription = null;

  ngOnInit() {
    this.getCount();
  }

  getCount() {
    this.isLoadingResults = true;

    this.httpSub$ = this.service
      .getRequest(this.globals.urls.TradeAdmin.workers.getNewCount)
      .subscribe(
        (res) => {
          this.isLoadingResults = false;
          this.activeDriversCount = res.data.count;
          this.verifiedCount = res.data.verifiedCount;
          this.pendingCertificateCount = res.data.pendingCertificateCount;
        },
        (err) => {
          this.isLoadingResults = false;
          this.service.showError(err);
        }
      );
  }
}
