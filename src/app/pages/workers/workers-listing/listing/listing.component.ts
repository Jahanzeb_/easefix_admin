import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  OnDestroy,
  ElementRef,
} from "@angular/core";
import { MatTableDataSource, MatPaginator, PageEvent } from "@angular/material";
// import {PageEvent} from '@angular/material';
import { Globals } from "src/app/Globals";
import { HttpService } from "src/app/services/http-service";
import {
  map,
  switchMap,
  startWith,
  debounceTime,
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import { Subscription, fromEvent, forkJoin } from "rxjs";
// import { Subscription, forkJoin } from 'rxjs';
import { Router } from "@angular/router";
import { ExcelService } from "src/app/services/excel.service";
import { Sort } from "@angular/material/sort";
import * as moment from "moment";
import * as XLSX from 'xlsx';

@Component({
  selector: "app-listing",
  templateUrl: "./listing.component.html",
  styleUrls: ["./listing.component.scss"],
})
export class ListingComponent implements OnInit {
  // Public Depedency
  public globals = Globals;

  // Observables/Subscriptions
  private httpSub$: Subscription = null;

  // Public arrays / boolean / any / number
  public drivers: any[];
  public listing: boolean = true;
  public newListing: boolean = false;
  dataToSend: any;
  driversExportData: any = [];
  // MatPaginator Output
  pageEvent: PageEvent;
  // Table settings
  displayedColumns = [];
  // dataSource: MatTableDataSource<any>;
  dataSource: any = [];
  vehicleTypes = [];

  // Type number
  selectedVehicleTypeId: number; // selected vehicle type id from dropdown

  // Type boolean variables
  isLoadingResults = true;

  // Type string variables
  name: string;
  jobFilterText = { serviceName: "Service", value: "", _id: "" };
  statusFilterText = { name: "Verification", value: "" };
  jobStatusText = [
    { name: "All", value: "" },
    { name: "Verified", value: true },
    { name: "Unverified", value: false },
  ];
  blockFilterText = { name: "Status", value: "" };

  blockStatus = [
    { name: "All", value: "" },
    { name: "Blocked", value: true },
    { name: "Active", value: false },
  ];

  accountTypeText = { name: "Account", value: "" };

  accountStatus = [
    { name: "All", value: "" },
    { name: "Company", value: true },
    { name: "Individual", value: false },
  ];

  importText = { name: "Imported", value: "" };

  importedStatus = [
    { name: "All", value: "" },
    { name: "Yes", value: true },
    { name: "No", value: false },
  ];
  inputString: string;
  // global limit and offset by default for exports
  offset: number = 0;
  limit: number = 100;
  jobType: any = [];
  previeDataFromServer: any = [];

  // Type number variables
  resultsLength: number = 10;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  totalRecords: number = 0;
  showData: any = {
    mobile: false,
    email: false,
  };
  showEye: any = {
    mobile: true,
    email: true,
  };

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("input") input: ElementRef;
  workerArray: any = []

  localStorage: any;

  constructor(
    public service: HttpService,
    private router: Router,
    private excelService: ExcelService
  ) { }

  ngOnInit() {
    this.displayedColumns = [
      "photo",
      "name",
      "job",
      "phone",
      "email",
      "status",
      "joining",
      "approval",
      "commission",
      "rating",
      "account",
      "action",
    ];
    //initial Data Call

    // Fetch data for xcel sheet USERS
    this.getDriversExportData(this.offset);

    // For region ID

    if (JSON.parse(localStorage.getItem("TradeMen-admin-data"))) {
      this.localStorage = JSON.parse(
        localStorage.getItem("TradeMen-admin-data")
      );
    }

    // this.httpSub$ = this.service.getRequest(this.globals.urls.get.vehicleTypes)
    //   .pipe(
    //     map(res => res.data.vehicleTypes.filter(elem => elem.status === 1)),
    //   )
    //   .subscribe(
    //     res => {
    //       this.vehicleTypes = res;
    //     },
    //     err => {
    //       this.service.showError(err.error.message);
    //     }
    //   )
  }

  editWorker(id) {
    this.router.navigate(["/workers/edit"], {
      queryParams: { workerId: id, from: "oldWorker" },
      queryParamsHandling: "merge",
    });
  }

  ngAfterViewInit() {
    this.httpSub$ = fromEvent(this.input.nativeElement, "keyup")
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.inputString = this.input.nativeElement.value;
          this.search({ searchText: this.input.nativeElement.value });
        })
      )
      .subscribe();

    this.getListOnstart(0, 10);

    // this.httpSub$ = this.paginator.page
    //   .pipe(
    //     startWith({}),
    //     switchMap(() => {
    //       let params = {
    //         offset: ((this.paginator.pageIndex + 1) * this.paginator.pageSize) - this.paginator.pageSize,
    //         limit: this.paginator.pageSize,
    //         statusFilter: 0,
    //       };

    //       return this.service.postRequest(this.globals.urls.driver.getDrivers, params);
    //     }),
    //     map(res => {
    //       return res.data;
    //     }),
    //   ).subscribe(
    //     data => {
    //       this.isLoadingResults = false;
    //       this.resultsLength = data.count;
    //       this.dataSource = data.drivers;
    //       this.totalRecords = data.drivers ? data.drivers.length : 0;

    //     },
    //     err => {
    //       this.dataSource = null;
    //       this.isLoadingResults = false;
    //       this.service.showError(err.error.message);
    //     }
    //   );
  }

  Enablegdpr(from, index) {
    this.isLoadingResults = true;
    if (from == "mobile") {
      this.showData.mobile = true;
    } else if (from == "email") {
      this.showData.email = true;
    }
  }

  GotGDPRDATA(event) {
    // this.isLoadingResults = false;
    // if(event.data.phoneNumber && event.data.phonePreFix){
    //   this.previewData.spProfile.phone=event.data.phonePreFix.toString() + event.data.phoneNumber.toString()
    //   this.showEye.mobile=false;
    // }
    // else if(event.data.email){
    //   this.previewData.spProfile.email=event.data.email
    //   this.showEye.email=false;
    // }
    console.log("I got this", event);
  }
  pagination(event) {
    this.getListOnstart(event.pageIndex * event.pageSize, event.pageSize);
  }

  getListOnstart(pageIndex, limit) {
    this.isLoadingResults = true;
    // let offset = ((this.paginator.pageIndex + 1) * this.paginator.pageSize) - this.paginator.pageSize;
    // let limit = this.paginator.pageSize;
    let spListUrl = this.globals.urls.TradeAdmin.workers.fetchSPList;
    spListUrl = spListUrl.replace(":limit", limit.toString());
    spListUrl = spListUrl.replace(":offset", pageIndex.toString());
    // spListUrl = spListUrl.replace(':search_text', '');
    spListUrl = spListUrl.replace(":status", this.statusFilterText.value);
    spListUrl = spListUrl.replace(":jobTypeId", this.jobFilterText._id);
    spListUrl = spListUrl.replace(":search_text", this.inputString || "");
    spListUrl = spListUrl.replace(
      ":status",
      this.statusFilterText.value.toString() || ""
    );
    spListUrl = spListUrl.replace(
      ":jobTypeId",
      this.jobFilterText._id.toString() || ""
    );
    spListUrl = spListUrl.replace(
      ":isBlocked",
      this.blockFilterText.value.toString() || ""
    );
    spListUrl = spListUrl.replace(
      ":accountType",
      this.accountTypeText.value.toString() || ""
    );
    spListUrl = spListUrl.replace(
      ":isAddedBySheet",
      this.importText.value.toString() || ""
    );
    // spListUrl = spListUrl.replace(':status','');

    this.httpSub$ = forkJoin(
      this.service.getRequest(
        this.globals.urls.TradeAdmin.workers.fetchJobTypes
      ),
      this.service.getRequest(spListUrl)
    )
      .pipe(
        map(([jobTypes, splist]) => {
          this.jobType = jobTypes.data.servicesList;
          this.previeDataFromServer = splist.data;

          this.jobType.unshift({
            serviceName: "All",
            _id: "",
          });
        })
      )
      .subscribe(
        (res) => {
          this.isLoadingResults = false;
          this.previeDataFromServer.respObj.forEach((element, index) => {
            if (element.status == true) {
              element.statusText = "Verified";
            } else {
              element.statusText = "unverified";
            }
          });
          this.dataSource = this.previeDataFromServer.respObj;
          this.dataToSend = this.previeDataFromServer.respObj;
          this.isLoadingResults = false;
          this.resultsLength = this.previeDataFromServer.count;
          // this.dataSource = data.drivers;
          this.totalRecords = this.previeDataFromServer.respObj
            ? this.previeDataFromServer.respObj.length
            : 0;
        },
        (err) => {
          this.isLoadingResults = false;
          this.service.showError(err);
        }
      );
  }

  filter(job, typeId) {
    this.paginator.pageIndex = 0;
    if (typeId == 1) {
      this.jobFilterText = job;
      // this.selectedVehicleTypeId = job.id;
      this.search({ jobTypeId: job._id });
    } else if (typeId == 2) {
      this.statusFilterText = job;
      // this.selectedVehicleTypeId = job.id;
      this.search({ statusType: job.value && job.value.toString() || '' });
    } else if (typeId == 3) {
      this.blockFilterText = job;
      this.search({ isBlocked: job.value && job.value.toString() || '' });
    } else if(typeId == 4) {
      this.accountTypeText = job;
      this.search({ accountType: job.value && job.value.toString() || '' })
    } else{
      this.importText = job;
      this.search({ isAddedBySheet: job.value && job.value.toString() || '' })
    }
  }

  search(input) {
    this.isLoadingResults = true;
    this.isLoadingResults = true;
    let offset =
      (this.paginator.pageIndex + 1) * this.paginator.pageSize -
      this.paginator.pageSize;
    let limit = this.paginator.pageSize;
    let spListUrl = this.globals.urls.TradeAdmin.workers.fetchSPList;
    spListUrl = spListUrl.replace(":limit", limit.toString());
    spListUrl = spListUrl.replace(":offset", offset.toString());
    spListUrl = spListUrl.replace(
      ":search_text",
      input.searchText || this.inputString || ""
    );
    spListUrl = spListUrl.replace(
      ":status",
      this.statusFilterText.value.toString() || input.statusType || ""
    );
    spListUrl = spListUrl.replace(
      ":jobTypeId",
      this.jobFilterText._id.toString() || input.jobTypeId || ""
    );
    spListUrl = spListUrl.replace(
      ":isBlocked",
      this.blockFilterText.value.toString() || input.isBlocked || ""
    );
    spListUrl = spListUrl.replace(
      ":accountType",
      this.accountTypeText.value.toString() || input.accountType || ""
    );
    spListUrl = spListUrl.replace(
      ":isAddedBySheet",
      this.importText.value.toString() || input.isAddedBySheet || ""
    );
    // spListUrl = spListUrl.replace(':status','');

    this.service
      .getRequest(spListUrl)
      .pipe(
        map((res) => {
          return res.data;
        })
      )
      .subscribe(
        (data) => {
          this.previeDataFromServer = data.respObj;
          this.previeDataFromServer.forEach((element, index) => {
            if (element.status == true) {
              element.statusText = "Verified";
            } else {
              element.statusText = "unverified";
            }
          });
          this.dataSource = this.previeDataFromServer;
          this.dataToSend = this.previeDataFromServer;
          this.isLoadingResults = false;
          this.resultsLength = data.count;
          // this.dataSource = data.drivers;
          this.totalRecords = data.respObj ? data.respObj.length : 0;
        },
        (err) => {
          this.isLoadingResults = false;
          if (err.error) this.service.showError(err.error.message);
          else this.service.showError(err);
        }
      );
  }

  // filterByVehicle(vehicle) {
  //   this.jobFilterText = vehicle;
  //   this.selectedVehicleTypeId = vehicle.id;
  //   this.search({ vehicleTypeId: this.selectedVehicleTypeId});
  // }

  // get data for exporting all drivers
  getDriversExportData(offset) {
    this.dataToSend = {
      limit: this.limit,
      offset: offset || 0,
    };

    // this.httpSub$ = this.service.postRequest(this.globals.urls.post.driversExportsData, this.dataToSend).subscribe(
    //   res => {
    //     if (res.response === 200) {

    //       let driversData = res.data.usersData.rows;

    //       if (driversData.length > 0) {

    //         driversData.forEach(singleUser => {
    //           this.driversExportData.push(singleUser);
    //         });

    //         if (driversData.count > this.limit) {
    //           this.getDriversExportData(this.limit);
    //         }
    //       }
    //     }

    //     else if (res.response == 400) {
    //       this.service.showError(res['message'], 'Dashboard Statistics');
    //     }
    //   },
    //   err => {
    //     this.service.showError(err.error.message);
    //   },
    // );
  }

  // export file
  exportAsXLSX(): void {
    this.dataToSend.forEach((element) => {
      let obj = {
        Name: element.name,
        Job:
          (element.job &&
            element.job.serviceId &&
            element.job.serviceId.serviceName) ||
          "",
        Mobile: element.phonee,
        Email: element.emaill,
        Status: element.statusText || "",
        "Joining Date": moment(element.createdAt).format("MM/DD/YYYY"),
        "Approval Date": moment.unix(element.approvalDate).format("MM/DD/YYYY"),
        Commission: element.commissionPercentage,
      };
      this.driversExportData.push(obj);
    });

    this.excelService.exportAsExcelFile(this.driversExportData, "Workers");
    this.driversExportData = [];
  }

  ngOnDestroy(): void {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }

  sortData(sort: Sort) {
    const data = this.dataSource.slice();
    // if (!sort.active || sort.direction === '') {
    //   this.sortedData = data;
    //   return;
    // }

    this.dataSource = data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "commission":
          return compare(a.commissionPercentage, b.commissionPercentage, isAsc);
        case "approval":
          return compare(a.approvalDate, b.approvalDate, isAsc);
        case "joining":
          return compare(
            moment(a.createdAt).unix(),
            moment(b.createdAt).unix(),
            isAsc
          );
        case "rating":
          return compare(a.avgRating, b.avgRating, isAsc);
        default:
          return 0;
      }
    });

    this.dataToSend = [...this.dataSource];
  }

  uploadWorker(files: FileList) {
    if (files && files.length > 0) {
      let file: File = files.item(0);
      let reader: FileReader = new FileReader();
      reader.readAsText(file);
      reader.onload = (e) => {
        this.isLoadingResults = true;
        let csv: string = reader.result as string;
        let csvToRowArray = csv.split("\n");
        for (let index = 1; index < csvToRowArray.length; index++) {
          let row = csvToRowArray[index].split(",");
          this.workerArray.push({
            firstName: row[0], lastName: row[1], phonePreFix: row[2], phoneNumber: row[3],
            name: row[0] + ' ' + row[1],
            email: row[4], serviceName: row[5], postalCode: row[6], accountType: row[7],
            companyName: row[8] || ''
          });
        }
        console.log(this.workerArray);

      }
      reader.onloadend = (e) => {
        let params = {
          workers: this.workerArray
        }
        this.service
          .postRequest(this.globals.urls.TradeAdmin.workers.addWorkersBulk, params)
          .subscribe(
            (res) => {
              this.isLoadingResults = false;
              this.service.showSuccess(res.message, "Workers");
            },
            (err) => {
              this.isLoadingResults = false;
              if (err.error) this.service.showError(err.error.message);
              else this.service.showError(err);
            }
          );
      }
    }
  }

  onFileChange(event: any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(event.target);
    if (target.files.length !== 1) {
      throw new Error('Cannot use multiple files');
    }
    const reader: FileReader = new FileReader();
    reader.readAsBinaryString(target.files[0]);
    reader.onload = (e: any) => {
      /* create workbook */
      const binarystr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(binarystr, { type: 'binary' });

      /* selected the first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.workerArray = XLSX.utils.sheet_to_json(ws); // to get 2d array pass 2nd parameter as object {header: 1}
    };

    reader.onloadend = (e) => {
      let params = {
        workers: this.workerArray
      }
      console.log(params);
      return;
      this.service
        .postRequest(this.globals.urls.TradeAdmin.workers.addWorkersBulk, params)
        .subscribe(
          (res) => {
            this.service.showSuccess(res.message, "Workers");
          },
          (err) => {
            this.isLoadingResults = false;
            if (err.error) this.service.showError(err.error.message);
            else this.service.showError(err);
          }
        );
    }
 }

}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
