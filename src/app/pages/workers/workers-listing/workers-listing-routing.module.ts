import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MainListingComponent } from "./main-listing/main-listing.component";
import { ListingComponent } from "./listing/listing.component";
import { NewRequestsComponent } from "./new-requests/new-requests.component";
import { RouterGuard } from "../../../guards/route.guard";
import { VerifiedListComponent } from "./verified-list/verified-list.component";
import { PendingCertificatesComponent } from "./pending-certificates/pending-certificates.component";

const routes: Routes = [
  {
    path: "",
    component: MainListingComponent,
    canActivate: [RouterGuard],
    data: { workers: 1 },
    children: [
      {
        path: "listing",
        component: ListingComponent,
      },
      {
        path: "new-requests",
        component: NewRequestsComponent,
      },
      {
        path: "",
        redirectTo: "listing",
        pathMatch: "full,",
      },
      {
        path: "verified-workers",
        component: VerifiedListComponent,
      },
      {
        path: "pending-certificate",
        component: PendingCertificatesComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorkersListingRoutingModule {}
