import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainListingComponent } from './main-listing/main-listing.component';
import { ListingComponent } from './listing/listing.component';
import { CommonPagesModule } from '../../common-pages/common-pages.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatToolbarModule } from '@angular/material';
import { WorkersListingRoutingModule } from './workers-listing-routing.module';
import { NewRequestsComponent } from './new-requests/new-requests.component';
import { SharedPipesModule } from '../../shared-pipes/shared-pipes.module';
import { VerifiedListComponent } from './verified-list/verified-list.component';
import { PendingCertificatesComponent } from './pending-certificates/pending-certificates.component';

@NgModule({
  imports: [
    CommonModule,
    WorkersListingRoutingModule,
    NgbModule,
    MatToolbarModule,
    SharedPipesModule,
    CommonPagesModule,
  ],
  declarations: [MainListingComponent, ListingComponent, NewRequestsComponent, VerifiedListComponent, PendingCertificatesComponent]
})
export class WorkersListingModule { }
