import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-price-breakdown-dialog',
  templateUrl: './price-breakdown-dialog.component.html',
  styleUrls: ['./price-breakdown-dialog.component.scss']
})

export class PriceBreakdownDialogComponent {
  public realItems: any;
  constructor(public dialog: MatDialog,
    public dialogRef: MatDialogRef<PriceBreakdownDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
      this.realItems = data;
    }
    
    onNoClick(): void {
        this.dialogRef.close();
    }

}
