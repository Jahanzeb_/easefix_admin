import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PriceBreakdownDialogComponent } from './price-breakdown-dialog.component';

describe('PriceBreakdownDialogComponent', () => {
  let component: PriceBreakdownDialogComponent;
  let fixture: ComponentFixture<PriceBreakdownDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PriceBreakdownDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceBreakdownDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
