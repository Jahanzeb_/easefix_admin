import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobsRoutingModule } from './jobs-routing.module';
import { JobsListingComponent } from './jobs-listing/jobs-listing.component';
import { JobDetailsComponent } from './job-details/job-details.component';
import { MatTableModule, MatPaginatorModule, MatToolbarModule, MatProgressSpinnerModule, MatCardModule, MatExpansionModule, MatSelectModule } from '@angular/material';
import { AgmMapOrignalModule } from '../agm-map-orignal/agm-map-orignal.module';
import { SharedPipesModule } from '../shared-pipes/shared-pipes.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PriceBreakdownDialogComponent } from './price-breakdown-dialog/price-breakdown-dialog.component';
import { CommonPagesModule } from '../common-pages/common-pages.module';
import { MessagesListComponent } from './messages-list/messages-list.component';
import { JobEditComponent } from './job-edit/job-edit.component';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

@NgModule({
  imports: [
    CommonModule,
    CommonPagesModule,
    MatTableModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatExpansionModule,
    NgbModule,
    AgmMapOrignalModule,
    SharedPipesModule,
    JobsRoutingModule,
    GooglePlaceModule,
    MatSelectModule
  ],
  declarations: [JobsListingComponent, JobDetailsComponent, PriceBreakdownDialogComponent, MessagesListComponent, JobEditComponent],
  entryComponents: [PriceBreakdownDialogComponent]
})
export class JobsModule { }
