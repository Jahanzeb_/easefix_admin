import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Globals } from "src/app/Globals";
import { HttpService } from "src/app/services/http-service";

@Component({
  selector: "app-messages-list",
  templateUrl: "./messages-list.component.html",
  styleUrls: ["./messages-list.component.scss"],
})
export class MessagesListComponent implements OnInit {
  messages: any = [];
  workers: any = [];
  selected = '';
  isLoadingResults = false;
  globals = Globals;
  jobId = '';
  workerId = '';

  constructor(
    public dialogRef: MatDialogRef<MessagesListComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public service: HttpService,

  ) { }

  ngOnInit() {
    this.messages = this.data && this.data.messages || [];
    console.log('this.messages', this.messages);
    this.workers = this.data && this.data.workers || [];
    this.jobId = this.data && this.data.jobId || '';
  }

  onChange(e) {
    this.workerId = e.value;
    this.fetchChatHistory();
  }

  fetchChatHistory() {
    let url = this.globals.urls.map.viewChat.replace(":jobId", this.jobId);
    url = url.replace(':workerId', this.workerId)

    this.service.getRequest(url).subscribe(
      res => {
        this.isLoadingResults = false;
        this.messages = res.data.chatHistory;
        // let messages = [{ name: 'Ahmed', time: '2 days ago', message: 'testing', senderType: 2 }]
        // this.messages = messages
      },
      err => {
        this.isLoadingResults = false;
        this.service.showError(err);
      }
    );
  }
  ngOnDestroy() {
    console.log('Destroying chat history')
    this.workerId = ''
  }
}
