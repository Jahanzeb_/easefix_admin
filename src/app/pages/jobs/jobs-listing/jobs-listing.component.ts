import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  OnDestroy,
  ElementRef,
} from "@angular/core";
import {
  MatTableDataSource,
  MatPaginator,
  Sort,
  MatDatepicker,
} from "@angular/material";
import { Globals } from "src/app/Globals";
import { HttpService } from "src/app/services/http-service";
import {
  map,
  switchMap,
  startWith,
  debounceTime,
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import { Subscription, fromEvent, forkJoin } from "rxjs";
// import { Subscription, forkJoin } from 'rxjs';
import { ActivatedRoute, Router } from "@angular/router";
import { ExcelService } from "src/app/services/excel.service";
import * as moment from "moment";
import { e } from "@angular/core/src/render3";

@Component({
  selector: "app-jobs-listing",
  templateUrl: "./jobs-listing.component.html",
  styleUrls: ["./jobs-listing.component.scss"],
})
export class JobsListingComponent implements OnInit {
  // Public Depedency
  public globals = Globals;
  // Observables/Subscriptions
  private httpSub$: Subscription = null;

  // Public arrays / boolean / any / number
  public drivers: any[];
  public listing: boolean = true;
  public newListing: boolean = false;
  dataToSend: any;
  driversExportData: any = [];
  hideButton: number = 0;
  // Table settings
  displayedColumns = [];
  // dataSource: MatTableDataSource<any>;
  dataSource: any = [];
  vehicleTypes = [];

  // Type number
  selectedVehicleTypeId: number; // selected vehicle type id from dropdown

  // Type boolean variables
  isLoadingResults = true;

  // Type string variables
  name: string;
  jobFilterText = { serviceName: "Service", value: "", _id: "" };
  statusFilterText = { name: "Status", value: "" };

  jobStatusText = [
    { name: "All", value: "" },
    { name: "Open", value: 1 },
    { name: "Assigned", value: 2 },
    { name: "InProgress", value: 3 },
    { name: "Completed", value: 4 },
    // { name: 'Rated By User', value: 5 },`
    { name: "Cancellation", value: 6 },
    { name: "Rejected", value: 7 },
    { name: "Time Out", value: 8 },
    { name: "Disputed", value: 9 },
    ``,
  ];
  inputString: string;
  // global limit and offset by default for exports
  offset: number = 0;
  limit: number = 100;
  jobType: any = [];
  previeDataFromServer: any = [];

  // Type number variables
  resultsLength: number = 10;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  totalRecords: number = 0;
  tabType: any;
  comingFrom: string;
  // jobTypeId: any;

  todaydate: Date = new Date();
  public model: any = {};

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("input") input: ElementRef;
  localStorage: any;

  constructor(
    public service: HttpService,
    public router: Router,
    private excelService: ExcelService,
    private route: ActivatedRoute
  ) {
    this.displayedColumns = [
      "jobId",
      "user",
      "worker",
      // "jobHrs",
      "job",
      "paid",
      "jobAddress",
      "bidsCount",
      "dateTime",
      "status",
      "action",
    ];
    // this.comingFrom = this.route.snapshot.queryParams.serviceName;
    this.jobFilterText = {
      serviceName: this.route.snapshot.queryParams.serviceName || "Service",
      value: "",
      _id: this.route.snapshot.queryParams._id || "",
    };
  }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem("TradeMen-admin-data"))) {
      this.localStorage = JSON.parse(
        localStorage.getItem("TradeMen-admin-data")
      );
    }
    // this.dataSource = [
    //   { jobId: 1, user: 'waqar hassan', worker: 'waris baloch', jobHrs: 12, paid: 100000, jobAddress: 'House 10', dateTime: 123123, status: 1 },
    //   { jobId: 1, user: 'waqar hassan', worker: 'waris baloch', jobHrs: 12, paid: 100000, jobAddress: 'House 10', dateTime: 123123, status: 1 },
    //   { jobId: 1, user: 'waqar hassan', worker: 'waris baloch', jobHrs: 12, paid: 100000, jobAddress: 'House 10', dateTime: 123123, status: 1 },
    //   { jobId: 1, user: 'waqar hassan', worker: 'waris baloch', jobHrs: 12, paid: 100000, jobAddress: 'House 10', dateTime: 123123, status: 1 },
    //   { jobId: 1, user: 'waqar hassan', worker: 'waris baloch', jobHrs: 12, paid: 100000, jobAddress: 'House 10', dateTime: 123123, status: 1 }]
    this.getServices();

    if (this.hideButton === 1) {
      this.displayedColumns.splice(5, 0, "bidsCount");
    }
  }
  ngAfterViewInit() {
    this.getListOnstart();

    this.httpSub$ = fromEvent(this.input.nativeElement, "keyup")
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.inputString = this.input.nativeElement.value;
          this.search({ searchText: this.input.nativeElement.value });
        })
      )
      .subscribe();
  }

  // getListOnstart() {
  //   this.isLoadingResults = true;
  //   let offset = ((this.paginator.pageIndex + 1) * this.paginator.pageSize) - this.paginator.pageSize;
  //   let limit = this.paginator.pageSize;
  //   let spListUrl = this.globals.urls.TradeAdmin.jobs.fetchJobsList;
  //   spListUrl = spListUrl.replace(':limit', limit.toString());
  //   spListUrl = spListUrl.replace(':offset', offset.toString());
  //   spListUrl = spListUrl.replace(':search_text', '');
  //   spListUrl = spListUrl.replace(':status', '');
  //   // spListUrl = spListUrl.replace(':jobTypeId','');
  //   // spListUrl = spListUrl.replace(':status','');

  //   this.service.getRequest(spListUrl)
  //     .pipe(
  //       map(res => {
  //         return res.data;
  //       })
  //     )
  //     .subscribe(
  //       data => {

  //         console.log(data);
  //         this.previeDataFromServer = data
  //         this.previeDataFromServer.getJobsList.forEach((element, index) => {
  //           if (element.status == 1) {
  //             element.statusText = 'open'
  //           }
  //           else if (element.status == 2) {
  //             element.statusText = 'Assigned'
  //           }
  //           else if (element.status == 3) {
  //             element.statusText = 'InProgress'
  //           }
  //           else if (element.status == 4) {
  //             element.statusText = 'Completed'
  //           }
  //           else if (element.status == 5) {
  //             element.statusText = 'Rated By User'
  //           }
  //           else if (element.status == 6) {
  //             element.statusText = 'Cancelled'
  //           }
  //           else if (element.status == 7) {
  //             element.statusText = 'Rejected'
  //           }
  //           else if (element.status == 8) {
  //             element.statusText = 'Time Out'
  //           }
  //         });
  //         this.dataSource = this.previeDataFromServer.getJobsList;
  //         this.isLoadingResults = false;
  //         this.resultsLength = data.countJobs;
  //         // this.dataSource = data.drivers;
  //         this.totalRecords = data.getJobsList ? data.getJobsList.length : 0;
  //       },
  //       err => {
  //         this.isLoadingResults = false;
  //         if (err.error)
  //           this.service.showError(err.error.message);
  //         else
  //           this.service.showError(err);
  //       },
  //   )
  // }

  sortData(sort: Sort) {
    const data = this.dataSource.slice();

    this.dataSource = data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "dateTime":
          return compare(a.date, b.date, isAsc);
        case "bidsCount":
          return compare(a.bidsCount, b.bidsCount, isAsc);
        default:
          return 0;
      }
    });
    this.dataToSend = [...this.dataSource];
  }

  getServices() {
    this.isLoadingResults = true;
    this.service
      .getRequest(this.globals.urls.TradeAdmin.workers.fetchJobTypes)
      .pipe(map((res) => res.data.servicesList))
      .subscribe(
        (data) => {
          this.jobType = data;
          this.jobType.unshift({
            serviceName: "All",
            _id: "",
          });
        },
        (err) => {
          this.isLoadingResults = false;
          if (err.error) this.service.showError(err.error.message);
          else this.service.showError(err);
        }
      );
  }

  getListOnstart() {
    this.isLoadingResults = true;
    this.httpSub$ = this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          let offset =
            (this.paginator.pageIndex + 1) * this.paginator.pageSize -
            this.paginator.pageSize;
          let limit = this.paginator.pageSize;
          let spListUrl = this.globals.urls.TradeAdmin.jobs.fetchJobsList;
          spListUrl = spListUrl.replace(":limit", limit.toString());
          spListUrl = spListUrl.replace(":offset", offset.toString());
          spListUrl = spListUrl.replace(":search_text", "");
          spListUrl = spListUrl.replace(
            ":status",
            this.tabType || this.statusFilterText.value || ""
          );
          spListUrl = spListUrl.replace(
            ":jobTypeId",
            this.jobFilterText._id.toString()
          );
          let startDate = this.model.startDate
            ? moment(this.model.startDate).valueOf() / 1000
            : "",
            endDate = this.model.endDate
              ? moment(this.model.endDate).valueOf() / 1000
              : "";
          spListUrl = spListUrl.replace(":startDate", startDate.toString());
          spListUrl = spListUrl.replace(":endDate", endDate.toString());
          return this.service.getRequest(spListUrl);
        }),
        map((res) => {
          return res.data;
        })
      )
      .subscribe(
        (data) => {
          this.previeDataFromServer = data;
          this.previeDataFromServer.getJobsList.forEach((element, index) => {
            if (element.status == 1) {
              element.statusText = "Open";
            } else if (element.status == 2) {
              element.statusText = "Assigned";
            } else if (element.status == 3) {
              element.statusText = "InProgress";
            } else if (element.status == 4) {
              element.statusText = "Completed";
            } else if (element.status == 5) {
              element.statusText = "Rated By User";
            } else if (element.status == 6) {
              element.statusText = "Cancelled";
            } else if (element.status == 7) {
              element.statusText = "Rejected";
            } else if (element.status == 8) {
              element.statusText = "Time Out";
            } else if (element.status == 9) {
              element.statusText = "Discarded";
            } else if (element.status == 11) {
              element.statusText = "Disputed";
            }
          });
          this.dataSource = this.previeDataFromServer.getJobsList;
          this.dataToSend = this.previeDataFromServer.getJobsList;
          this.isLoadingResults = false;
          this.resultsLength = data.countJobs;
          // this.dataSource = data.drivers;
          this.totalRecords = data.getJobsList ? data.getJobsList.length : 0;
        },
        (err) => {
          this.isLoadingResults = false;
          if (err.error) this.service.showError(err.error.message);
          else this.service.showError(err);
        }
      );
  }

  filter(job) {
    this.statusFilterText = job;
    // this.selectedVehicleTypeId = job.id;
    this.search({ statusType: job.value.toString() });
  }

  changeListing(status) {
    this.hideButton = status;
    this.paginator.pageIndex = 0;
    this.tabType = status.toString();
    if (this.hideButton === 1) {
      this.displayedColumns.splice(5, 0, "bidsCount");
    } else {
      const index = this.displayedColumns.indexOf('bidsCount');
      this.displayedColumns.splice(index, 1);
    }
    if (status === 0) {
      this.tabType = null;
      this.getListOnstart();
    } else {
      this.search({ statusType: status.toString() });
    }
  }

  search(input) {
    this.isLoadingResults = true;
    this.isLoadingResults = true;
    let offset =
      (this.paginator.pageIndex + 1) * this.paginator.pageSize -
      this.paginator.pageSize;
    let limit = this.paginator.pageSize;
    let spListUrl = this.globals.urls.TradeAdmin.jobs.fetchJobsList;
    spListUrl = spListUrl.replace(":limit", limit.toString());
    spListUrl = spListUrl.replace(":offset", offset.toString());
    spListUrl = spListUrl.replace(
      ":search_text",
      input.searchText || this.inputString || ""
    );
    spListUrl = spListUrl.replace(
      ":status",
      this.tabType ||
      input.statusType ||
      this.statusFilterText.value.toString() ||
      ""
    );
    spListUrl = spListUrl.replace(
      ":jobTypeId",
      this.jobFilterText._id.toString() || input.jobTypeId || ""
    );
    spListUrl = spListUrl.replace(":dateTime", input.dateTime || "");
    let startDate = this.model.startDate
      ? moment(this.model.startDate).valueOf() / 1000
      : "",
      endDate = this.model.endDate
        ? moment(this.model.endDate).valueOf() / 1000
        : "";
    spListUrl = spListUrl.replace(":startDate", startDate.toString());
    spListUrl = spListUrl.replace(":endDate", endDate.toString());
    // spListUrl = spListUrl.replace(':status','');
    this.service
      .getRequest(spListUrl)
      .pipe(
        map((res) => {
          return res.data;
        })
      )
      .subscribe(
        (data) => {
          this.previeDataFromServer = data;
          this.previeDataFromServer.getJobsList.forEach((element, index) => {
            if (element.status == 1) {
              element.statusText = "Open";
            } else if (element.status == 2) {
              element.statusText = "Assigned";
            } else if (element.status == 3) {
              element.statusText = "InProgress";
            } else if (element.status == 4) {
              element.statusText = "Completed";
            } else if (element.status == 5) {
              element.statusText = "Rated By User";
            } else if (element.status == 6) {
              element.statusText = "Cancelled";
            } else if (element.status == 7) {
              element.statusText = "Rejected";
            } else if (element.status == 8) {
              element.statusText = "Time Out";
            } else if (element.status == 11) {
              element.statusText = "Disputed";
            }
          });
          this.dataSource = this.previeDataFromServer.getJobsList;
          this.dataToSend = this.previeDataFromServer.getJobsList;
          this.isLoadingResults = false;
          this.resultsLength = data.countJobs;
          // this.dataSource = data.drivers;
          this.totalRecords = data.getJobsList ? data.getJobsList.length : 0;
        },
        (err) => {
          this.isLoadingResults = false;
          if (err.error) this.service.showError(err.error.message);
          else this.service.showError(err);
        }
      );
  }
  exportAsXLSX(): void {
    this.dataToSend.forEach((element) => {
      let obj = {
        "Job Id": element.jobId,
        "User Name": element.user,
        "Worker Name": element.worker,
        "Job Hours": element.jobHours,
        Paid: element.amount,
        Address: element.address,
        Date: moment.unix(element.date).format("MM/DD/YYYY"),
        Status: element.statusText,
        Service: element.jobService,
      };
      this.driversExportData.push(obj);
    });

    this.excelService.exportAsExcelFile(this.driversExportData, "Jobs");
    this.driversExportData = [];
  }

  filterJob(job, typeId) {
    this.paginator.pageIndex = 0;
    if (typeId == 1) {
      this.jobFilterText = job;
      this.search({ jobTypeId: job._id });
    } else {
      this.statusFilterText = job;
      this.search({ statusType: job.value.toString() });
    }
  }

  _openCalendar(
    picker: MatDatepicker<Date>,
    bindCalendar: string = "startDate"
  ) {
    picker.open();
    if (bindCalendar == "startDate") {
      setTimeout(() => this.model.startDate);
    } else {
      setTimeout(() => this.model.endDate);
    }
  }

  _closeCalendar(e, bindCalendar: string = "endDate") {
    if (bindCalendar == "endDate") {
      setTimeout(() => this.model.startDate);
    } else {
      setTimeout(() => this.model.endDate);
    }
    console.log(this.model.startDate, this.model.endDate);
    this.getListOnstart();
  }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
export interface jobsInterface {
  [index: number]: {
    jobId: number;
    user: string;
    worker: string;
    jobHrs: number;
    paid: number;
    jobAddress: string;
    dateTime: number;
    status: number;
  };
}
