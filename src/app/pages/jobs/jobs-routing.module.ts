import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobsListingComponent } from './jobs-listing/jobs-listing.component';
import { JobDetailsComponent } from './job-details/job-details.component';
import { Globals } from 'src/app/Globals';
import { RouteResolverService } from 'src/app/services/route-resolver.service';
import { RouterGuard } from '../../guards/route.guard';
import { MessagesListComponent } from './messages-list/messages-list.component';
import { JobEditComponent } from './job-edit/job-edit.component';

const routes: Routes = [{
  path: '',
  canActivate: [RouterGuard],
  data: { jobs: 1 },
  children: [
    {
      path: 'listing',
      component: JobsListingComponent
    },
    {
      path: 'inprogress',
      component: JobsListingComponent
    },
    {
      path: 'pending',
      component: JobsListingComponent
    },
    {
      path: 'job-detail/:id/:from',
      component: JobDetailsComponent,
      resolve: {
        resolvedData: RouteResolverService
      },
      data: { apiUrl: Globals.urls.job.detail, methodType: 'GET', paramsAvailable: true },
    },
    {
      path: '',
      redirectTo: 'listing',
    },
    {
      path: "messages",
      component: MessagesListComponent,
    },
    {
      path: 'job-edit/:id/:from',
      component: JobEditComponent,
      resolve: {
        resolvedData: RouteResolverService
      },
      data: { apiUrl: Globals.urls.job.detail, methodType: 'GET', paramsAvailable: true },
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobsRoutingModule { }
