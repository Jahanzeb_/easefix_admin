import { Component, OnInit } from "@angular/core";
import { Globals } from "src/app/Globals";
import { ActivatedRoute, Router, RoutesRecognized } from "@angular/router";
import { HttpService } from "src/app/services/http-service";
import { Subscription } from "rxjs";
import { map, filter, pairwise } from "rxjs/operators";
import { PriceBreakdownDialogComponent } from "../price-breakdown-dialog/price-breakdown-dialog.component";
import { MatDialog, MatTableDataSource } from "@angular/material";
import { MessagesListComponent } from "../messages-list/messages-list.component";

@Component({
  selector: "app-job-details",
  templateUrl: "./job-details.component.html",
  styleUrls: ["./job-details.component.scss"],
})
export class JobDetailsComponent implements OnInit {
  globals = Globals;

  model: any = {};
  jobId: string;

  // maps routes
  origin: any;
  destination: any;
  waypoints: Array<any> = [];

  // show hide
  showPhoneNumber: boolean = false;
  showEmail: boolean = false;

  // worker or user
  assignedWorker: boolean = false;
  jobDetails: any;
  jobDetails$: Subscription = null;
  isLoadingResults: boolean = true;
  jobStatus: string;
  public markerOptions: any = {};
  public renderOptions = {
    suppressMarkers: true,
  };
  GdprDData: any;
  showData: any = {
    mobile: false,
    email: false,
    IdImage: false,
    userMobile: false,
    userEmail: false,
    userImage: false,
  };
  showEye: any = {
    mobile: true,
    email: true,
    IdImage: true,
    userMobile: true,
    userEmail: true,
    userImage: true,
  };
  comingFrom: string;

  localStorage: any;
  chatDetails: any;
  public previewImage: boolean = false;
  public previewImageUrl: string = "";
  displayedColumns = [];
  dataSource: any = [];
  resultsLength: number = 10;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  constructor(
    private route: ActivatedRoute,
    public service: HttpService,
    public router: Router,
    public dialog: MatDialog
  ) {
    this.comingFrom = this.route.snapshot.params.from;
  }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem("TradeMen-admin-data"))) {
      this.localStorage = JSON.parse(
        localStorage.getItem("TradeMen-admin-data")
      );
    }
    // this.isLoadingResults = true;
    this.router.events
      .pipe(
        filter((e: any) => e instanceof RoutesRecognized),
        pairwise()
      )
      .subscribe((e: any) => {
        console.log(e[0].urlAfterRedirects); // previous url
      });

    this.route.data.subscribe((data) => {
      this.jobDetails = data["resolvedData"].data;
      this.jobDetails.workDescription = this.jobDetails.workDescription.replaceAll('\n', '<br>');
      this.dataSource = this.jobDetails.bidsFound;
      this.resultsLength = this.jobDetails.totalBids;
      console.log(this.jobDetails)
      this.isLoadingResults = false;
    });
    this.origin = {
      lng: this.jobDetails.pickUpJobLocation.coordinates[0],
      lat: this.jobDetails.pickUpJobLocation.coordinates[1],
    };
    this.destination = {
      lng: this.jobDetails.jobLocation.coordinates[0],
      lat: this.jobDetails.jobLocation.coordinates[1],
    };

    if (this.jobDetails.jobStatus == 0) this.jobStatus = "Temporary";
    if (this.jobDetails.jobStatus == 1) this.jobStatus = "Open";
    if (this.jobDetails.jobStatus == 2) this.jobStatus = "Assigned";
    if (this.jobDetails.jobStatus == 3) this.jobStatus = "In Progress";
    if (this.jobDetails.jobStatus == 4) this.jobStatus = "Completed";
    if (this.jobDetails.jobStatus == 5) this.jobStatus = "Rated By User";
    if (this.jobDetails.jobStatus == 6) this.jobStatus = "Cancelled";
    if (this.jobDetails.jobStatus == 7) this.jobStatus = "Rejected";
    if (this.jobDetails.jobStatus == 8) this.jobStatus = "Job Timeout";

    this.jobId =
      this.route.snapshot.paramMap.get("jobId") ||
      this.route.snapshot.paramMap.get("id");
    this.model.phoneNumber = "03435206900";
    this.model.email = "usman.iqbal@vizteck.com";

    this.markerOptions = {
      origin: {
        icon: "../../../../assets/images/icon_dropoff_location.png",
        draggable: false,
      },
      destination: {
        icon: "../../../../assets/images/mapIconSelected.png",
      },
    };

    this.displayedColumns = [
      "image",
      "name",
      "phoneNumber",
      "amount",
      "userAmount",
      "totalAmount",
      "lastUpdated"
    ];
  }

  openDialogDetails(jobDetails): void {
    const dialogRef = this.dialog.open(PriceBreakdownDialogComponent, {
      width: "500px",
      data: jobDetails,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
      }
    });
  }

  Enablegdpr(from) {
    this.isLoadingResults = true;
    if (from == "mobile") {
      this.showData.mobile = true;
    } else if (from == "email") {
      this.showData.email = true;
    } else if (from == "IdImage") {
      this.showData.IdImage = true;
    }
  }

  EnablegdprUser(from) {
    this.isLoadingResults = true;
    if (from == "mobile") {
      this.showData.userMobile = true;
    } else if (from == "email") {
      this.showData.userEmail = true;
    } else if (from == "IdImage") {
      this.showData.IdImage = true;
    }
  }

  back() {
    if (this.comingFrom == "workerMaps") {
      this.router.navigateByUrl("/map");
      // this.router.navigate(['jobs/job-detail/' + id,'payouts']);
    } else if (this.comingFrom == "payouts") {
      this.router.navigate([
        "/payouts/payout-details",
        this.jobDetails.payoutId,
      ]);
    } else if (this.comingFrom == "jobListing") {
      this.router.navigateByUrl("/jobs/listing");
    }
  }
  GotGDPRDATA(event) {
    this.isLoadingResults = false;
    if (event.data.phoneNumber && event.data.phonePreFix) {
      this.jobDetails.spData.spProfileId.phoneNumber =
        event.data.phoneNumber.toString();
      this.showEye.mobile = false;
    } else if (event.data.email) {
      this.jobDetails.spData.spProfileId.email = event.data.email;
      this.showEye.email = false;
    }
  }

  GotGDPRDATAUSER(event) {
    this.isLoadingResults = false;
    if (event.data.phoneNumber && event.data.phonePreFix) {
      this.jobDetails.userData.userProfileId.phoneNumber =
        event.data.phoneNumber.toString();
      this.showEye.userMobile = false;
    } else if (event.data.email) {
      this.jobDetails.userData.userProfileId.email = event.data.email;
      this.showEye.userEmail = false;
    }
  }

  show(typeOfBit) {
    if (typeOfBit === 1) {
    } else if (typeOfBit === 2) {
    }
  }

  cancelFinishJob(typeOfJob) {
    this.isLoadingResults = true;
    let url, params, serviceType;
    if (typeOfJob === 1) {
      url = this.globals.urls.map.cancelJob;
      params = { jobId: this.jobId };
      serviceType = this.service.putRequest(url, params);
    } else if (typeOfJob === 2) {
      url = this.globals.urls.map.finishJob + this.jobId + "/complete-job";
      params = { jobId: this.jobId };
      serviceType = this.service.getRequest(url, params);
    }

    this.jobDetails$ = serviceType.subscribe(
      (data) => {
        this.isLoadingResults = false;
        if (data.response === 200) {
          this.service.showSuccess(data.message, "Jobs");
          if (typeOfJob === 1) {
            this.jobDetails.jobStatus = 4;
          }
          if (typeOfJob === 2) {
            this.jobDetails.jobStatus = 6;
          }
          // this.router.navigateByUrl('/map');
        } else if (!data.success) {
          this.service.showError(data.message, "Jobs");
        }
      },
      (err) => {
        this.isLoadingResults = false;
        this.service.showError(err.error.message);
      }
    );
  }

  fetchChatHistory() {
    let url = this.globals.urls.map.viewChat.replace(":jobId", this.jobId);
    url = url.replace(":workerId", '');

    this.service.getRequest(url).subscribe(
      res => {
        this.isLoadingResults = false;
        this.chatDetails = res.data.chatHistory;
        let className = 'messagesDialogue';
        if (!this.chatDetails.length) {
          className = 'noDataDialogue'
        }
        let dialogRef = this.dialog.open(MessagesListComponent, {
          width: "600px",
          maxHeight: "600px",
          // height: "700px",
          panelClass: className,
          data: {
            messages: this.chatDetails,
            jobId: this.jobId,
            workers: this.dataSource
          },
        });
        // this.service.showSuccess('User status updated successfully.', 'User');
      },
      err => {
        this.isLoadingResults = false;
        this.service.showError(err);
      }
    );
  }

  export() {
    this.isLoadingResults = true;
    let url, params, serviceType;
    url = this.globals.urls.getJobChatHistory;
    params = { jobId: this.jobId };
    serviceType = this.service.getRequest(url, params);
    this.jobDetails$ = serviceType.subscribe(
      (data) => {
        this.isLoadingResults = false;
        if (data.response === 200) {
          if (data.data.userFileUrl) {
            this.service.showSuccess(data.message, "Jobs");
            // window.open(data.data.userFileUrl, "_blank");
            const link = document.createElement("a");
            // link.target = '_blank';
            link.href = data.data.userFileUrl;
            link.setAttribute("visibility", "hidden");
            link.click();
          } else {
            this.service.showError(data.message, "Jobs");
          }

          // if (typeOfJob === 1) {
          //   this.jobDetails.jobStatus = 4;
          // }
          // if (typeOfJob === 2) {
          //   this.jobDetails.jobStatus = 6;
          // }
          // this.router.navigateByUrl('/map');
        } else if (!data.success) {
          this.service.showError(data.message, "Jobs");
        }
      },
      (err) => {
        this.isLoadingResults = false;
        this.service.showError(err.error.message);
      }
    );
  }

  previewImageViewer(image) {
    this.previewImageUrl = image;
    this.previewImage = true;
  }

  closePreview() {
    this.previewImage = false;
  }

  changeJobStatus(status) {
    console.log('status--', status);
    this.isLoadingResults = true;

    let url = this.globals.urls.job.updateStatus.replace(":jobId", this.jobId);
    url = url.replace(":status", status);
    this.service.getRequest(url).subscribe(
      res => {
        this.isLoadingResults = false;
        this.jobDetails.isAdminResponedToJob = true
      },
      err => {
        this.isLoadingResults = false;
        this.service.showError(err);
      }
    );
  }
}