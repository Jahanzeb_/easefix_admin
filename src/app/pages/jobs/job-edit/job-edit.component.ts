import { Component, OnInit } from "@angular/core";
import { Globals } from "src/app/Globals";
import { ActivatedRoute, Router, RoutesRecognized } from "@angular/router";
import { HttpService } from "src/app/services/http-service";
import { Subscription } from "rxjs";
import { map, filter, pairwise } from "rxjs/operators";
import { PriceBreakdownDialogComponent } from "../price-breakdown-dialog/price-breakdown-dialog.component";
import { MatDialog, MatTableDataSource } from "@angular/material";
import { MessagesListComponent } from "../messages-list/messages-list.component";
@Component({
  selector: 'app-job-edit',
  templateUrl: './job-edit.component.html',
  styleUrls: ['./job-edit.component.scss']
})
export class JobEditComponent implements OnInit {
  globals = Globals;

  model: any = {};
  jobId: string;

  // maps routes
  origin: any;
  destination: any;
  waypoints: Array<any> = [];

  // show hide
  showPhoneNumber: boolean = false;
  showEmail: boolean = false;

  // worker or user
  assignedWorker: boolean = false;
  jobDetails: any;
  jobDetails$: Subscription = null;
  isLoadingResults: boolean = true;
  jobStatus: string;
  public markerOptions: any = {};
  public renderOptions = {
    suppressMarkers: true,
  };
  GdprDData: any;
  showData: any = {
    mobile: false,
    email: false,
    IdImage: false,
    userMobile: false,
    userEmail: false,
    userImage: false,
  };
  showEye: any = {
    mobile: true,
    email: true,
    IdImage: true,
    userMobile: true,
    userEmail: true,
    userImage: true,
  };
  comingFrom: string;

  localStorage: any;
  chatDetails: any;
  public previewImage: boolean = false;
  public previewImageUrl: string = "";
  displayedColumns = [];
  dataSource: any = [];
  resultsLength: number = 10;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  primaryAddress: string = '';
  city: string = '';
  country: string = '';
  state: string = '';
  zipCode: string = '';
  fileToUpload: File = null;
  httpSub$: Subscription = null;

  constructor(
    private route: ActivatedRoute,
    public httpService: HttpService,
    public router: Router,
    public dialog: MatDialog
  ) {
    this.comingFrom = this.route.snapshot.params.from;
  }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem("TradeMen-admin-data"))) {
      this.localStorage = JSON.parse(
        localStorage.getItem("TradeMen-admin-data")
      );
    }
    // this.isLoadingResults = true;
    this.router.events
      .pipe(
        filter((e: any) => e instanceof RoutesRecognized),
        pairwise()
      )
      .subscribe((e: any) => {
        console.log(e[0].urlAfterRedirects); // previous url
      });

    this.route.data.subscribe((data) => {
      this.jobDetails = data["resolvedData"].data;
      this.jobDetails.workDescription = this.jobDetails.workDescription.replaceAll('\n', '<br>');
      this.dataSource = this.jobDetails.bidsFound;
      this.resultsLength = this.jobDetails.totalBids;
      console.log(this.jobDetails)
      this.isLoadingResults = false;
    });
    this.origin = {
      lng: this.jobDetails.pickUpJobLocation.coordinates[0],
      lat: this.jobDetails.pickUpJobLocation.coordinates[1],
    };
    this.destination = {
      lng: this.jobDetails.jobLocation.coordinates[0],
      lat: this.jobDetails.jobLocation.coordinates[1],
    };

    if (this.jobDetails.jobStatus == 0) this.jobStatus = "Temporary";
    if (this.jobDetails.jobStatus == 1) this.jobStatus = "Open";
    if (this.jobDetails.jobStatus == 2) this.jobStatus = "Assigned";
    if (this.jobDetails.jobStatus == 3) this.jobStatus = "In Progress";
    if (this.jobDetails.jobStatus == 4) this.jobStatus = "Completed";
    if (this.jobDetails.jobStatus == 5) this.jobStatus = "Rated By User";
    if (this.jobDetails.jobStatus == 6) this.jobStatus = "Cancelled";
    if (this.jobDetails.jobStatus == 7) this.jobStatus = "Rejected";
    if (this.jobDetails.jobStatus == 8) this.jobStatus = "Job Timeout";

    this.jobId =
      this.route.snapshot.paramMap.get("jobId") ||
      this.route.snapshot.paramMap.get("id");
    this.model.phoneNumber = "03435206900";
    this.model.email = "usman.iqbal@vizteck.com";

    this.markerOptions = {
      origin: {
        icon: "../../../../assets/images/icon_dropoff_location.png",
        draggable: false,
      },
      destination: {
        icon: "../../../../assets/images/mapIconSelected.png",
      },
    };

    this.displayedColumns = [
      "image",
      "name",
      "phoneNumber",
      "amount",
      "userAmount",
      "lastUpdated"
    ];
  }

  handleAddressChange(address: any) {
    this.jobDetails.coordinates = address.geometry.location.toJSON();
    this.jobDetails.jobLocation.coordinates[0] = this.jobDetails.coordinates.lng;
    this.jobDetails.jobLocation.coordinates[1] = this.jobDetails.coordinates.lat;

    this.jobDetails.jobAddress.primaryAddress = address.formatted_address
    const addresses = address.address_components;
    addresses.forEach((element, index) => {
      element.types.forEach((type, i) => {
        if (type === 'locality') {
          this.jobDetails.jobAddress.city = element.long_name;
        } else if (type === 'country') {
          this.jobDetails.jobAddress.country = element.long_name;
        }
      });
    });

  }

  uploadPackageImage(files: FileList) {
    if (files[0].type === "image/gif") {
      this.httpService.showError(
        "Gif images are not allowed",
        "Profile details"
      );
      return;
    }
    let mediaType = 0;
    if (files[0].type === "video/mp4") {
      mediaType = 1;
    }

    this.fileToUpload = files.item(0);
    this.isLoadingResults = true;
    this.httpSub$ = this.httpService
      .uploadImage(this.globals.urls.uploadMedia, this.fileToUpload)
      .subscribe(
        (res) => {
          this.isLoadingResults = false;
          this.jobDetails.media.push({ type: mediaType, url: res.data.url })
        },
        (err) => {
          this.isLoadingResults = false;
          this.httpService.showError(err.error.message)
        }
      );
  }

  deleteImage(e, image, index) {
    e.preventDefault();
    this.jobDetails.media.splice(index, 1);
  }

  editJob() {
    this.isLoadingResults = true;
    let url = this.globals.urls.TradeAdmin.jobs.editJob;
    let params = {
      media: this.jobDetails.media,
      workDescription: this.jobDetails.workDescription,
      jobAddress: this.jobDetails.jobAddress,
      jobId: this.jobDetails._id,
      jobLat: this.jobDetails.jobLocation.coordinates[1],
      jobLong: this.jobDetails.jobLocation.coordinates[0]

    }
    this.httpSub$ = this.httpService.postRequest(url, params).subscribe(
      (res) => {
        this.isLoadingResults = false;
        this.router.navigate(['/jobs/listing']);
      },
      (err) => {
        this.isLoadingResults = false;
        this.httpService.showError(err.error.message)
      }
    )
  }

  previewImageViewer(image) {
    this.previewImageUrl = image;
    this.previewImage = true;
  }

  closePreview() {
    this.previewImage = false;
  }

  Enablegdpr(from) {
    this.isLoadingResults = true;
    if (from == "mobile") {
      this.showData.mobile = true;
    } else if (from == "email") {
      this.showData.email = true;
    } else if (from == "IdImage") {
      this.showData.IdImage = true;
    }
  }

  EnablegdprUser(from) {
    this.isLoadingResults = true;
    if (from == "mobile") {
      this.showData.userMobile = true;
    } else if (from == "email") {
      this.showData.userEmail = true;
    } else if (from == "IdImage") {
      this.showData.IdImage = true;
    }
  }

  GotGDPRDATA(event) {
    this.isLoadingResults = false;
    if (event.data.phoneNumber && event.data.phonePreFix) {
      this.jobDetails.spData.spProfileId.phoneNumber =
        event.data.phoneNumber.toString();
      this.showEye.mobile = false;
    } else if (event.data.email) {
      this.jobDetails.spData.spProfileId.email = event.data.email;
      this.showEye.email = false;
    }
  }

  GotGDPRDATAUSER(event) {
    this.isLoadingResults = false;
    if (event.data.phoneNumber && event.data.phonePreFix) {
      this.jobDetails.userData.userProfileId.phoneNumber =
        event.data.phoneNumber.toString();
      this.showEye.userMobile = false;
    } else if (event.data.email) {
      this.jobDetails.userData.userProfileId.email = event.data.email;
      this.showEye.userEmail = false;
    }
  }

}
