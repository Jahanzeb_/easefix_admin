import { NgModule } from '@angular/core';

//Mat Tables, Cards
import {
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
  MatTooltipModule,
  MatExpansionModule,
  MatNativeDateModule,
} from '@angular/material';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDialogModule, } from '@angular/material/dialog';
// import { AgmCoreModule } from '../../../agm/core';
// import { AgmDirectionModule } from 'agm-direction'   // agm-direction
// import { environment } from '../../../environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmDialougComponent } from './confirm-dialoug/confirm-dialoug.component';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatTooltipModule,
    MatCardModule,
    MatExpansionModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatRadioModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatDialogModule,
    // AgmCoreModule.forRoot({
    //   apiKey: environment.mapApiKey,
    //   libraries: ['places', 'drawing', 'geometry'],
    // }),
    // AgmDirectionModule, 
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatTooltipModule,
    MatCardModule,
    // AgmCoreModule,
    // AgmDirectionModule,
    MatExpansionModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatRadioModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatDialogModule,
  ],
  declarations: [
    ConfirmDialougComponent],
  entryComponents: [ConfirmDialougComponent],
})
export class CommonPagesModule { }
