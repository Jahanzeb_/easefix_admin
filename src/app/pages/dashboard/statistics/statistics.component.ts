import { Component, OnInit } from "@angular/core";
import { Globals } from "src/app/Globals";
import * as moment from "moment";
import { HttpService } from "src/app/services/http-service";
import { Subscription } from "rxjs";
import { MatDatepicker } from "@angular/material";
import { Router } from "@angular/router";
import { ExcelService } from "src/app/services/excel.service";
@Component({
  selector: "app-statistics",
  templateUrl: "./statistics.component.html",
  styleUrls: ["./statistics.component.scss"],
})
export class StatisticsComponent implements OnInit {
  colorScheme;

  globals = Globals;

  isLoadingResults: boolean = false;

  dashboard$: Subscription = null;
  todaydate: Date = new Date();

  yearArr: Array<any> = [];
  chartsData: any = [];
  public model: any = {};
  monthsArr: Array<any> = [
    { name: "January", value: 1 },
    { name: "February", value: 2 },
    { name: "March", value: 3 },
    { name: "April", value: 4 },
    { name: "May", value: 5 },
    { name: "June", value: 6 },
    { name: "July", value: 7 },
    { name: "August", value: 8 },
    { name: "September", value: 9 },
    { name: "October", value: 10 },
    { name: "November", value: 11 },
    { name: "December", value: 12 },
  ];

  selectedMonth: any;
  selectedYear: any;

  dashboardStats: any = null;

  arrowImage = "../../../assets/images/dashboard-cross.png";
  currentMonth: number;
  negativeFeedback: number;

  dashboardYear = 2019;
  exportData: any = [];
  localStorage: any;

  constructor(
    private service: HttpService,
    private router: Router,
    private excelService: ExcelService
  ) {
    this.currentMonth = new Date().getMonth();

    for (let i = moment().year(); i >= this.dashboardYear; i--) {
      this.yearArr.push(i);
    }
  }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem("TradeMen-admin-data"))) {
      this.localStorage = JSON.parse(
        localStorage.getItem("TradeMen-admin-data")
      );
    }
    this.colorScheme = {
      domain: ["#3866de", "#ee0068"],
    };

    this.selectedMonth = this.monthsArr[this.currentMonth];
    this.selectedYear = new Date().getFullYear();
    // this.getStats(this.selectedMonth.value, this.selectedYear);
    this.getStats("", "");
  }

  getStats(start, end) {
    this.isLoadingResults = true;
    let params = {
      startDate: this.model.startDate
        ? moment(this.model.startDate).valueOf() / 1000
        : "",
      endDate: this.model.endDate
        ? moment(this.model.endDate).valueOf() / 1000
        : "",
    };

    this.dashboard$ = this.service
      .getRequest(this.globals.urls.dashBoard.analytics, params)
      .subscribe(
        (res) => {
          this.isLoadingResults = false;

          if (res.response === 200) {
            this.dashboardStats = res.data;
            this.chartsData = [];
            let data = {
              name: "",
              series: [
                {
                  name: "",
                  value: "",
                },
                {
                  name: "",
                  value: "",
                },
              ],
            };

            res.data.respObj.salesData.forEach((singleRecord, index) => {
              this.chartsData.push({
                name: "Week - " + singleRecord._id,
                series: [
                  {
                    name: "Completed",
                    value: singleRecord.completedJobEarning,
                  },
                  {
                    name: "InProgress",
                    value: singleRecord.postedJobEarning,
                  },
                ],
              });
            });
          } else if (res.response == 400) {
            this.service.showError(res["message"], "Dashboard Statistics");
          }
        },
        (err) => {
          this.service.showError(err.error.message);
          this.isLoadingResults = false;
        }
      );
  }

  _openCalendar(
    picker: MatDatepicker<Date>,
    bindCalendar: string = "startDate"
  ) {
    picker.open();
    if (bindCalendar == "startDate") {
      setTimeout(() => this.model.startDate);
    } else {
      setTimeout(() => this.model.endDate);
    }
  }

  _closeCalendar(e, bindCalendar: string = "endDate") {
    if (bindCalendar == "endDate") {
      setTimeout(() => this.model.startDate);
    } else {
      setTimeout(() => this.model.endDate);
    }
    this.getStats(this.model.startDate, this.model.endDate);
  }

  selectMonth(month) {
    this.selectedMonth = month;
    this.getStats(this.selectedMonth.value, this.selectedYear);
  }

  selectYear(year) {
    this.selectedYear = year;
    this.getStats(this.selectedMonth.value, this.selectedYear);
  }

  convertToInt(number) {
    return parseInt(number);
  }

  clickListener(page, data) {
    if (page === 1) {
      this.router.navigate(["/jobs/listing"]);
    } else if (page === 2) {
      this.router.navigate(["/workers/"]);
    } else if (page === 3) {
      this.router.navigate(["/users"]);
    } else if (page === 4) {
      this.router.navigate(["/jobs/listing"]);
      this.router.navigate(["/jobs/listing"], {
        queryParams: { serviceName: data.serviceName, _id: data._id },
        queryParamsHandling: "merge",
      });
    }
  }
  exportAsXLSX(): void {
    let obj = {
      "Total Revenue": this.dashboardStats.respObj.totalEarnings,
      'Earning': this.dashboardStats.respObj.profit,
      "Total Jobs": this.dashboardStats.respObj.totalJobs,
      "Jobs Completed": this.dashboardStats.respObj.completedJobs,
      "InProgress Jobs": this.dashboardStats.respObj.inProgressJobs,
      'Cancelled': this.dashboardStats.respObj.cancelledJobs,
      "Total Workers": this.dashboardStats.respObj.totalCustomer,
      "Total Customers": this.dashboardStats.respObj.totalWorkers,
    };

    this.dashboardStats.perServiceEarnings.forEach(element => {
      obj[element.serviceName] = element.earnings
    });
    console.log(obj);
    this.exportData.push(obj);
    this.excelService.exportAsExcelFile(this.exportData, "Dashbaord");
    this.exportData = [];
  }
}
