import { Component, OnDestroy, Input } from '@angular/core';

@Component({
    selector: 'ngx-d3-bar',
    template: `
    <ngx-charts-bar-vertical-2d
    [view]="view"
    [scheme]="colorScheme"
    [results]="results"
    [gradient]="gradient"
    [xAxis]="showXAxis"
    [yAxis]="showYAxis"
    [legend]="showLegend"
    [showXAxisLabel]="showXAxisLabel"
    [showYAxisLabel]="showYAxisLabel"
    (select)="onSelect($event)">
  </ngx-charts-bar-vertical-2d>
  `,
})
export class D3BarComponent implements OnDestroy {

    @Input('colorScheme') colorScheme: any;
    @Input('results') results: any;
    
    view: any[] = [700, 400];

    showXAxis = true;
    showYAxis = true;
    gradient = false;
    showLegend = false;
    showXAxisLabel = true;
    // xAxisLabel = 'Country';
    showYAxisLabel = true;
    // yAxisLabel = 'Population';

    constructor() { }

    ngOnDestroy(): void {
        // this.themeSubscription.unsubscribe();
    }

    onSelect(event) {
        // //console.log(event);
      }
}
