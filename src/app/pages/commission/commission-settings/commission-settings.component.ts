import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Globals } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { AddEditCommissionModalComponent } from '../add-edit-commission-modal/add-edit-commission-modal.component';

@Component({
  selector: 'app-commission-settings',
  templateUrl: './commission-settings.component.html',
  styleUrls: ['./commission-settings.component.scss']
})
export class CommissionSettingsComponent implements OnInit {

  globals = Globals;

  // form
  model: any = {}
  // isDisabled: boolean = true;
  commission$: Subscription = null;
  isLoadingResults: boolean = false;
  localStorage: any;
  commissionsArray: any;
  configurationId: any;
  commissionColumns: any = [
    "range",
    "commission",
    "actions",
  ];
  constructor(private service: HttpService, private router: Router, public dialog: MatDialog) { }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem('TradeMen-admin-data'))) {
      this.localStorage = JSON.parse(localStorage.getItem('TradeMen-admin-data'))
    }
    console.log('sndjsahdjkashdaskd', this.router.url)
    this.getCommission();
  }

  // editFieldsEnable() {
  //   this.isDisabled = false;
  // }
  // editFieldsDisable() {
  //   this.isDisabled = true;
  // }

  getCommission() {

    this.isLoadingResults = true;

    this.commission$ = this.service.getRequest(Globals.urls.settings.commission.getRanges, {})
      .pipe(
        map(res => res.data),
      ).subscribe(
        data => {

          this.model = data;
          this.isLoadingResults = false;
          this.commissionsArray = this.model.commissionRanges;
          this.configurationId = this.model._id;

        },
        err => {

          this.model = null;
          this.isLoadingResults = false;
          this.service.showError(err.error.message);

        }
      )
  }

  updateCommission(i, row) {
    this.commissionsArray.splice(i, 1)
    let params = { configurationId: this.configurationId, commissionRanges: this.commissionsArray }
    console.log('params---', params);

    // this.isDisabled = true;
    this.isLoadingResults = true;

    let url = this.globals.urls.settings.commission.updateRanges,
      serviceType = this.service.postRequest(url, params);;

    this.commission$ = serviceType.subscribe(

      res => {

        this.isLoadingResults = false;

        if (res.response === 200) {

          this.getCommission();
          this.service.showSuccess('Commission Successfully updated', 'Commission');
          this.isLoadingResults = false;

        }
        else if (res.response == 400) {

          this.isLoadingResults = false;
          this.service.showError(res['message'], 'Commission');

        }
      },
      err => {

        this.isLoadingResults = false;
        this.service.showError(err.error.message);

      }
    );

  }

  ngOnDestroy(): void {
    if (this.commission$) this.commission$.unsubscribe();
  }

  commissionAction(action, data) {
    let dialogRef = this.dialog.open(AddEditCommissionModalComponent, {
      width: "600px",
      maxHeight: "600px",
      // height: "700px",
      data: {
        type: action,
        comissionRange: data,
        originalArray: this.commissionsArray,
        configurationId: this.configurationId
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getCommission();
    });
  }

}
