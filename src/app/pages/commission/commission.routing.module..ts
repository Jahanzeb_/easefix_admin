import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommissionSettingsComponent } from './commission-settings/commission-settings.component';
import { ConfigurationsHeaderComponent } from '../configurations/configurations-header/configurations-header.component';
import { RouterGuard } from '../../guards/route.guard';
import { AddEditCommissionModalComponent } from './add-edit-commission-modal/add-edit-commission-modal.component';

const routes: Routes = [{
  path: '',
  component: ConfigurationsHeaderComponent,
  canActivate: [RouterGuard],
  data: { configurations: 1 },
  children: [
    {
      path: '',
      pathMatch: 'full',
      component: CommissionSettingsComponent
    },
    {
      path: 'commission-dialog',
      pathMatch: 'full',
      component: AddEditCommissionModalComponent
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommissionRoutingModule { }
