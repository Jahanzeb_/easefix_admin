import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { Globals } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';

@Component({
  selector: 'app-add-edit-commission-modal',
  templateUrl: './add-edit-commission-modal.component.html',
  styleUrls: ['./add-edit-commission-modal.component.scss']
})
export class AddEditCommissionModalComponent implements OnInit {
  model: any;
  reqData: any;
  btnTxt: any;
  commission$: Subscription = null;
  configurationId: any;
  isLoadingResults: any = false;

  constructor(public dialogRef: MatDialogRef<AddEditCommissionModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private service: HttpService, private router: Router) {
    this.btnTxt = data.type;
    this.reqData = data.comissionRange;
    this.configurationId = data.configurationId;
  }

  ngOnInit(): void {
    this.model = {
      commissionPercentage: '',
      maxRange: '',
      minRange: ''
    };
    if (this.btnTxt === 'Edit') {
      this.model = {
        commissionPercentage: this.reqData.commissionPercentage,
        maxRange: this.reqData.maxRange,
        minRange: this.reqData.minRange,
      }
    }
  }

  addEditCommission(aa) {
    this.isLoadingResults = true;

    let url = this.btnTxt === 'Add' ? Globals.urls.settings.commission.addRange : Globals.urls.settings.commission.editRange;
    let params = {
      minRange: parseInt(this.model.minRange),
      maxRange: parseInt(this.model.maxRange),
      commissionPercentage: parseInt(this.model.commissionPercentage),
      configurationId: this.configurationId
    }
    if (this.btnTxt === 'Edit') {
      params['rangeId'] = this.reqData._id
    }
    this.commission$ = this.service.postRequest(url, params)
      .pipe(
        map(res => res.data),
      ).subscribe(
        data => {

          this.model = data;
          this.dialogRef.close();
          this.isLoadingResults = false;

        },
        err => {
          this.model = null;
          // this.isLoadingResults = false;
          this.service.showError(err.error.message);

        }
      )
  }
}