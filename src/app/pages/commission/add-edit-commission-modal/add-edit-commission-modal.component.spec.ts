import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditCommissionModalComponent } from './add-edit-commission-modal.component';

describe('AddEditCommissionModalComponent', () => {
  let component: AddEditCommissionModalComponent;
  let fixture: ComponentFixture<AddEditCommissionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditCommissionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditCommissionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
