import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommissionSettingsComponent } from './commission-settings/commission-settings.component';
import { CommissionRoutingModule } from './commission.routing.module.';
import { ConfigurationsModule } from '../configurations/configurations.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SharedPipesModule } from '../shared-pipes/shared-pipes.module';
import { MatTableModule, MatPaginatorModule, MatProgressSpinnerModule, MatToolbarModule, MatCardModule, MatRadioModule } from '@angular/material';
import { AddEditCommissionModalComponent } from './add-edit-commission-modal/add-edit-commission-modal.component';
import { CommonPagesModule } from '../common-pages/common-pages.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ConfigurationsModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatCardModule,
    SharedPipesModule,
    CommissionRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatRadioModule,
    CommonPagesModule,
  ],
  declarations: [CommissionSettingsComponent, AddEditCommissionModalComponent],
  entryComponents: [AddEditCommissionModalComponent]
})
export class CommissionModule { }
