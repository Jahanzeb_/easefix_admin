import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule, MatCardModule, MatTableModule,  MatPaginatorModule, MatToolbarModule, MatRadioModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { LogingRoutingModule } from './loging-routing.module';
import { LogListComponent } from './log-list/log-list.component';

@NgModule({
  imports: [
    CommonModule,
    LogingRoutingModule,
    MatProgressSpinnerModule,
    FormsModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatRadioModule,
    MatDatepickerModule
  ],
  declarations: [LogListComponent]
})
export class LogingModule { }
