import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { map, switchMap, startWith, debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { Subscription, fromEvent } from 'rxjs';
import { HttpService } from '../../../services/http-service';
import { Globals } from '../../../Globals';
import { MatDatepicker } from '@angular/material';
import * as moment from 'moment'
@Component({
  selector: 'app-log-list',
  templateUrl: './log-list.component.html',
  styleUrls: ['./log-list.component.scss']
})
export class LogListComponent implements OnInit {

 // Table Settings
  // dataSource: referralInterface;
  // dataSource: MatTableDataSource<any>;
  displayedColumns = [];
  // dataSource: MatTableDataSource<any>;
  dataSource:any=[];
  private httpSub$: Subscription = null;
  isLoadingResults = false;
  totalUsers = 0;
  public globals = Globals;
  public translation: any;
  public model: any = {};
  @ViewChild(MatPaginator) paginator: MatPaginator;

  public users: Array<any> = [];

  constructor(public service: HttpService) { }

  //Set total records
  resultsLength: number = 10;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  inputString:string;
  dataToSend: any;
  todaydate: Date = new Date();
  userType:string='user';
  offset: number = 0;
  limit: number = 100;
  tabactive:boolean=true;
  totalRecords: number = 0;

  ngOnInit() {
    this.displayedColumns = ['number', 'Type','description','logTime','userType', 'name'];

    // Fetch data for xcel sheet DRIVERS
    // this.getUsersExportsData(this.offset);

  }
  ngAfterViewInit() {

    // this.httpSub$ = fromEvent(this.input.nativeElement, 'keyup')
    //   .pipe(
    //     debounceTime(400),
    //     distinctUntilChanged(),
    //     tap(() => {
    //       this.paginator.pageIndex = 0;
    //       this.inputString=this.input.nativeElement.value;
    //       this.getLogs({ searchText: this.input.nativeElement.value });
    //     })
    //   )
    //   .subscribe();

      this.getLogs({ searchText:''});
  }


  // search(input) {
  //   this.isLoadingResults = true;
  //   let offset = ((this.paginator.pageIndex + 1) * this.paginator.pageSize) - this.paginator.pageSize;
  //   let limit = this.paginator.pageSize;
  //   let spListUrl=this.globals.urls.TradeAdmin.users.fetchUsersList;
  //   spListUrl = spListUrl.replace(':limit', limit.toString());
  //   spListUrl = spListUrl.replace(':offset', offset.toString());
  //   spListUrl = spListUrl.replace(':search_text',  input.searchText || this.inputString || '');
  //   spListUrl = spListUrl.replace(':status', input.statusType||'');
  //   spListUrl = spListUrl.replace(':jobTypeId','');
  //   this.service.getRequest(spListUrl)
  //     .pipe(
  //       map(res => {
  //         return res.data;
  //       })
  //     )
  //     .subscribe(
  //       data => {
  //         this.isLoadingResults = false;
  //         this.dataSource = data.users;
  //         this.totalRecords = data.users.length;
  //         this.resultsLength = data.count;
  //       },
  //       err => {
  //         this.isLoadingResults = false;
  //         this.service.showError(err.error.message);
  //       },
  //     )
  // }

  // switchType(type){
  //   this.userType=type;
  //   if(type== 'user'){
  //     this.tabactive=true;
  //   }
  //   else{
  //     this.tabactive=false;
  //   }
  //   this.getRefferals('');
  // }

  getLogs(input) {
    this.isLoadingResults = true;
    this.httpSub$ = this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {
          let params = {
            offset: ((this.paginator.pageIndex + 1) * this.paginator.pageSize) - this.paginator.pageSize,
            limit: this.paginator.pageSize,
            // searchText: this.input.nativeElement.value || null,
            startDate: this.model.startDate? moment(this.model.startDate).valueOf() / 1000 : '',
            endDate: this.model.endDate? moment(this.model.endDate).valueOf() / 1000 : '',
          };
          this.isLoadingResults = true;
          // let offset = ((this.paginator.pageIndex + 1) * this.paginator.pageSize) - this.paginator.pageSize;
          // let limit = this.paginator.pageSize;
          let spListUrl=this.globals.urls.TradeAdmin.Loging.getLogs;
          // spListUrl = spListUrl.replace(':limit', limit.toString());
          // spListUrl = spListUrl.replace(':offset', offset.toString());
          // spListUrl = spListUrl.replace(':startDate','');
          // spListUrl = spListUrl.replace(':endDate','');

          return this.service.postRequest(spListUrl,params);
        }),
        map(res => {
          return res.data;
        }),
      ).subscribe(
        data => {
          this.totalUsers = data.count;
          this.isLoadingResults = false;
          this.resultsLength = data.count;
          this.dataSource = data.logs ;
          this.totalRecords = data.logs.length;
        },
        err => {
          this.isLoadingResults = false;
          this.service.showError(err.error.message);
        }
      );
  }

  _openCalendar(picker: MatDatepicker<Date>, bindCalendar: string = "startDate") {
    picker.open();
    if (bindCalendar == 'startDate') {
      setTimeout(() => this.model.startDate);
    } else {
      setTimeout(() => this.model.endDate);
    }
  }

  _closeCalendar(e, bindCalendar: string = "endDate") {
    if (bindCalendar == 'endDate') {
      setTimeout(() => this.model.startDate);
    } else {
      setTimeout(() => this.model.endDate);
    }
    this.getLogs({ searchText:''});
  }


  ngOnDestroy(): void {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }

}
