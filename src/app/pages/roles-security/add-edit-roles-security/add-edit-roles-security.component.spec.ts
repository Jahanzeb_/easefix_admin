import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditRolesSecurityComponent } from './add-edit-roles-security.component';

describe('AddEditRolesSecurityComponent', () => {
  let component: AddEditRolesSecurityComponent;
  let fixture: ComponentFixture<AddEditRolesSecurityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditRolesSecurityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditRolesSecurityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
