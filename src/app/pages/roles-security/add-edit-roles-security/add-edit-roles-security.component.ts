import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { HttpService } from '../../../services/http-service';
import { Router } from '@angular/router';
import { Globals, checkIfOnlySpaces } from '../../../Globals';
import { NgForm } from '@angular/forms';
import { of as observableOf, Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-add-edit-roles-security',
  templateUrl: './add-edit-roles-security.component.html',
  styleUrls: ['./add-edit-roles-security.component.scss']
})
export class AddEditRolesSecurityComponent implements OnInit {

  // Public Functions
  public globals = Globals;

  // variable type ANY
  model: any = {
    dashboard: 1,
    map: 1,
    workers: 1,
    users: 1,
    jobs: 1,
    promoCodes: 1,
    configurations: 1,
    referrals: 1,
    rolesAndSecurity: 1,
    banks: 1,
    payouts: 1,
    notifications: 1,
    privacyPolicy: 1,
    termsAndConditions: 1,
  };
  type: any;
  agentId: any;
  typeOfRequestSend: any;
  dataToSend: any;
  roleData$: Subscription = null
  isLoadingResults: boolean = false;
  // variable type string
  typeOfForm: string = 'Add New'
  requestUrl: string;
  countryCodes: any = [];
  statusFilterText: any;

  @ViewChild('roleForm') form;

  constructor(private service: HttpService, private router: Router, private route: ActivatedRoute) {
    this.model.status = '1'; //active : 1
  }

  ngOnInit() {
    this.type = this.route.snapshot.data['type'];
    this.agentId = this.route.snapshot.paramMap.get('agentId');
    this.getStatus();
    if (this.type === 'add-role') {
      this.typeOfForm = 'Add New';
    } else if (this.type === 'edit-role') {
      this.typeOfForm = 'Edit';
      this.singleRole();
    }

  }

  // Add new role || Update role
  addNewRole(form: NgForm) {
    this.model.profileImageUrl = this.globals.defaultImage;
    if (this.type === 'add-role') {
      this.requestUrl = this.globals.urls.TradeAdmin.RolesSecurity.AddRoles;
      this.typeOfRequestSend = this.service.postRequest(this.requestUrl, this.model);
    }
    else if (this.type === 'edit-role') {
      this.requestUrl = this.globals.urls.TradeAdmin.RolesSecurity.updateRoles;
      this.requestUrl = this.requestUrl.replace(':subAdminId', this.agentId.toString());
      this.model.agentId = this.agentId
      this.typeOfRequestSend = this.service.postRequest(this.requestUrl, this.model);
    }

    this.typeOfRequestSend.subscribe(
      res => {
        if (res.response === 200) {
          if (this.type === 'add-role') {
            form.resetForm();
            this.service.showSuccess('Role and Secuirty Successfully Created', 'Role and Secuirty');
            form.controls['status'].setValue('1');
            this.router.navigate(['role-security']);

          } else if (this.type === 'edit-role') {
            this.service.showSuccess('Role and Secuirty updated successfully.', 'Role and Secuirty');
          }
          this.router.navigate(['roles-security']);
        }
        else if (res.response == 400) {
          this.service.showError(res['message'], 'Role and Secuirty');
        }
      },
      err => {
        this.service.showError(err.error.message);
      },
    );
  }

  // Edit bank (fetch details)
  singleRole() {

    let spListUrl = this.globals.urls.TradeAdmin.RolesSecurity.getSingleRole;
    spListUrl = spListUrl.replace(':subAdminId', this.agentId.toString());

    this.roleData$ = this.service.getRequest(spListUrl)
      .pipe(
        map(res => {
          return res.data.subAdminDetail;
        })
      )
      .subscribe(
        roleData => {
          this.model = roleData ? roleData : this.model,
            this.model.status = roleData.status === 1 ? '1' : '0'   // ONLY in this module 1 is for active and 0 is for inactive
            this.countryCodes.forEach((element, index) => {
            if (element.countryCode == this.model.phonePreFix) {
              this.statusFilterText = this.countryCodes[index];
              this.model.phonePreFix = this.countryCodes[index].countryCode;
            }
            // else {
            //   this.statusFilterText = this.countryCodes[1];
            //   this.model.phonePreFix = this.countryCodes[1].countryCode;
            // }
          });

         
        },
        err => {
          if (err.error)
            this.service.showError(err.error.message);
          else
            this.service.showError(err);
        },
    )
  }

  getStatus() {
    this.service.getRequest(this.globals.urls.getPrfixes)
      .pipe(
        map(res => {
          return res.data;
        })
      )
      .subscribe(
        data => {
          this.isLoadingResults = true;
          this.countryCodes = data.countryCodes;
          this.statusFilterText = this.countryCodes[1];
          this.model.phonePreFix = this.countryCodes[1].countryCode;
          // this.previeDataFromServer=data.respObj
          // this.previeDataFromServer.forEach((element, index) => {
          //   if(element.status==true){
          //      element.statusText='Verified'
          //   }
          //   else{
          //    element.statusText='unverified'
          //   }
          //  });
          //  this.dataSource = this.previeDataFromServer;
          //  this.isLoadingResults = false;
          //  this.resultsLength = data.count;
          //  // this.dataSource = data.drivers;
          //  this.totalRecords = data.respObj ? data.respObj.length : 0;
        },
        err => {
          this.isLoadingResults = false;
          if (err.error)
            this.service.showError(err.error.message);
          else
            this.service.showError(err);
        },
    )
  }

  filter(status) {
    this.statusFilterText = status;
    this.model.phonePreFix = status.countryCode;
  }

  back() {
    this.router.navigate(['admin/role-security']);
  }

  public checkIfOnlySpaces(control: string) {
    return checkIfOnlySpaces(this.form, control);
  }

  ngOnDestroy() {
    if (this.roleData$) this.roleData$.unsubscribe();
  }
}
