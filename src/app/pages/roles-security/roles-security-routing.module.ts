import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RolesSecurityListComponent } from './roles-security-list/roles-security-list.component';
import { AddEditRolesSecurityComponent } from './add-edit-roles-security/add-edit-roles-security.component';
import { RouterGuard } from '../../guards/route.guard';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'add',
      component: AddEditRolesSecurityComponent,
      canActivate: [RouterGuard],
      data: {
        type: 'add-role',
        rolesAndSecurity: 1,
        rolesAndSecurityView: 2
      }
    },
    {
      path: 'edit/:agentId',
      component: AddEditRolesSecurityComponent,
      canActivate: [RouterGuard],
      data: {
        type: 'edit-role',
        rolesAndSecurity: 1,
        rolesAndSecurityView: 2
      }
    },
    {
      path: '',
      pathMatch: 'full',
      canActivate: [RouterGuard],
      data: { rolesAndSecurity: 1 },
      component: RolesSecurityListComponent
    },
  ],
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RolesSecurityRoutingModule { }
