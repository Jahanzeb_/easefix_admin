import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolesSecurityListComponent } from './roles-security-list.component';

describe('RolesSecurityListComponent', () => {
  let component: RolesSecurityListComponent;
  let fixture: ComponentFixture<RolesSecurityListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolesSecurityListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesSecurityListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
