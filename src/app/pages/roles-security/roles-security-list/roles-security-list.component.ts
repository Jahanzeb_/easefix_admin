import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { Globals } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';
import { map, switchMap, startWith, } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
@Component({
  selector: 'app-roles-security-list',
  templateUrl: './roles-security-list.component.html',
  styleUrls: ['./roles-security-list.component.scss']
})
export class RolesSecurityListComponent implements OnInit {

  displayedColumns = ['number', 'name', 'email', 'mobile' ,'role', 'status', 'action'];
  dataSource: MatTableDataSource<any>;

  // Observables/Subscriptions
  private httpSub$: Subscription = null;

  // Public Depedency
  public globals = Globals;

  // Type boolean variables
  isLoadingResults = true;

  // Type string variables
  statusFilterText = 'Status';

  // Type number variables
  resultsLength: number = 10;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  totalRecords: number = 0;
  localStorage: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public service: HttpService, public dialog: MatDialog, private router: Router) { }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem('TradeMen-admin-data'))) {
      this.localStorage = JSON.parse(localStorage.getItem('TradeMen-admin-data'))
  }
   }

  ngAfterViewInit() {
    this.paginator.pageIndex = 0;
    this.getRolesSecurity();
  }

  // get roles and security list
  getRolesSecurity() {
    this.httpSub$ = this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {
          let params = {
            offset: ((this.paginator.pageIndex + 1) * this.paginator.pageSize) - this.paginator.pageSize,
            limit: this.paginator.pageSize,
          };
          this.isLoadingResults = true;
          // let offset = ((this.paginator.pageIndex + 1) * this.paginator.pageSize) - this.paginator.pageSize;
          // let limit = this.paginator.pageSize;
          // let spListUrl=this.globals.urls.TradeAdmin.RolesSecurity.getRoles;
          // spListUrl = spListUrl.replace(':limit', limit.toString());
          // spListUrl = spListUrl.replace(':offset', offset.toString());

          return this.service.postRequest(this.globals.urls.TradeAdmin.RolesSecurity.getRoles,params);
        }),
        map(res => {
          return res.data;
        }),
      ).subscribe(
        data => {
          this.isLoadingResults = false;
          this.resultsLength = data.count;
          this.dataSource = data.adminUsers;
          this.totalRecords = data.adminUsers.length;

        },
        err => {
          this.dataSource = null;
          this.isLoadingResults = false;
          this.service.showError(err.error.message);
        }
      );
  }

  //delete role and security
  // deleteRoleSecurity(agentId) {
  //   this.isLoadingResults = true;
  //   this.service.postRequest(this.globals.urls.post.updateRoleSecurity, { status: 0, agentId: agentId }).subscribe(
  //     res => {
  //       if (res.response === 200) {
  //         this.service.showError('Role and Security deleted successfully.', 'Roles and Security');
  //         this.getRolesSecurity();
  //         this.isLoadingResults = false;
  //       }
  //       else if (res.response == 400) {
  //         this.service.showError(res['message'], 'Roles and Security');
  //       }
  //     },
  //     err => {
  //       this.service.showError(err.error.message);
  //     },
  //   );
  // }

  ngOnDestroy(): void {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }
}