import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RolesSecurityListComponent } from './roles-security-list/roles-security-list.component';
import { AddEditRolesSecurityComponent } from './add-edit-roles-security/add-edit-roles-security.component';
import { RolesSecurityRoutingModule } from './roles-security-routing.module';
import { MatProgressSpinnerModule, MatCardModule, MatTableModule, MatPaginatorModule, MatToolbarModule, MatRadioModule, MatCheckboxModule,  } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    FormsModule,
    MatCardModule,
    MatTableModule,
    NgbModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatRadioModule,
    MatCheckboxModule,
    RolesSecurityRoutingModule
  ],
  declarations: [RolesSecurityListComponent, AddEditRolesSecurityComponent]
})
export class RolesSecurityModule { }
