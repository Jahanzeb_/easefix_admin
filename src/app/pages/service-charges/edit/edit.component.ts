import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { Globals, checkIfOnlySpaces } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  globals = Globals;

  model: any = {};
  type: any;

  service$: Subscription = null

  isLoadingResults: boolean = false;
  disableInput:boolean=false;
  localStorage: any;
  @ViewChild('addEditVt') form;

  constructor(private service: HttpService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem('TradeMen-admin-data'))) {
      this.localStorage = JSON.parse(localStorage.getItem('TradeMen-admin-data'))
  }
  if(this.localStorage.configurations!=3){
    this.disableInput=true;
  }
  else{
    this.disableInput=false
  }
    this.fetchService();
  }

  edit(form: NgForm) {

    let url: string,
      request: any,
      params: any = {
        jobRadiusInKm: this.model.jobRadiusInKm,
        serviceCharges: this.model.serviceCharges,
        vat: this.model.vat,
        timeout: this.model.timeout,
        jobCancellationAmount: this.model.jobCancellationAmount,
        referralJobs: this.model.referralJobs,
        referralPercentage: this.model.referralPercentage,
        sendEmail: this.model.sendEmail,
        sendJob: this.model.sendJob
      };

    url = this.globals.urls.settings.serviceCharges.edit
    request = this.service.putRequest(url, params);

    this.isLoadingResults = true;

    request.subscribe(
      res => {
        if (res.response === 200) {

          this.isLoadingResults = false;

          this.service.showSuccess('Service updated successfully.', 'Service Charges');
          this.fetchService();

        }
        else if (res.response == 400) {

          this.isLoadingResults = false;
          this.service.showError(res['message'], 'Service Charges');

        }
      },
      err => {

        this.isLoadingResults = false;
        this.service.showError(err);

      },
    );

  }

  fetchService() {

    let url: any = this.globals.urls.settings.serviceCharges.get,
      request: any = this.service.getRequest(url);

    this.isLoadingResults = true;

    this.service$ = request.subscribe(
      res => {

        if (res.response === 200) {
          this.model = res.data;
        }
        else if (res.response == 400) {
          this.service.showError(res['message'], 'Service Charges');
        }

        this.isLoadingResults = false;


      },
      err => {

        this.service.showError(err.error.message);
        this.isLoadingResults = false;

      },
    );

  }

  public checkIfOnlySpaces(control: string) {
    return checkIfOnlySpaces(this.form, control);
  }

  ngOnDestroy() {
    if (this.service$) this.service$.unsubscribe();
  }

}
