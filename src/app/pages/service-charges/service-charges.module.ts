import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServiceChargesRoutingModule } from './service-charges-routing.module';
import { EditComponent } from './edit/edit.component';
import { ConfigurationsModule } from '../configurations/configurations.module';
import { FormsModule } from '@angular/forms';
import { MatCardModule, MatProgressSpinnerModule, MatSlideToggleModule } from '@angular/material';


@NgModule({
  imports: [
    CommonModule,
    ConfigurationsModule,
    FormsModule,
    MatCardModule,
    MatProgressSpinnerModule,
    ServiceChargesRoutingModule,
    MatSlideToggleModule
  ],
  declarations: [EditComponent]
})
export class ServiceChargesModule { }
