import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigurationsHeaderComponent } from '../configurations/configurations-header/configurations-header.component';
import { EditComponent } from './edit/edit.component';
import { RouterGuard } from '../../guards/route.guard';

const routes: Routes = [{
  path: '',
  component: ConfigurationsHeaderComponent,
  canActivate: [RouterGuard],
  data: { configurations: 1 },
  children: [
    {
      path: 'edit',
      component: EditComponent
    },
    {
      path: '',
      pathMatch: 'full',
      component: EditComponent
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceChargesRoutingModule { }
