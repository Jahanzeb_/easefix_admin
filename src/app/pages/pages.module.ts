import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { HeaderComponent } from './components/header/header.component';
import {
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
  MatTooltipModule,
  MatCheckboxModule,
} from '@angular/material';
import { MatCardModule } from '@angular/material/card';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { environment } from '../../environments/environment';
// import { MapComponent } from './map/map.component';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';   // agm-direction
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AuthGuard } from '../guards/auth.guard';
import { RouterGuard } from '../guards/route.guard';
import { MatToolbarModule, MatDialogModule } from '@angular/material';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { ExcelService } from '../services/excel.service';
import { momentAgoPipe } from '../pipes/momentAgoPipe';
import { SidebarModule } from 'ng-sidebar';

import { GooglePlacesDirective } from '../directives/google-places.directive';
import { IbanValidator } from '../directives/iban-validator.directive';
import { CommonPagesModule } from './common-pages/common-pages.module';
import { CreditCardDirectivesModule } from 'angular-cc-library';
import { NgxStripeModule } from 'ngx-stripe';
// import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { SharedPipesModule } from './shared-pipes/shared-pipes.module';
// import { CommisionCheckComponent } from './workers/worker-profile/commision-check/commision-check.component';

// import { D3BarComponent } from './dash-board-franchiser/d3-bar.component';
const PAGES_COMPONENTS = [
  PagesComponent,
  HeaderComponent,
  // MapComponent,
  GooglePlacesDirective,
  IbanValidator,
  // CommisionCheckComponent
];
// const config: SocketIoConfig = {
//   url: environment.socket, options: {
//     query: {
//       token: localStorage.token
//     }
//   }
// };

@NgModule({
  imports: [
    NgbModule,
    FormsModule,
    MatInputModule,
    CommonPagesModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatTooltipModule,
    MatCardModule,
    NgxChartsModule,
    MatToolbarModule,
    MatCheckboxModule,
    SharedPipesModule,
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: "#78C000",
      innerStrokeColor: "#C7E596",
      animationDuration: 300,
    }),
    AgmCoreModule.forRoot({
      apiKey: environment.mapApiKey,
      libraries: ['places', 'drawing', 'geometry'],
    }),
    AgmDirectionModule,
    SidebarModule.forRoot(),
    CreditCardDirectivesModule,
    NgxStripeModule.forRoot(environment.stripe),
    // SocketIoModule.forRoot(config),
    PagesRoutingModule,
  ],
  exports: [
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatDialogModule,
    MatTableModule,
    MatTooltipModule,
    MatCardModule,
    AgmCoreModule,
    NgxChartsModule,
    MatToolbarModule,
  ],

  providers: [AuthGuard, ExcelService, RouterGuard],

  declarations: [
    ...PAGES_COMPONENTS
  ],
  
})
export class PagesModule {
}
