import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurationsHeaderComponent } from './configurations-header.component';

describe('ConfigurationsHeaderComponent', () => {
  let component: ConfigurationsHeaderComponent;
  let fixture: ComponentFixture<ConfigurationsHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigurationsHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurationsHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
