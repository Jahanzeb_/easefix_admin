import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PasswordRoutingModule } from './password-routing.module';
import { PasswordForgotComponent } from './password-forgot/password-forgot.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { PasswordComponent } from './password.component';
import { CommonPagesModule } from '../common-pages/common-pages.module';
import { PasswordGuardGuard } from 'src/app/guards/password-guard.guard';

@NgModule({
  imports: [
    CommonModule,
    CommonPagesModule,
    PasswordRoutingModule
  ],
  providers: [PasswordGuardGuard],
  declarations: [PasswordComponent, PasswordForgotComponent, PasswordResetComponent]
})
export class PasswordModule { }
