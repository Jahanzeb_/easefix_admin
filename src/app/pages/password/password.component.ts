import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-password',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class PasswordComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }
}
