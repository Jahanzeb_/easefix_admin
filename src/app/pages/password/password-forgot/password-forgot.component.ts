import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../services/http-service';
import { ToastrService } from 'ngx-toastr';
import { Globals } from '../../../Globals';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-password-forgot',
  templateUrl: './password-forgot.component.html',
  styleUrls: ['./password-forgot.component.scss']
})
export class PasswordForgotComponent implements OnInit {

  // Public Functions
  public globals = Globals;

  // variable type ANY
  model: any = {}
  typeOfRequestSend: any;
  dataToSend: any;

  // variable type string
  requestUrl: string;

  constructor(private service: HttpService, private toastrService: ToastrService, private route: ActivatedRoute, public router: Router) { }

  ngOnInit() { }

  // forgot password
  forgotPassword(form: NgForm) {
    this.dataToSend = {
      email: this.model.email,
    }
    this.requestUrl = this.globals.urls.post.headerNotifcations;
    this.typeOfRequestSend = this.service.postRequest(this.requestUrl, this.dataToSend);

    this.typeOfRequestSend.subscribe(
      res => {
        if (res.message === "Success") {
          this.service.showSuccess('Email sent successfully.', 'Forgot Password');
          this.router.navigate(['/login']);
        }
        else if (res.message !== "Success") {
          this.service.showError(res['message'], 'Forgot Password');
        }
      },
      err => {
        if(err.error)
        this.service.showError(err.error.message);
      },
    );
  }
}
