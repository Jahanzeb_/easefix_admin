import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../services/http-service';
import { ToastrService } from 'ngx-toastr';
import { Globals } from '../../../Globals';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent implements OnInit {

  // Public Functions
  public globals = Globals;

  // variable type ANY
  model: any = {}
  typeOfRequestSend: any;
  dataToSend: any;
  token: any;

  // variable type string
  requestUrl: string;

  constructor(private service: HttpService, private toastrService: ToastrService, private route: ActivatedRoute, public router: Router) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get('token');
  }

  // reset password
  resetPassword(form: NgForm) {
    if (this.model.password && !this.model.confirmPassword || !this.model.password && this.model.confirmPassword) {
      this.service.showError('Password is required', 'Password');
      return;
    }
    if (this.model.password && this.model.confirmPassword && this.model.password !== this.model.confirmPassword) {
      this.service.showError('Password does not match', 'Password');
      return;
    }
    this.dataToSend = {
      password: this.model.password,
      token: this.token
    }
    this.requestUrl = this.globals.urls.post.headerNotifcations;
    this.typeOfRequestSend = this.service.postRequest(this.requestUrl, this.dataToSend);

    this.typeOfRequestSend.subscribe(
      res => {
        if (res.message === "Success") {
          this.service.showSuccess('Password reset successfully.', 'Password Reset');
          this.router.navigateByUrl('/');
        }
        else if (res.message !== "Success") {
          this.service.showError(res['message'], 'Password Reset');
        }
      },
      err => {
        this.service.showError(err.error.message);
      },
    );
  }
}
