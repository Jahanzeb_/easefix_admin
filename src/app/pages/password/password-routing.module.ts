import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PasswordForgotComponent } from './password-forgot/password-forgot.component';
import { PasswordComponent } from './password.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { PasswordGuardGuard } from 'src/app/guards/password-guard.guard';

const routes: Routes = [{
  path: '',
  component: PasswordComponent,
  // canActivate: [PasswordGuardGuard],
  children: [
    {
      path: 'reset',
      component: PasswordResetComponent,
    },
    {
      path: 'forgot',
      component: PasswordForgotComponent,
    },
    {
      path: '',
      redirectTo: 'forgot',
      pathMatch: 'full'
    }
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PasswordRoutingModule { }
