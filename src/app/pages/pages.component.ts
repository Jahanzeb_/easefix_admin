import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pages',
  template: `
    <app-header></app-header>
    <router-outlet></router-outlet>
  `,
})
export class PagesComponent implements OnInit {

  constructor( private router: Router) { }

  ngOnInit() {
    let userData = localStorage.getItem('TradeMen-admin-data');
    let redirectAdmin = '';
    if (userData) {
      let adminData = JSON.parse(userData);
      if (adminData.dashboard === 1) {
        redirectAdmin = 'map';
        this.router.navigate(['/map']);
      }
      if (adminData.map === 1) {
        redirectAdmin = 'workers';
        this.router.navigate(['/workers']);
        console.log('workers', redirectAdmin);
      }
      if (adminData.workers === 1) {
        redirectAdmin = 'users';
        this.router.navigate(['/users']);
        console.log('users', redirectAdmin);
      }
      if (adminData.users === 1) {
        redirectAdmin = 'jobs';
        this.router.navigate(['/jobs']);
        console.log('jobs', redirectAdmin);
      }
      if (adminData.jobs === 1) {
        redirectAdmin = 'profile';
        this.router.navigate(['/profile']);
        console.log('profile', redirectAdmin);
      }
      else if (this.router.url === '/') {
        redirectAdmin = 'dashboard';
        this.router.navigate(['/dashboard']);
        console.log('dashboard', redirectAdmin);
      }
    }

  }
}
