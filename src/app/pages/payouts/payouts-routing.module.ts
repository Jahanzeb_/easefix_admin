import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PayoutsListComponent } from './payouts-list/payouts-list.component';
import { PayoutDetailsComponent } from './payout-details/payout-details.component';
import { RouteResolverService } from 'src/app/services/route-resolver.service';
import { Globals } from 'src/app/Globals';
import { RouterGuard } from '../../guards/route.guard';

const routes: Routes = [{
  path: '',
  canActivate: [RouterGuard],
  data: { payouts: 1 },
  children: [
    {
      path: 'payout-details/:id',
      component: PayoutDetailsComponent,
      resolve: {
        resolvedData: RouteResolverService
      },
      data: { apiUrl: Globals.urls.settings.payouts.get, methodType: 'GET', paramsAvailable: true }
    },
    {
      path: '',
      pathMatch: 'full',
      component: PayoutsListComponent
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PayoutsRoutingModule { }
