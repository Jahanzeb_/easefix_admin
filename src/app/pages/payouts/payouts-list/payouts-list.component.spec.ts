import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayoutsListComponent } from './payouts-list.component';

describe('PayoutsListComponent', () => {
  let component: PayoutsListComponent;
  let fixture: ComponentFixture<PayoutsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayoutsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayoutsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
