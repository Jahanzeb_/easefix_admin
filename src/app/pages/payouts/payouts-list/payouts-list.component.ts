import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { of as observableOf, Subscription } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { HttpService } from 'src/app/services/http-service';
import { Globals } from 'src/app/Globals';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { HttpParams } from '@angular/common/http';
import * as moment from 'moment';
import { Router } from '@angular/router';


@Component({
  selector: 'app-payouts-list',
  templateUrl: './payouts-list.component.html',
  styleUrls: ['./payouts-list.component.scss']
})
export class PayoutsListComponent implements OnInit {

  globals = Globals;

  displayedColumns = [];
  resultsLength: number = 10;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  // dataSource = new MatTableDataSource<payoutInterface>([])
  // dataSource: MatTableDataSource<any>;
  dataSource:any=[];

  payout$: Subscription = null;

  isLoadingResults: boolean = false;

  weeksArr: Array<any> = [];
  years: Array<any> = [];
  selectedWeek: any;
  totalWeeks: number;
  weekStart: any;
  weekEnd: any;

  currentYear = moment().year();
  selectedYear: any = moment().year();

  @ViewChild(MatPaginator) paginator: MatPaginator;


  constructor(public service: HttpService, private router: Router) {

    this.displayedColumns = ['id', 'photo', 'name', 'phone', 'email', 'earning', 'status', 'action'];

    this.totalWeeks = moment().isoWeek() + 1;   // total weeks will be equal to current week number


    // we have to show years in dropdown from 2019 onwards
    if (moment(2019).isSame(this.currentYear)) {

      // if current year is exactly 2019, just push 2019
      this.years.push({ year: this.currentYear })

    } else if (moment(2019).isBefore(this.currentYear)) {

      // else iterate over loop and push every year starts from 2019 till current year
      for (let index = 2019; index <= this.currentYear; index++) {
        this.years.push({ year: index })
      }

    }

    for (let i = 1; i < this.totalWeeks; i++) {

      this.weekStart = moment().utc().isoWeeks(i).startOf('isoWeek').format('MMM DD, YYYY');

      this.weekEnd = moment().utc().isoWeeks(i).endOf('isoWeek').format('MMM DD, YYYY');

      this.weeksArr.push({
        duration: this.weekStart + ' - ' + this.weekEnd,
        weekNumber: i
      });

    }

  }

  ngOnInit() {

    this.selectedWeek = this.weeksArr[this.weeksArr.length - 1];

  }

  ngAfterViewInit() {
    this.paginator.pageIndex = 0;
    this.payoutList();
  }

  selectWeek(week) {
    this.selectedWeek = week;
    this.payoutList();
  }

  selectYear(year) {
    this.selectedYear = year;
  }

  payoutList() {

    this.isLoadingResults = true;

    this.payout$ = this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {

          let params = new HttpParams()
            .set('offset', (((this.paginator.pageIndex + 1) * 10) - 10).toString())
            .set('weekNumber', this.selectedWeek.weekNumber)
            .set('year', this.selectedYear)

          return this.service.getRequest(this.globals.urls.settings.payouts.list, params);

        }),

        map(
          res => {
            if (res.response === 200) {

              this.isLoadingResults = false;
              this.resultsLength = res.data.count;
              return res.data.weeklyEarnings;

            }
          }),
        catchError(() => {

          this.isLoadingResults = false;
          return observableOf([]);

        })
      ).subscribe(
        data => {
          if (data) {
            this.dataSource = data;
          }
          else this.dataSource = new MatTableDataSource([]);
          return this.dataSource
        },
        err => {
          this.service.showError(err.error.message);
        }),

      this.dataSource.paginator = this.paginator;
  }

  payoutDetails(id) {
    this.router.navigate(['admin/payout-details', id, this.selectedWeek.weekNumber]);
  }

  ngOnDestroy(): void {
    if (this.payout$) this.payout$.unsubscribe();
  }

}

export interface payoutInterface {
  [index: number]: {
    id: number;
    profileImage: string;
    name: string;
    phone: number;
    email: string;
    earning: number;
    status: number;
  }
}