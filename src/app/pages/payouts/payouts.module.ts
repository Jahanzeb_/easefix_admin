import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PayoutsRoutingModule } from './payouts-routing.module';
import { PayoutsListComponent } from './payouts-list/payouts-list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTableModule, MatToolbarModule, MatProgressSpinnerModule, MatPaginatorModule, MatCardModule, MatDialogModule } from '@angular/material';
import { PayoutDetailsComponent } from './payout-details/payout-details.component';
import { PayoutsModalComponent } from '../modals/payouts-modal/payouts-modal.component';
import { SharedPipesModule } from '../shared-pipes/shared-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    MatTableModule,
    MatToolbarModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatCardModule,
    MatDialogModule,
    SharedPipesModule,
    PayoutsRoutingModule
  ],
  declarations: [PayoutsListComponent, PayoutDetailsComponent, PayoutsModalComponent],
  entryComponents: [PayoutsModalComponent]
})
export class PayoutsModule { }
