import { Component, OnInit } from '@angular/core';
import { Globals } from 'src/app/Globals';
import { MatDialog, MatTableDataSource } from '@angular/material';
// import { MatTableDataSource, MatPaginator } from '@angular/material';
import { PayoutsModalComponent } from '../../modals/payouts-modal/payouts-modal.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { HttpService } from 'src/app/services/http-service';

@Component({
  selector: 'app-payout-details',
  templateUrl: './payout-details.component.html',
  styleUrls: ['./payout-details.component.scss']
})
export class PayoutDetailsComponent implements OnInit {

  globals = Globals;
  // Table Settings
  // dataSource: payoutDetailInterface;
  // dataSource: MatTableDataSource<any>;
  dataSource:any=[];
  displayedColumns = [];
  resultsLength: number = 10;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  payoutDetails: any = [];
  payout$: Subscription = null
  isLoadingResults: boolean = false;
  localStorage: any;

  constructor(public dialog: MatDialog, private route: ActivatedRoute, private router: Router, private service: HttpService) {

    this.displayedColumns = ['jobId', 'jobCost', 'spEarning', 'address', 'jobTime', 'action'];
    this.route.data.subscribe(data => this.payoutDetails = data['resolvedData'].data);

  }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem('TradeMen-admin-data'))) {
      this.localStorage = JSON.parse(localStorage.getItem('TradeMen-admin-data'))
  }
    this.dataSource = this.payoutDetails.jobData;
    this.resultsLength= this.dataSource.length;
  }

  viewJob(id) {
    this.router.navigate(['jobs/job-detail/' , id , 'payouts']);
  }

  viewProfile(id) {
    this.router.navigate(['workers/profile/current', id]);
  }

  payPayout() {
    const params = {
      id: this.payoutDetails._id,
      status: 1
    };

    let url = '';
    url = this.globals.urls.settings.payouts.pay;

    this.isLoadingResults = true;

    this.payout$ = this.service.putRequest(url, params)
      .subscribe(
        res => {

          this.isLoadingResults = false;
          this.payoutDetails.status=1;
          this.service.showSuccess('Payout updated successfully.', 'Payout');
          this.router.routeReuseStrategy.shouldReuseRoute = function () { return false; }; // for reloading page
        },
        err => {
          this.isLoadingResults = false;
          // this.service.showError(err);
        }
      );
  }


  openDialog(data): void {
    const dialogRef = this.dialog.open(PayoutsModalComponent, {
      width: '400px',
      height: '200px',
      data: { data }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.payPayout();
      }
    });
  }

}

export interface payoutDetailInterface {
  [index: number]: {
    jobId: string;
    jobCost: number;
    spEarning: number;
    address: string;
    jobTime: number;
  }
}
