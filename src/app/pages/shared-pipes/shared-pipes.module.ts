import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { momentAgoPipe } from 'src/app/pipes/momentAgoPipe';
import { dateTimeFormatPipe } from 'src/app/pipes/dateTimeFormatPipe';
import { dateTimeFormatIsoPipe } from 'src/app/pipes/dateTimeFormatIsoPipe';
import { dateFormatPipe } from 'src/app/pipes/dateFormatPipe';
import { timeFormatPipe } from 'src/app/pipes/timeFormatPipe';
import { dateFormatIsoPipe } from 'src/app/pipes/dateFormatIsoPipe';
import { GdprDirective } from '../../directives/gdpr.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    momentAgoPipe,
    dateTimeFormatPipe,
    dateTimeFormatIsoPipe,
    dateFormatPipe,
    timeFormatPipe,
    dateFormatIsoPipe,
    GdprDirective
  ],
  exports: [
    momentAgoPipe,
    dateTimeFormatPipe,
    dateTimeFormatIsoPipe,
    dateFormatPipe,
    timeFormatPipe,
    dateFormatIsoPipe,
    GdprDirective
  ]
})
export class SharedPipesModule { }
