import { Component, OnInit, ViewChild } from '@angular/core';
import { Globals, checkIfOnlySpaces } from 'src/app/Globals';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpService } from 'src/app/services/http-service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-admin-profile',
  templateUrl: './admin-profile.component.html',
  styleUrls: ['./admin-profile.component.scss']
})
export class AdminProfileComponent implements OnInit {

  globals = Globals;

  model: any = {}

  localStorage: any;

  profile$: Subscription = null;

  isLoadingResults: boolean = false;

  @ViewChild('updateProfileForm') form;

  constructor(private service: HttpService, private router: Router) { }

  ngOnInit() {
    this.localStorage = JSON.parse(localStorage.getItem('TradeMen-admin-data'));
    this.getProfile();
  }

  getProfile() {
    this.isLoadingResults = true;

    this.profile$ = this.service.getRequest(Globals.urls.settings.profile.get, {})
      .pipe(
        map(res => res.data.account),
    ).subscribe(
      data => {

        this.model.name = data.name;
        this.model.lastName = data.lastName;
        this.model.email = data.email;
        this.model.mobileNumber = data.mobileNumber;

        this.model.canAcceptWorkerRequest = data.canAcceptWorkerRequest
        this.model.canCreateNewWorker = data.canCreateNewWorker
        this.model.canCreateWorkerJob = data.canCreateWorkerJob
        this.model.canRespondRequest = data.canRespondRequest

        this.model.password = '';
        this.model.confirmPassword = '';
        this.isLoadingResults = false;

      },
      err => {
        this.model = null;
        this.isLoadingResults = false;
        this.service.showError(err.error.message);
      }
    )
  }

  updateProfile(form: NgForm) {

    if (this.model.password && !this.model.confirmPassword || !this.model.password && this.model.confirmPassword) {
      this.service.showError('Password is required', 'Password');
      return;
    }
    if (this.model.password && this.model.confirmPassword && this.model.password !== this.model.confirmPassword) {
      this.service.showError('Password does not match', 'Password');
      return;
    }

    let url = this.globals.urls.settings.profile.update,
      params:any = { adminId: this.localStorage._id, name: this.model.name, lastName: this.model.lastName, mobileNumber: this.model.mobileNumber},
      serviceType = this.service.putRequest(url, params);
      if(this.model.password.length>2){
        params.password=this.model.password 
      }
    this.isLoadingResults = true;

    this.profile$ = serviceType
      .subscribe(
        res => {

          this.isLoadingResults = false;

          if (res.response === 200) {
            this.service.showSuccess('Profile Successfully updated', 'Profile');
            this.getProfile();
          }
          else if (res.response == 400) {
            this.service.showError(res['message'], 'Profile');
          }
        },
        err => {
          this.isLoadingResults = false;
          this.service.showError(err.error.message);
        }
      );

  }

  public checkIfOnlySpaces(control: string) {
    return checkIfOnlySpaces(this.form, control);
  }

  ngOnDestroy(): void {
    if (this.profile$) this.profile$.unsubscribe();
  }

}
