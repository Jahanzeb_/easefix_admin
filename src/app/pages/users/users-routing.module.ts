import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { UsersComponent } from "./users.component";
import { UsersListingComponent } from "./users-listing/users-listing.component";
import { UserProfileComponent } from "./user-profile/user-profile.component";
import { UserPackageDetailComponent } from "./user-package-detail/user-package-detail.component";
import { UserPackageTrackingComponent } from "./user-package-tracking/user-package-tracking.component";
import { RouterGuard } from "../../guards/route.guard";
// import { FeedbackListingComponent } from "./feedback-listing/feedback-listing.component";
import { BlockUserComponent } from "./block-user/block-user.component";
import { UserActivityComponent } from "./user-activity/user-activity.component";

const routes: Routes = [
  {
    path: "",
    component: UsersComponent,
    canActivate: [RouterGuard],
    data: { users: 1 },
    children: [
      {
        path: "",
        component: UsersListingComponent,
        pathMatch: "full",
      },
      {
        path: "profile/:userId",
        component: UserProfileComponent,
      },
      {
        path: "package-detail/:packageId",
        component: UserPackageDetailComponent,
      },
      {
        path: "package-tracking/:packageId/:driverId/:userId",
        component: UserPackageTrackingComponent,
      },
      // {
      //   path: "feedback",
      //   component: FeedbackListingComponent,
      // },
      {
        path: "block-user",
        component: BlockUserComponent,
      },
      {
        path: "user-activity",
        component: UserActivityComponent,
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule { }
