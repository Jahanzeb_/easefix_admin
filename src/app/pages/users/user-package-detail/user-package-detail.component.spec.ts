import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPackageDetailComponent } from './user-package-detail.component';

describe('UserPackageDetailComponent', () => {
  let component: UserPackageDetailComponent;
  let fixture: ComponentFixture<UserPackageDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPackageDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPackageDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
