import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
} from "@angular/core";
import { MatPaginator, MatTableDataSource } from "@angular/material";
import {
  map,
  switchMap,
  startWith,
  debounceTime,
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import { Subscription, fromEvent } from "rxjs";
import { HttpService } from "../../../services/http-service";
import { Globals } from "../../../Globals";
import { ExcelService } from "src/app/services/excel.service";
import { Sort } from "@angular/material/sort";
import * as moment from "moment";

@Component({
  selector: "app-users-listing",
  templateUrl: "./users-listing.component.html",
  styleUrls: ["./users-listing.component.scss"],
})
export class UsersListingComponent implements OnInit, AfterViewInit {
  displayedColumns = [];
  // dataSource: MatTableDataSource<any>;
  dataSource: any = [];
  private httpSub$: Subscription = null;
  isLoadingResults = false;
  totalUsers = 0;
  public globals = Globals;
  public translation: any;
  statusFilterText = { name: "Status", value: "" };
  jobStatusText = [
    { name: "All", value: "" },
    { name: "Active", value: false },
    { name: "Blocked", value: true },
  ];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("input") input: ElementRef;

  public users: Array<any> = [];

  constructor(
    public service: HttpService,
    private excelService: ExcelService
  ) { }

  //Set total records
  resultsLength: number = 10;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  inputString: string;
  dataToSend: any;
  usersExportData: any = [];

  offset: number = 0;
  limit: number = 100;

  totalRecords: number = 0;
  localStorage: any;

  ngOnInit() {
    if (JSON.parse(localStorage.getItem("TradeMen-admin-data"))) {
      this.localStorage = JSON.parse(
        localStorage.getItem("TradeMen-admin-data")
      );
    }
    this.displayedColumns = [
      "Id",
      "image",
      "name",
      "Mobile Number",
      "Email Address",
      "totalJobSort",
      "activeJobSort",
      "joining",
      "Status",
      "rating",
      "refree",
      "action"
    ];

    // Fetch data for xcel sheet DRIVERS
    // this.getUsersExportsData(this.offset);
  }
  ngAfterViewInit() {
    this.httpSub$ = fromEvent(this.input.nativeElement, "keyup")
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.inputString = this.input.nativeElement.value;
          this.search({ searchText: this.input.nativeElement.value });
        })
      )
      .subscribe();

    this.getUsers();
  }

  filter(job) {
    this.statusFilterText = job;
    // this.selectedVehicleTypeId = job.id;
    this.search({ statusType: job.value.toString() });
  }

  search(input) {
    console.log("this.statusFilterText");
    this.isLoadingResults = true;
    let offset =
      (this.paginator.pageIndex + 1) * this.paginator.pageSize -
      this.paginator.pageSize;
    let limit = this.paginator.pageSize;
    let spListUrl = this.globals.urls.TradeAdmin.users.fetchUsersList;
    spListUrl = spListUrl.replace(":limit", limit.toString());
    spListUrl = spListUrl.replace(":offset", offset.toString());
    spListUrl = spListUrl.replace(
      ":search_text",
      input.searchText || this.inputString || ""
    );
    spListUrl = spListUrl.replace(
      ":status",
      input.statusType || this.statusFilterText.value
    );
    spListUrl = spListUrl.replace(":jobTypeId", "");
    spListUrl = spListUrl.replace(":activeJobSort", input.activeJobSort || "");
    spListUrl = spListUrl.replace(":totalJobSort", input.totalJobSort || "");
    this.service
      .getRequest(spListUrl)
      .pipe(
        map((res) => {
          return res.data;
        })
      )
      .subscribe(
        (data) => {
          this.isLoadingResults = false;
          this.dataSource = data.users;
          this.totalRecords = data.users.length;
          this.resultsLength = data.count;
          this.dataToSend = data.users;
        },
        (err) => {
          this.isLoadingResults = false;
          this.service.showError(err.error.message);
        }
      );
  }

  getUsers() {
    this.isLoadingResults = true;
    this.httpSub$ = this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {
          // let params = {
          //   offset: ((this.paginator.pageIndex + 1) * this.paginator.pageSize) - this.paginator.pageSize,
          //   limit: this.paginator.pageSize,
          //   searchText: this.input.nativeElement.value || null
          // };
          this.isLoadingResults = true;
          let offset =
            (this.paginator.pageIndex + 1) * this.paginator.pageSize -
            this.paginator.pageSize;
          let limit = this.paginator.pageSize;
          let spListUrl = this.globals.urls.TradeAdmin.users.fetchUsersList;
          spListUrl = spListUrl.replace(":limit", limit.toString());
          spListUrl = spListUrl.replace(":offset", offset.toString());
          spListUrl = spListUrl.replace(":search_text", "");
          spListUrl = spListUrl.replace(":status", this.statusFilterText.value);
          spListUrl = spListUrl.replace(":jobTypeId", "");
          spListUrl = spListUrl.replace(":sort", "");
          spListUrl = spListUrl.replace(":activeJobsCount", "");
          return this.service.getRequest(spListUrl);
        }),
        map((res) => {
          return res.data;
        })
      )
      .subscribe(
        (data) => {
          this.totalUsers = data.count;
          this.isLoadingResults = false;
          this.resultsLength = data.count;
          this.dataSource = data.users;
          this.totalRecords = data.users.length;
          this.dataToSend = data.users;
        },
        (err) => {
          this.isLoadingResults = false;
          this.service.showError(err.error.message);
        }
      );
  }

  exportAsXLSX(): void {
    for (let i = 0; i < this.dataToSend.length; i++) {
      let element = this.dataToSend[i];
      let obj = {
        Name: element.name,
        Mobile: element.phonee,
        Email: element.emaill,
        "Total Jobs": element.jobsCount,
        "Active Jobs": element.activeJobsCount,
        "Joining Date": moment(element.createdAt).format("MM/DD/YYYY"),
        Status: element.status ? "Blocked" : "Active",
      };
      this.usersExportData.push(obj);
    }
    this.excelService.exportAsExcelFile(this.usersExportData, "Users");
    this.usersExportData = [];
  }

  // get data for exporting all drivers
  // getUsersExportsData(offset) {
  //   this.dataToSend = {
  //     limit: this.limit,
  //     offset: offset || 0
  //   }
  //   this.httpSub$ = this.service.postRequest(this.globals.urls.post.headerNotifcations, this.dataToSend).subscribe(
  //     res => {
  //       if (res === 200) {
  //         let driversData = res.data.usersData.rows;

  //         if (driversData.length > 0) {

  //           driversData.forEach(singleDriver => {
  //             this.usersExportData.push(singleDriver);
  //           });

  //           if (driversData.count > this.limit) {
  //             this.getUsersExportsData(this.limit);
  //           }
  //         }
  //       }

  //       else if (res.response == 400) {
  //         this.service.showError(res['message'], 'Dashboard Statistics');
  //       }
  //     },
  //     err => {
  //       this.service.showError(err.error.message);
  //     },
  //   );
  // }

  ngOnDestroy(): void {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }

  sortData(sort: Sort) {
    const data = this.dataSource.slice();
    // if (!sort.active || sort.direction === '') {
    //   this.sortedData = data;
    //   return;
    // }

    this.dataSource = data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "totalJobSort":
          return compare(a.jobsCount, b.jobsCount, isAsc);
        case "activeJobSort":
          return compare(a.activeJobsCount, b.activeJobsCount, isAsc);
        case "joining":
          return compare(
            moment(a.createdAt).unix(),
            moment(b.createdAt).unix(),
            isAsc
          );
        case "rating":
          return compare(a.avgRating, b.avgRating, isAsc);
        default:
          return 0;
      }
    });
    this.dataToSend = [...this.dataSource];
  }
}
function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
