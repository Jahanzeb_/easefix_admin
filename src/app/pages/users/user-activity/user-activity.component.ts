import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, Sort } from '@angular/material';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, startWith, switchMap, tap } from 'rxjs/operators';
import { Globals } from 'src/app/Globals';
import { ExcelService } from 'src/app/services/excel.service';
import { HttpService } from 'src/app/services/http-service';
import * as moment from "moment";

@Component({
  selector: 'app-user-activity',
  templateUrl: './user-activity.component.html',
  styleUrls: ['./user-activity.component.scss']
})
export class UserActivityComponent implements OnInit {
  displayedColumns = [];
  resultsLength: number = 10;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  isLoadingResults = false;
  public globals = Globals;
  private users$: Subscription = null;
  totalUsers = 0;
  dataSource: any = [];
  totalRecords = 0;
  dataToSend = [];
  offset: number = 0;
  limit: number = 100;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    public service: HttpService,
    private excelService: ExcelService
  ) { }
  ngOnInit() {
    this.displayedColumns = [
      "Mobile Number",
      "Step Completed",
      "Service",
      "dateTime"
    ];
  }

  ngAfterViewInit() {
    this.paginator.pageIndex = 0;
    this.getUsers();
  }

  getUsers() {
    this.users$ = this.paginator.page.pipe(
      startWith({}),
      switchMap(() => {
        this.isLoadingResults = true;
        let offset =
          (this.paginator.pageIndex + 1) * this.paginator.pageSize -
          this.paginator.pageSize;
        let limit = this.paginator.pageSize;
        let params = {
          limit,
          offset
        }
        let userListUrl = this.globals.urls.TradeAdmin.users.fetchUsersActivity;
        // userListUrl = userListUrl.replace(":limit", limit.toString());
        // userListUrl = userListUrl.replace(":offset", offset.toString());
        return this.service.getRequest(userListUrl, params);
      }),
    ).subscribe(
      (res) => {
        this.totalUsers = res.data.count;
        this.isLoadingResults = false;
        this.resultsLength = res.data.count;
        this.dataSource = res.data.users;
        this.totalRecords = res.data.users.length;
        this.dataToSend = res.data.users;

      },
      (err) => {
        console.log('err', err)
        this.isLoadingResults = false;
        this.service.showError(err.message);
      }
    )
  }

  sortData(sort: Sort) {
    const data = this.dataSource.slice();

    this.dataSource = data.sort((a, b) => {
      const isAsc = sort.direction === "asc";
      switch (sort.active) {
        case "dateTime":
          return compare(
            moment(a.createdAt).unix(),
            moment(b.createdAt).unix(),
            isAsc
          );

        default:
          return 0;
      }
    });
  }

}
function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}