import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnDestroy,
} from "@angular/core";
import { MatPaginator, MatTableDataSource } from "@angular/material";
import { of as observableOf, fromEvent, Subscription, forkJoin } from "rxjs";
import { HttpService } from "../../../services/http-service";
import { Globals } from "../../../Globals";
import {
  map,
  switchMap,
  startWith,
  debounceTime,
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import { Router, ActivatedRoute } from "@angular/router";
import {
  DomSanitizer,
  SafeResourceUrl,
  SafeUrl,
} from "@angular/platform-browser";

import * as moment from "moment";
import { MatDialog } from "@angular/material";
import { ConfirmDialougComponent } from "../../common-pages/confirm-dialoug/confirm-dialoug.component";
import { BlockUserComponent } from "../block-user/block-user.component";
import { FeedbackListingComponent } from "../feedback-listing/feedback-listing.component";
@Component({
  selector: "app-user-profile",
  templateUrl: "./user-profile.component.html",
  styleUrls: ["./user-profile.component.scss"],
})
export class UserProfileComponent implements OnInit, AfterViewInit, OnDestroy {
  // Public Depedency
  public globals = Globals;

  // Table settings
  displayedColumns = [];
  dataSource: MatTableDataSource<any>;

  // Observables/Subscriptions
  private httpSub$: Subscription = null;

  // Type number variables
  resultsLength: number = 10;
  inputString: string;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  totalRecords: number = 0;
  packageType: number = 1; // package type by default is active
  userId: string; // user id from url
  // boolean
  isLoadingResults = false;
  enableBtn = false;
  // Type any
  packagesData: any = [];
  localStorage: any;
  exportUserDataURL: string;
  exportUserJobsDataURL: string;
  GdprDData: any;
  showData: any = {
    mobile: false,
    email: false,
    IdImage: false,
  };
  showEye: any = {
    mobile: true,
    email: true,
    IdImage: true,
  };
  typeOfPackage: any = 1;
  jobStatusText = [
    { name: "All", value: "" },
    { name: "Open", value: 1 },
    { name: "Assigned", value: 2 },
    { name: "InProgress", value: 3 },
    { name: "Completed", value: 4 },
    // { name: 'Rated By User', value: 5 },`
    { name: "Cancellation", value: 6 },
    { name: "Rejected", value: 7 },
    { name: "Time Out", value: 8 },
  ];
  statusFilterText = { name: "Status", value: "" };
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild("input") input: ElementRef;

  constructor(
    public service: HttpService,
    private router: Router,
    private route: ActivatedRoute,
    private _sanitizer: DomSanitizer,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem("TradeMen-admin-data"))) {
      this.localStorage = JSON.parse(
        localStorage.getItem("TradeMen-admin-data")
      );
    }
    this.userId = this.route.snapshot.paramMap.get("userId");
    this.displayedColumns = [
      "id",
      "jobHours",
      "job",
      "address",
      "amount",
      "Date",
      "Status",
      "action",
    ];
  }
  filter(job) {
    this.statusFilterText = job;
    // this.selectedVehicleTypeId = job.id;
    this.search({ status: job.value.toString() });
  }

  feedbackDial(): void {
    let className = 'feedbackDialogue';
    if (!this.packagesData.ratings.length) {
      className = 'noDataDialogue'
    }
    let dialogRef = this.dialog.open(FeedbackListingComponent, {
      width: "600px",
      // height: "700px",
      maxHeight: "600px",
      panelClass: className,
      data: {
        ratings: this.packagesData.ratings,
      },
    });
    // dialogRef.afterClosed().subscribe((result) => {
    //   let commission = this.previewData.spProfile.commissionPercentage;
    //   if (result.terms == true) {
    //     commission = this.previewData.spProfile.commissionPercentage;
    //   } else {
    //     commission = result.commission;
    //   }
    //   this.approve(commission);
    // });
  }

  blockUserDial(): void {
    let dialogRef = this.dialog.open(BlockUserComponent, {
      width: "400px",
      height: "300px",
      panelClass: "blockUserDialogue",
      data: {
        userId: this.userId,
        userType: 'user'
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.packagesData.userProfile.status = result.isBlocked;
        this.packagesData.userProfile.reason = result.blockReason;
      }
      // this.getListing();
    });
  }

  ngAfterViewInit() {
    this.httpSub$ = fromEvent(this.input.nativeElement, "keyup")
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.inputString = this.input.nativeElement.value;
          this.search({ searchText: this.input.nativeElement.value });
        })
      )
      .subscribe();
    this.getExport();
    this.getListing();
  }
  Enablegdpr(from) {
    this.isLoadingResults = true;
    if (from == "mobile") {
      this.showData.mobile = true;
    } else if (from == "email") {
      this.showData.email = true;
    } else if (from == "IdImage") {
      this.showData.IdImage = true;
    }
  }

  GotGDPRDATA(event) {
    this.isLoadingResults = false;
    if (event.data.phoneNumber && event.data.phonePreFix) {
      this.packagesData.userProfile.phone =
        event.data.phonePreFix.toString() + event.data.phoneNumber.toString();
      this.showEye.mobile = false;
    } else if (event.data.email) {
      this.packagesData.userProfile.email = event.data.email;
      this.showEye.email = false;
    }
  }

  userStatusUpdate(status) {
    this.isLoadingResults = true;
    let params = {
      userId: this.userId,
      isBlocked: !status,
      userType: "user",
    };
    this.service
      .putRequest(this.globals.urls.TradeAdmin.activeBlockUser, params)
      .subscribe(
        (res) => {
          this.isLoadingResults = false;
          this.packagesData.userProfile.status = res.data.isBlocked;
          this.service.showSuccess("User status updated successfully.", "User");
        },
        (err) => {
          this.isLoadingResults = false;
          this.service.showError(err);
        }
      );
  }
  exportUser() {
    this.isLoadingResults = true;
    let params = {
      userProfileId: this.userId,
      userType: "user",
    };
    this.httpSub$ = this.service
      .postRequest(this.globals.urls.getCSVDetails, params)
      .subscribe(
        (res) => {
          this.isLoadingResults = false;
          this.exportUserDataURL = res.data.userFileUrl;
        },
        (err) => {
          this.isLoadingResults = false;
          this.service.showError(err);
        }
      );
  }

  exportUserJobs() {
    this.isLoadingResults = true;
    let params = {
      userProfileId: this.userId,
      userType: "user",
    };
    this.httpSub$ = this.service
      .postRequest(this.globals.urls.getCSVJob, params)
      .subscribe(
        (res) => {
          this.isLoadingResults = false;
          this.exportUserJobs = res.data.userFileUrl;
        },
        (err) => {
          this.isLoadingResults = false;
          this.service.showError(err);
        }
      );
  }

  getExport() {
    this.isLoadingResults = true;
    let params = {
      userProfileId: this.userId,
      userType: "user",
    };
    this.httpSub$ = forkJoin(
      this.service.postRequest(this.globals.urls.getCSVDetails, params),
      this.service.postRequest(this.globals.urls.getCSVJob, params)
    )
      .pipe(
        map(([exportUserDataURL, exportUserJobs]) => {
          this.exportUserDataURL = exportUserDataURL.data.userFileUrl;
          this.exportUserJobs = exportUserJobs.data.userFileUrl;
        })
      )
      .subscribe(
        (res) => {
          this.enableBtn = true;
          this.isLoadingResults = false;
        },
        (err) => {
          this.isLoadingResults = false;
          this.service.showError(err);
        }
      );
  }

  search(input) {
    this.isLoadingResults = true;
    let offset =
      (this.paginator.pageIndex + 1) * this.paginator.pageSize -
      this.paginator.pageSize;
    let limit = this.paginator.pageSize;
    let spListUrl = this.globals.urls.TradeAdmin.users.fetchUsersJobLists;
    spListUrl = spListUrl.replace(":limit", limit.toString());
    spListUrl = spListUrl.replace(":offset", offset.toString());
    spListUrl = spListUrl.replace(":userId", this.userId.toString());
    spListUrl = spListUrl.replace(
      ":search_text",
      input.searchText || this.inputString || ""
    );
    // spListUrl = spListUrl.replace(':status', this.packageType.toString());
    spListUrl = spListUrl.replace(":status", input.status || 1);
    this.service
      .getRequest(spListUrl)
      .pipe(
        map((res) => {
          return res.data;
        })
      )
      .subscribe(
        (data) => {
          this.isLoadingResults = false;
          this.resultsLength = data.count;
          this.packagesData = data;
          this.totalRecords = data.jobsList.length;
          data.jobsList.forEach((element) => {
            if (element.status == 1) {
              element.statusText = "Open";
            } else if (element.status == 2) {
              element.statusText = "Assigned";
            } else if (element.status == 3) {
              element.statusText = "InProgress";
            } else if (element.status == 4) {
              element.statusText = "Completed";
            } else if (element.status == 5) {
              element.statusText = "Rated By User";
            } else if (element.status == 6) {
              element.statusText = "Cancelled";
            } else if (element.status == 7) {
              element.statusText = "Rejected";
            } else if (element.status == 8) {
              element.statusText = "Time Out";
            } else if (element.status == 11) {
              element.statusText = "Disputed";
            }
          });
          this.dataSource = data.jobsList;
        },
        (err) => {
          this.isLoadingResults = false;
          if (err.error) this.service.showError(err.error.message);
          else this.service.showError(err);
        }
      );
  }

  getListing() {
    // this.isLoadingResults = true;
    this.httpSub$ = this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {
          let offset =
            (this.paginator.pageIndex + 1) * this.paginator.pageSize -
            this.paginator.pageSize;
          let limit = this.paginator.pageSize;
          let spListUrl = this.globals.urls.TradeAdmin.users.fetchUsersJobLists;
          spListUrl = spListUrl.replace(":limit", limit.toString());
          spListUrl = spListUrl.replace(":offset", offset.toString());
          spListUrl = spListUrl.replace(":userId", this.userId.toString());
          spListUrl = spListUrl.replace(":search_text", "");
          spListUrl = spListUrl.replace(":status", this.packageType.toString());

          return this.service.getRequest(spListUrl);
        }),
        map((res) => {
          return res.data;
        })
      )
      .subscribe(
        (data) => {
          // this.isLoadingResults = false;
          data.jobsList.forEach((element) => {
            if (element.status == 1) {
              element.statusText = "Open";
            } else if (element.status == 2) {
              element.statusText = "Assigned";
            } else if (element.status == 3) {
              element.statusText = "InProgress";
            } else if (element.status == 4) {
              element.statusText = "Completed";
            } else if (element.status == 5) {
              element.statusText = "Rated By User";
            } else if (element.status == 6) {
              element.statusText = "Cancelled";
            } else if (element.status == 7) {
              element.statusText = "Rejected";
            } else if (element.status == 8) {
              element.statusText = "Time Out";
            } else if (element.status == 11) {
              element.statusText = "Disputed";
            }
          });
          this.resultsLength = data.count;
          this.dataSource = data.jobsList;
          this.packagesData = data;
          this.totalRecords = data.jobsList.length;
        },
        (err) => {
          this.dataSource = null;
          this.isLoadingResults = false;
          this.service.showError(err.error.message);
        }
      );
  }

  getDateTime(date) {
    return moment(date).format("hh:mm A, DD MMMM, YYYY");
  }

  changeTypeOfPackage(packageToShow) {
    this.showData = {
      mobile: false,
      email: false,
      IdImage: false,
    };
    this.showEye = {
      mobile: true,
      email: true,
      IdImage: true,
    };
    if (packageToShow == "active") {
      this.packageType = 1;
      this.paginator.pageSize = 10;
      this.paginator.pageIndex = 0;
      this.typeOfPackage = 1;
      this.getListing();
    } else if (packageToShow == "past") {
      this.packageType = 2;
      this.paginator.pageSize = 10;
      this.paginator.pageIndex = 0;
      this.typeOfPackage = 0;
      this.getListing();
    }
  }

  update(): void {
    let dialogRef = this.dialog.open(ConfirmDialougComponent, {
      width: "500px",
      panelClass: "ratingDialoug",
      data: {
        callingFromFunction: "DeleteUser",
        DialougHeaderText: "Delete User",
        DialougBodyText:
          "Are you sure you want to permenantly delete user data?",
        ButtonCancelText: "Cancel",
        ButtonSubmitText: "Delete",
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result == true) {
        this.DeleteInfo();
      }
    });
  }

  DeleteInfo() {
    this.isLoadingResults = true;
    let params = {
      _id: this.userId,
      userType: "user",
    };
    this.httpSub$ = this.service
      .postRequest(this.globals.urls.deleteUserCompletely, params)
      .subscribe(
        (res) => {
          this.isLoadingResults = false;
          this.router.navigate(["/users"]);
          this.service.showSuccess("All User data deleted", "User");
        },
        (err) => {
          this.isLoadingResults = false;
          // this.service.showError(err);
        }
      );
  }

  ngOnDestroy(): void {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }
}
