import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersListingComponent } from './users-listing/users-listing.component';
import { UsersComponent } from './users.component';
import { CommonPagesModule } from '../common-pages/common-pages.module';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserPackageDetailComponent } from './user-package-detail/user-package-detail.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatToolbarModule } from '@angular/material';
import { AgmMapOrignalModule } from '../agm-map-orignal/agm-map-orignal.module';
import { UserPackageTrackingComponent } from './user-package-tracking/user-package-tracking.component';
import { SharedPipesModule } from '../shared-pipes/shared-pipes.module';
import { BlockUserComponent } from './block-user/block-user.component';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { FeedbackListingComponent } from './feedback-listing/feedback-listing.component';
import { UserActivityComponent } from './user-activity/user-activity.component';
@NgModule({
  imports: [
    CommonModule,
    CommonPagesModule,
    NgbModule,
    MatToolbarModule,
    AgmMapOrignalModule,
    SharedPipesModule,
    UsersRoutingModule,
    MatFormFieldModule,
    MatInputModule
  ],
  declarations: [UsersComponent, UsersListingComponent, UserProfileComponent, UserPackageDetailComponent, UserPackageTrackingComponent, BlockUserComponent, FeedbackListingComponent, UserActivityComponent],
  entryComponents:[FeedbackListingComponent]
})
export class UsersModule { }
