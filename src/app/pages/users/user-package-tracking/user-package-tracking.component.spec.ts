import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPackageTrackingComponent } from './user-package-tracking.component';

describe('UserPackageTrackingComponent', () => {
  let component: UserPackageTrackingComponent;
  let fixture: ComponentFixture<UserPackageTrackingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPackageTrackingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPackageTrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
