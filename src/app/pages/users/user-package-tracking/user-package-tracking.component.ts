import { Component, OnInit } from '@angular/core';
import { Globals } from 'src/app/Globals';
import { of as observableOf, Subscription } from 'rxjs';
import { HttpService } from 'src/app/services/http-service';
import { Router, ActivatedRoute } from '@angular/router';
import { TimerObservable } from "rxjs/observable/TimerObservable";
import { map } from 'rxjs/internal/operators/map';

@Component({
  selector: 'app-user-package-tracking',
  templateUrl: './user-package-tracking.component.html',
  styleUrls: ['./user-package-tracking.component.scss']
})
export class UserPackageTrackingComponent implements OnInit {

  // Public Functions
  public globals = Globals;

  // type any
  model: any = {};

  // variable type boolean
  isLoadingResults: boolean = true;

  // type number
  lat: number;
  lng: number;

  // IDS for package and driver [assigned]
  driverId: number;
  packageId: number;
  userId: number;

  // Class Subscription
  activePackage$: Subscription = null;

  // Type string
  imageUrl: string = '';

  private alive: boolean; // used to unsubscribe from the TimerObservable // when OnDestroy is called.
  private interval: number

  constructor(public service: HttpService, private router: Router, private route: ActivatedRoute) {
    this.alive = true;
    this.interval = 40000; // every 40 seconds

    this.driverId = parseInt(this.route.snapshot.params.driverId, 10);
    this.packageId = parseInt(this.route.snapshot.params.packageId, 10);
    this.userId = parseInt(this.route.snapshot.params.userId, 10);

    this.imageUrl = '../.../../../../../assets/images/moving_car.png';
  }

  ngOnInit() {
    // Get active job drivers list after every 40 seconds
    TimerObservable.create(0, this.interval)
      .takeWhile(() => this.alive)
      .subscribe(() => {
        this.getactJobsDriversList()
      });
  }

  // get active jobs drivers listing
  getactJobsDriversList() {
    this.activePackage$ = this.service.postRequest(this.globals.urls.post.headerNotifcations, { driverId: this.driverId, packageId: this.packageId })
      .pipe(
        map(res => {
          return res.data.location;
        }),
      ).subscribe(
        data => {
          this.isLoadingResults = false;
          this.model = data;

          this.lat = this.model.latitude;
          this.lng = this.model.longitude;

        },
        err => {
          this.model = null;
          this.isLoadingResults = false;
          this.service.showError(err.error.message);
        }
      );
  }

}
