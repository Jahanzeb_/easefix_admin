import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Globals } from "src/app/Globals";


@Component({
  selector: "app-feedback-listing",
  templateUrl: "./feedback-listing.component.html",
  styleUrls: ["./feedback-listing.component.scss"],
})
export class FeedbackListingComponent implements OnInit {
  ratingsFound: any = [];
  globals = Globals;

  constructor(
    public dialogRef: MatDialogRef<FeedbackListingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.ratingsFound = this.data.ratings;
  }
}
