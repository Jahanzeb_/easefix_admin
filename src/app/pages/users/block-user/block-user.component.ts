import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Globals } from "../../../Globals";
import { HttpService } from "../../../services/http-service";
@Component({
  selector: "app-block-user",
  templateUrl: "./block-user.component.html",
  styleUrls: ["./block-user.component.scss"],
})
export class BlockUserComponent implements OnInit {
  public globals = Globals;
  isLoadingResults: false;
  model: any = {};

  constructor(
    public service: HttpService,
    public dialogRef: MatDialogRef<BlockUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.model = {
      blockText : ""
    };
  }

  userStatusUpdate() {
    // this.isLoadingResults = true;
    let params = {
      userId: this.data.userId,
      isBlocked: true,
      userType: this.data.userType,
      blockReason: this.model.blockText,
    };
    console.log("-------6666------", params);
    // return;

    this.service
      .putRequest(this.globals.urls.TradeAdmin.activeBlockUser, params)
      .subscribe(
        (res) => {
          this.service.showSuccess("User status updated successfully.", "User");
          this.dialogRef.close(params);
        },
        (err) => {
          this.isLoadingResults = false;
          this.service.showError(err);
        }
      );
  }

  onNoClick() {
    this.dialogRef.close();
  }
}
