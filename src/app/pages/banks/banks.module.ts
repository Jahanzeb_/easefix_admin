import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BanksListingComponent } from './banks-listing/banks-listing.component';
import { AddEditBanksComponent } from './add-edit-banks/add-edit-banks.component';
import { BanksRoutingModule } from './banks-routing.module';
import { MatTableModule, MatPaginatorModule, MatProgressSpinnerModule, MatToolbarModule, MatCardModule, MatRadioModule } from '@angular/material';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatToolbarModule,
    MatCardModule,
    NgbModule,
    MatRadioModule,
    FormsModule,
    BanksRoutingModule
  ],
  declarations: [BanksListingComponent, AddEditBanksComponent]
})
export class BanksModule { }
