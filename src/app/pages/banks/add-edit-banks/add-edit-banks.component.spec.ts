import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditBanksComponent } from './add-edit-banks.component';

describe('AddEditBanksComponent', () => {
  let component: AddEditBanksComponent;
  let fixture: ComponentFixture<AddEditBanksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditBanksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditBanksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
