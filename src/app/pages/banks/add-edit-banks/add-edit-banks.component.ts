import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-edit-banks',
  templateUrl: './add-edit-banks.component.html',
  styleUrls: ['./add-edit-banks.component.scss']
})
export class AddEditBanksComponent implements OnInit {

  // variable type ANY
  model: any = {};
  isLoadingResults:boolean=false;

  constructor() { }

  ngOnInit() {
  }

}
