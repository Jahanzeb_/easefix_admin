import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BanksListingComponent } from './banks-listing/banks-listing.component';
import { AddEditBanksComponent } from './add-edit-banks/add-edit-banks.component';
import { RouterGuard } from '../../guards/route.guard';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'add',
      component: AddEditBanksComponent,
      canActivate: [RouterGuard],
      data: { 
        banks: 1 ,
        banksView: 2 ,
      },
    },
    {
      path: 'edit/:id',
      component: AddEditBanksComponent,
      canActivate: [RouterGuard],
      data: { 
        banks: 1 ,
        banksView: 2 ,
      },
    },
    {
      path: '',
      pathMatch: 'full',
      component: BanksListingComponent,
      canActivate: [RouterGuard],
      data: { banks: 1 },
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BanksRoutingModule { }
