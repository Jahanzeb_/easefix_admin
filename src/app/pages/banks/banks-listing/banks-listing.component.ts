import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
@Component({
  selector: 'app-banks-listing',
  templateUrl: './banks-listing.component.html',
  styleUrls: ['./banks-listing.component.scss']
})
export class BanksListingComponent implements OnInit {


  // Table Settings
  displayedColumns = [];
  // dataSource: MatTableDataSource<any>;
  dataSource:any =[];
  resultsLength: number = 10;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  isLoadingResults:boolean=false;
  // totalRecords: number = 0;

  // Type string variables
  statusFilterText = 'Status';


  constructor() {
    this.displayedColumns = ['number', 'bankName', 'status', 'action'];
  }

  ngOnInit() {
    this.dataSource = [
      { number: 1, bankName: 'Mon promocode', status: 1 },
      { number: 1, bankName: 'Tue promocode', status: 1 },
      { number: 1, bankName: 'Wed promocode', status: 1 },
      { number: 1, bankName: 'Thurs promocode', status: 1 },
      { number: 1, bankName: 'Fri promocode', status: 1 }]
  }

  filterByStatus(status) {
    this.statusFilterText = status.name;
  }

}


export interface banksInterface {
  [index: number]: {
    number: number;
    bankName: string;
    status: number;
  }
}