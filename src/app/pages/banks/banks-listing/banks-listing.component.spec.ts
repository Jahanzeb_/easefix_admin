import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BanksListingComponent } from './banks-listing.component';

describe('BanksListingComponent', () => {
  let component: BanksListingComponent;
  let fixture: ComponentFixture<BanksListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BanksListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BanksListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
