import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { Globals } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';
import { map } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-notification',
  templateUrl: './user-notification.component.html',
  styleUrls: ['./user-notification.component.scss']
})
export class UserNotificationComponent implements OnInit {

  // variable names Arr
  variableNames = [];
  userType:any=1;
  private globals = Globals;
    // Type ANY
    typeOfRequestSend: any;

  // Notifications Arr
  notificationData = [];
  // Type String
  requestUrl: string;

  // Type boolean variables
  isLoadingResults = false;
  httpSub$: Subscription = null;
  disableInput: boolean = false;
  localStorage: any;
  constructor(private service: HttpService, private router: Router, private route: ActivatedRoute) {
    this.variableNames = [
      "workerName",
    ]

    // this.notificationData = [
    //   {
    //     heading: "Upon Driver's Package Offer",
    //     id: 1,
    //     sendingMedium: 0,
    //     text: "[DriverName] has offered to drive your package."
    //   }
    // ]
  }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem('TradeMen-admin-data'))) {
      this.localStorage = JSON.parse(localStorage.getItem('TradeMen-admin-data'))
    }
    if (this.localStorage.notifications != 3) {
      this.disableInput = true;
    }
    else {
      this.disableInput = false;
    }
    this.isLoadingResults = true;
    let spDetailsUrl = this.globals.urls.TradeAdmin.notification.getNotifications;
    spDetailsUrl = spDetailsUrl.replace(':userType', this.userType.toString());
    this.httpSub$ = this.service.getRequest(spDetailsUrl)
      .pipe(map(res => res.data))
      .subscribe(
        res => {
          // this.variableNames = res.variables;
          this.notificationData = res.notifications;
          this.isLoadingResults = false;
        },
        err => {
          this.service.showError(err);
          this.isLoadingResults = false;
        }
      );
  }

    // sender notifications update
    updateSenderNotif() {
      this.requestUrl = this.globals.urls.TradeAdmin.notification.postNotifications;
      this.typeOfRequestSend = this.service.postRequest(this.requestUrl, { notificationData: this.notificationData });
      this.isLoadingResults = true;
      this.typeOfRequestSend.subscribe(
        res => {
          if (res.response === 200) {
            this.isLoadingResults = false;
            this.service.showSuccess('User Notifications updated successfully.', 'User Notifications');
          } else if (res.response === 400) {
            this.isLoadingResults = false;
            this.service.showError(res['message'], 'User Notifications');
          }
        },
        err => {
          this.isLoadingResults = false;
          this.service.showError(err.error.message);
        },
      );
    }
  
    ngOnDestroy() {
      if (this.httpSub$) { this.httpSub$.unsubscribe(); }
    }
  



}
