import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkerNotificationComponent } from './worker-notification/worker-notification.component';
import { UserNotificationComponent } from './user-notification/user-notification.component';
import { NotificationHeaderComponent } from './notification-header/notification-header.component';
import { NotificationsRoutingModule } from './notifications-routing.module';
import { MatCardModule, MatProgressSpinnerModule} from '@angular/material';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    FormsModule,
    MatProgressSpinnerModule,
    NotificationsRoutingModule
  ],
  declarations: [WorkerNotificationComponent, UserNotificationComponent, NotificationHeaderComponent]
})
export class NotificationsModule { }
