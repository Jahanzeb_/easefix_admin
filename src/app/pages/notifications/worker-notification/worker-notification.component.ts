import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { Globals } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';
import { map } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-worker-notification',
  templateUrl: './worker-notification.component.html',
  styleUrls: ['./worker-notification.component.scss']
})
export class WorkerNotificationComponent implements OnInit {
  // variable names Arr
  variableNames = [];

  // Notifications Arr
  notificationData = [];
  userType: any = 2;
  private globals = Globals;
  // Type ANY
  typeOfRequestSend: any;
  // Type String
  requestUrl: string;
  // Type boolean variables
  isLoadingResults = false;
  isDisabled: boolean = true;
  httpSub$: Subscription = null;
  disableInput: boolean = false;
  notificationRadius = 0;
  localStorage: any;
  constructor(private service: HttpService, private router: Router, private route: ActivatedRoute) {
    this.variableNames = [
      "userName"
    ]

    this.notificationData = [
      {
        heading: "Upon Driver's Package Offer",
        id: 1,
        sendingMedium: 0,
        text: "[DriverName] has offered to drive your package."
      }
    ]
  }


  ngOnInit() {

    if (JSON.parse(localStorage.getItem('TradeMen-admin-data'))) {
      this.localStorage = JSON.parse(localStorage.getItem('TradeMen-admin-data'))
    }
    if (this.localStorage.notifications != 3) {
      this.disableInput = true;
    }
    else {
      this.disableInput = false;
    }


    this.isLoadingResults = true;
    let spDetailsUrl = this.globals.urls.TradeAdmin.notification.getNotifications;
    spDetailsUrl = spDetailsUrl.replace(':userType', this.userType.toString());
    this.httpSub$ = this.service.getRequest(spDetailsUrl)
      .pipe(map(res => res.data))
      .subscribe(
        res => {
          // this.variableNames = res.variables;
          this.notificationData = res.notifications;
          this.isLoadingResults = false;
        },
        err => {
          this.service.showError(err);
          this.isLoadingResults = false;
        }
      );
  }

  // sender notifications update
  updateSenderNotif() {
    this.requestUrl = this.globals.urls.TradeAdmin.notification.postNotifications;
    this.typeOfRequestSend = this.service.postRequest(this.requestUrl, { notificationData: this.notificationData });
    this.isLoadingResults = true;
    this.typeOfRequestSend.subscribe(
      res => {
        if (res.response === 200) {
          this.isLoadingResults = false;
          this.service.showSuccess('Worker Notifications updated successfully.', 'Worker Notifications');
        } else if (res.response === 400) {
          this.isLoadingResults = false;
          this.service.showError(res['message'], 'Worker Notifications');
        }
      },
      err => {
        this.isLoadingResults = false;
        this.service.showError(err.error.message);
      },
    );
  }

  ngOnDestroy() {
    if (this.httpSub$) { this.httpSub$.unsubscribe(); }
  }


  editFieldsEnable() {
    this.isDisabled = false;
  }
  editFieldsDisable() {
    this.isDisabled = true;
  }

}
