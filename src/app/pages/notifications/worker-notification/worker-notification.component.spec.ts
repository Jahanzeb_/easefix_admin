import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerNotificationComponent } from './worker-notification.component';

describe('WorkerNotificationComponent', () => {
  let component: WorkerNotificationComponent;
  let fixture: ComponentFixture<WorkerNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkerNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkerNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
