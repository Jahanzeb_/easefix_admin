import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotificationHeaderComponent } from './notification-header/notification-header.component';
import { UserNotificationComponent } from './user-notification/user-notification.component';
import { WorkerNotificationComponent } from './worker-notification/worker-notification.component';
import { RouterGuard } from '../../guards/route.guard';

const routes: Routes = [{
  path: '',
  component: NotificationHeaderComponent,
  canActivate: [RouterGuard],
  data: { notifications: 1 },
  children: [
    {
      path: 'user',
      component: UserNotificationComponent,
    },
    {
      path: 'worker',
      component: WorkerNotificationComponent,
    },
    {
      path: '',
      redirectTo: 'user',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationsRoutingModule { }
