import { TermsConditionModule } from './terms-condition.module';

describe('TermsConditionModule', () => {
  let termsConditionModule: TermsConditionModule;

  beforeEach(() => {
    termsConditionModule = new TermsConditionModule();
  });

  it('should create an instance', () => {
    expect(termsConditionModule).toBeTruthy();
  });
});
