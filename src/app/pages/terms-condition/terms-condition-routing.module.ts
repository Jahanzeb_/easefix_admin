import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';
import { RouterGuard } from '../../guards/route.guard';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'list',
      component: ListComponent,
      canActivate: [RouterGuard],
      data: { termsAndConditions: 1 },
    },
    {
      path: 'edit/:id/:dead/:type',
      // canActivate: [RouterGuard],
      data: {
        termsAndConditions: 1,
        termsAndConditionsView: 2
      },
      component: EditComponent
    },
    {
      path: '',
      pathMatch: 'full',
      component: ListComponent
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TermsConditionRoutingModule { }
