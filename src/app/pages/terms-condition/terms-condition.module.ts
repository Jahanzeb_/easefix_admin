import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TermsConditionRoutingModule } from './terms-condition-routing.module';
import { ListComponent } from './list/list.component';
import { MatTableModule, MatToolbarModule, MatProgressSpinnerModule, MatPaginatorModule, MatCardModule } from '@angular/material';
import { SharedPipesModule } from '../shared-pipes/shared-pipes.module';
import { EditComponent } from './edit/edit.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatTableModule,
    MatToolbarModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatCardModule,
    SharedPipesModule,
    TermsConditionRoutingModule
  ],
  declarations: [ListComponent, EditComponent]
})
export class TermsConditionModule { }
