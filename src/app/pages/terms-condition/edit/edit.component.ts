import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from 'src/app/services/http-service';
import { Globals } from 'src/app/Globals';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  globals = Globals;

  model: any = {};
  isLoadingResults: boolean = false;

  tc$: Subscription = null

  @ViewChild('editTc') form;
  disabledBit:any=false
  userType:any;
  localStorage: any;
  disabledBitRole:any=false;
  constructor(private router: Router, private service: HttpService, private route: ActivatedRoute) { 
    this.disabledBit = this.route.snapshot.paramMap.get('dead');
    this.userType = this.route.snapshot.paramMap.get('type');
  }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem('TradeMen-admin-data'))) {
      this.localStorage = JSON.parse(localStorage.getItem('TradeMen-admin-data'))
    }
    if(this.localStorage.termsAndConditions!=3){
      this.disabledBitRole=true;
    }
    else{
      this.disabledBitRole=false;
    }
    this.fetchTc()
  }

  fetchTc() {

    let url: any = this.globals.urls.settings.termsCondition.fetch,
      request: any = this.service.getRequest(url + '/' + this.route.snapshot.paramMap.get('id'));

    this.isLoadingResults = true;

    this.tc$ = request.subscribe(
      res => {

        if (res.response === 200) {
          this.model = res.data;
        } else if (res.response == 400) {
          this.service.showError(res['message'], 'Terms and condition');
        }

        this.isLoadingResults = false;

      },
      err => {

        this.service.showError(err.error.message);
        this.isLoadingResults = false;

      },
    );

  }

  saveTc(form: NgForm) {

    let url: string,
      request: any,
      params: any = {
        currentTermsAndCondititonId: this.route.snapshot.paramMap.get('id'),
        // termsAndCondtionUserType: this.model.termsAndCondtionUserType,
        description: this.model.description,
        descriptionET: this.model.descriptionET,
        descriptionRU: this.model.descriptionRU,
        termsAndCondtionUserType:this.userType,
      };

    url = this.globals.urls.settings.termsCondition.add;
    params.serviceId = this.route.snapshot.paramMap.get('id');
    request = this.service.postRequest(url, params);

    this.isLoadingResults = true;

    request.subscribe(
      res => {
        if (res.response === 200) {

          this.isLoadingResults = false;

          this.service.showSuccess('Terms And Conditions updated successfully.', 'Terms and condition');
          this.router.navigate(['/terms-condition']);
          this.fetchTc();

        }
        else if (res.response == 400) {

          this.isLoadingResults = false;
          this.service.showError(res['message'], 'Terms and condition');

        }
      },
      err => {

        this.isLoadingResults = false;
        this.service.showError(err);

      },
    );

  }

  back() {
    this.router.navigate(['terms-condition']);
  }

}
