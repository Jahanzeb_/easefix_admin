import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { of as observableOf, Subscription } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { HttpService } from 'src/app/services/http-service';
import { Globals } from 'src/app/Globals';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
}) 
export class ListComponent implements OnInit {

  globals = Globals;

  displayedColumns = [];
  resultsLengthSp: number = 10;
  resultsLengthUser: number = 10;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  // dataSource = new MatTableDataSource<termsConditionInterface>([])
  dataSource:any=[];

  tc$: Subscription = null;

  isLoadingResults: boolean = false;
  maxUsers:number
  maxSp:number
  @ViewChild(MatPaginator) paginator: MatPaginator;
  localStorage: any;
  constructor(public service: HttpService) {
    this.displayedColumns = ['number', 'version', 'date', 'text', 'action'];
  }

  ngOnInit() { 
    if (JSON.parse(localStorage.getItem('TradeMen-admin-data'))) {
      this.localStorage = JSON.parse(localStorage.getItem('TradeMen-admin-data'))
    }
  }

  ngAfterViewInit() {
    this.paginator.pageIndex = 0;
    this.tcList();
  }

  tcList() {

    this.isLoadingResults = true;

    this.tc$ = this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {
          let params = new HttpParams().set('offset', (((this.paginator.pageIndex + 1) * 10) - 10).toString());
          return this.service.getRequest(this.globals.urls.settings.termsCondition.list, params);
        }),
        map(
          res => {
            if (res.response === 200) {

              this.isLoadingResults = false;
              this.resultsLengthSp = res.data.spTMcount;
              this.resultsLengthUser = res.data.userTMcount;
              return res.data;

            }
          }),
        catchError(() => {

          this.isLoadingResults = false;
          return observableOf([]);

        })
      ).subscribe(
        data => {
          if (data) {
            this.dataSource = data;
          }
          else this.dataSource = new MatTableDataSource([]);
          this.maxUsers=Math.max.apply(Math, this.dataSource.updatedTermsCondition.users.map(function(o) { return o.termAndConditionVersion; }))
          this.maxSp=Math.max.apply(Math, this.dataSource.updatedTermsCondition.sp.map(function(o) { return o.termAndConditionVersion; }))
          return this.dataSource
        },
        err => {
          this.service.showError(err.error.message);
        }),

      this.dataSource.paginator = this.paginator;
  }

  ngOnDestroy(): void {
    if (this.tc$) this.tc$.unsubscribe();
  }

}


export interface termsConditionInterface {
  [index: number]: {
    number: number;
    version: number;
    date: number;
    text: string;
  }
}
