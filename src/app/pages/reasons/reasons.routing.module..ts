import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddEditReasonsComponent } from './add-edit-reasons/add-edit-reasons.component';
import { ConfigurationsHeaderComponent } from '../configurations/configurations-header/configurations-header.component';
import { RouterGuard } from '../../guards/route.guard';

const routes: Routes = [{
  path: '',
  component: ConfigurationsHeaderComponent,
  canActivate: [RouterGuard],
  data: { configurations: 1 },
  children: [
    {
      path: '',
      pathMatch: 'full',
      component: AddEditReasonsComponent
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReasonsRoutingModule { }
