import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddEditReasonsComponent } from './add-edit-reasons/add-edit-reasons.component';
import { ReasonsRoutingModule } from './reasons.routing.module.';
import { ConfigurationsModule } from '../configurations/configurations.module';
import { RouterModule } from '@angular/router';
import { MatExpansionModule, MatProgressSpinnerModule, MatCardModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ConfigurationsModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    FormsModule,
    MatCardModule,
    ReasonsRoutingModule
  ],
  declarations: [AddEditReasonsComponent]
})
export class ReasonsModule { }
