import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Globals } from 'src/app/Globals';
import { of as observableOf, Subscription } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';
import { HttpService } from 'src/app/services/http-service';

@Component({
  selector: 'app-add-edit-reasons',
  templateUrl: './add-edit-reasons.component.html',
  styleUrls: ['./add-edit-reasons.component.scss']
})
export class AddEditReasonsComponent implements OnInit {

  globals = Globals;

  // form
  model: any = {}
  isDisabled: boolean = true;
  typeOfReason: number = 0;

  reasons = [];
  userReasons = [];
  spReasons = [];

  reasons$: Subscription = null;

  isLoadingResults: boolean = true;
  localStorage: any;
  constructor(public service: HttpService, private route: ActivatedRoute, ) { }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem('TradeMen-admin-data'))) {
      this.localStorage = JSON.parse(localStorage.getItem('TradeMen-admin-data'))
  }

    this.getReasons();
  }

  // open new reason across respective panel
  openReason(typeOfReason) {

    if (typeOfReason == 1) {
      this.typeOfReason = typeOfReason
    } else if (typeOfReason == 2) {
      this.typeOfReason = typeOfReason
    }

  }

  // add new reason 
  addReason(reason, reasonET, reasonRU, type) {

    this.isLoadingResults = true;

    this.service.postRequest(this.globals.urls.settings.reasons.add, { reason: reason, reasonRU: reasonRU, reasonET: reasonET , userType: type === 1 ? 'user' : 'sp' }).subscribe(
      res => {
        if (res.response === 200) {

          this.service.showSuccess('Reason added successfully.', 'Reasons');
          this.typeOfReason = 0; // O means nothing, dont show any input fields for adding reason

          this.model.ucr = ''; // empty the adding reason model
          this.model.ucrET = ''; // empty the adding reason model
          this.model.ucrRU = ''; // empty the adding reason model
          this.model.wcr = '';
          this.model.wcrET = '';
          this.model.wcrRU = '';

          this.isLoadingResults = true;
          this.getReasons();

        } else if (res.response == 400) {

          this.service.showError(res['message'], 'Reasons');
          this.typeOfReason = 0; // O means nothing, dont show any input fields for adding reason
          this.isLoadingResults = true;

        }
      },
      err => {

        this.service.showError(err.error.message);
        this.isLoadingResults = true;

      },
    );
  }

  updateReason(event, userReason, type) {

    userReason.disable = true;
    this.isLoadingResults = true;

    this.service.putRequest(this.globals.urls.settings.reasons.update, { reason: userReason.reason, reasonRU: userReason.reasonRU, reasonET: userReason.reasonET , userType: type === 1 ? 'user' : 'sp', id: userReason._id, isDeleted: userReason.isDeleted })
    .subscribe(
      res => {
        if (res.response === 200) {

          if (userReason.status == 0) {
            this.service.showSuccess('Reason deleted successfully.', 'Reasons');
          } else {
            this.service.showSuccess('Reason updated successfully.', 'Reasons');
          }

          this.typeOfReason = 0 // O means nothing, dont show any input fields for adding reason
          this.isLoadingResults = true;
          this.getReasons();

        }
        else if (res.response == 400) {

          this.service.showError(res['message'], 'Reasons');
          this.typeOfReason = 0 // O means nothing, dont show any input fields for adding reason
          this.isLoadingResults = true;

        }
      },
      err => {
        this.service.showError(err.error.message);
        this.isLoadingResults = true;
      },
    );

  }

  getReasons() {

    this.reasons$ = this.route.paramMap.pipe(
      switchMap((urlParams: ParamMap) => {
        return this.service.getRequest(`${this.globals.urls.settings.reasons.get}`);
      }),
      map(reasonsData => {
        return reasonsData.data;
      }),
      catchError(() => {
        this.isLoadingResults = false;
        return observableOf([]);
      }))
      .subscribe(reasonsData => {

        this.reasons = reasonsData;

        this.userReasons = this.reasons.filter((user) => {
          return user.userType === 'user';
        })

        this.spReasons = this.reasons.filter((sp) => {
          return sp.userType === 'sp';
        })

        if (this.userReasons && this.userReasons.length > 0) {
          this.userReasons.forEach(function (obj) { obj.disable = true; });
        }

        if (this.spReasons && this.spReasons.length > 0) {
          this.spReasons.forEach(function (obj) { obj.disable = true; });
        }

        this.isLoadingResults = false;

      });

  }

  editFieldsEnable(event, userReason) {
    userReason.disable = false;
  }

}

