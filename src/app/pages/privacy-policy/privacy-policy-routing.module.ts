import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';
import { RouterGuard } from '../../guards/route.guard';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'list',
      component: ListComponent,
      canActivate: [RouterGuard],
      data: { privacyPolicy: 1 },
    },
    {
      path: 'edit/:id/:dead/:type',
      component: EditComponent,
      // canActivate: [RouterGuard],
      data: { 
        privacyPolicy: 1, 
        privacyPolicyView: 2 
      },
    },
    {
      path: '',
      pathMatch: 'full',
      component: ListComponent
    },
  ],
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrivacyPolicyRoutingModule { }
