import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrivacyPolicyRoutingModule } from './privacy-policy-routing.module';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';
import { FormsModule } from '@angular/forms';
import { MatTableModule, MatToolbarModule, MatProgressSpinnerModule, MatPaginatorModule, MatCardModule } from '@angular/material';
import { SharedPipesModule } from '../shared-pipes/shared-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatTableModule,
    MatToolbarModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatCardModule,
    SharedPipesModule,
    PrivacyPolicyRoutingModule
  ],
  declarations: [ListComponent, EditComponent]
})
export class PrivacyPolicyModule { }
