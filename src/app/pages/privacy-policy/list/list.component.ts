import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { of as observableOf, Subscription } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { HttpService } from 'src/app/services/http-service';
import { Globals } from 'src/app/Globals';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  globals = Globals;

  displayedColumns = [];
  resultsLength: number = 10;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  // dataSource = new MatTableDataSource<termsConditionInterface>([])
  dataSource:any=[];

  tc$: Subscription = null;

  isLoadingResults: boolean = false;
  maxUsers:number
  maxSp:number
  localStorage: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public service: HttpService) {
    this.displayedColumns = ['number', 'version', 'date', 'text', 'action'];
  }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem('TradeMen-admin-data'))) {
      this.localStorage = JSON.parse(localStorage.getItem('TradeMen-admin-data'))
    }
   }

  ngAfterViewInit() {
    this.paginator.pageIndex = 0;
    this.tcList();
  }

  tcList() {

    this.isLoadingResults = true;

    this.tc$ = this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {
          let params = new HttpParams().set('offset', (((this.paginator.pageIndex + 1) * 10) - 10).toString());
          return this.service.getRequest(this.globals.urls.settings.privacyPolicy.list, params);
        }),
        map(
          res => {
            if (res.response === 200) {

              this.isLoadingResults = false;
              this.resultsLength = res.data.count;
              return res.data;

            }
          }),
        catchError(() => {

          this.isLoadingResults = false;
          return observableOf([]);

        })
      ).subscribe(
        data => {
          if (data) {
            this.dataSource = data;
          }
          else this.dataSource = new MatTableDataSource([]);
          this.maxUsers=Math.max.apply(Math, this.dataSource.privacyPolicyObj.users.map(function(o) { return o.privacyPolicyVersion; }))
          this.maxSp=Math.max.apply(Math, this.dataSource.privacyPolicyObj.sp.map(function(o) { return o.privacyPolicyVersion; }))
          return this.dataSource;
        },
        err => {
          this.service.showError(err.error.message);
        }),

      this.dataSource.paginator = this.paginator;
  }

  ngOnDestroy(): void {
    if (this.tc$) this.tc$.unsubscribe();
  }

}


export interface termsConditionInterface {
  [index: number]: {
    number: number;
    version: number;
    date: number;
    text: string;
  }
}
