import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayoutsModalComponent } from './payouts-modal.component';

describe('PayoutsModalComponent', () => {
  let component: PayoutsModalComponent;
  let fixture: ComponentFixture<PayoutsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayoutsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayoutsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
