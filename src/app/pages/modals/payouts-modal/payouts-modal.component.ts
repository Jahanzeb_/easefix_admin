import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDatepicker } from '@angular/material';

@Component({
  selector: 'app-payouts-modal',
  templateUrl: './payouts-modal.component.html',
  styleUrls: ['./payouts-modal.component.scss']
})

export class PayoutsModalComponent {

  constructor(
    public dialogRef: MatDialogRef<PayoutsModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { }

  onNoClick(): void {
    this.dialogRef.close();
  }


}
