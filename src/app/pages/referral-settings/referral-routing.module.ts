import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReferralListComponent } from './referral-list/referral-list.component';
import { EditReferralComponent } from './edit-referral/edit-referral.component';
import { RouterGuard } from '../../guards/route.guard';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'edit/:id',
      component: EditReferralComponent,
      canActivate: [RouterGuard],
      data: { 
        referrals: 1, 
        referralsView: 2, 
      },
    },
    {
      path: 'users',
      component: ReferralListComponent,
      canActivate: [RouterGuard],
      data: {
        users: true,
        referrals:1
      }
    },
    {
      path: 'workers',
      component: ReferralListComponent,
      canActivate: [RouterGuard],
      data: {
        workers: true,
        referrals:1
      }
    },
    {
      path: '',
      pathMatch: 'full',
      redirectTo: 'users',
      canActivate: [RouterGuard],
      data: { referrals: 1 },
    },
  ],
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReferralRoutingModule { }
