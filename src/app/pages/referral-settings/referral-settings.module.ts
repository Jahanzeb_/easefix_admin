import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReferralListComponent } from './referral-list/referral-list.component';
import { EditReferralComponent } from './edit-referral/edit-referral.component';
import { ReferralRoutingModule } from './referral-routing.module';
import { MatProgressSpinnerModule, MatCardModule, MatTableModule,  MatPaginatorModule, MatToolbarModule, MatRadioModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    FormsModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatRadioModule,
    ReferralRoutingModule
  ],
  declarations: [ReferralListComponent, EditReferralComponent]
})
export class ReferralSettingsModule { }
