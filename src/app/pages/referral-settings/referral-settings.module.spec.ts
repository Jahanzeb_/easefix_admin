import { ReferralSettingsModule } from './referral-settings.module';

describe('ReferralSettingsModule', () => {
  let referralSettingsModule: ReferralSettingsModule;

  beforeEach(() => {
    referralSettingsModule = new ReferralSettingsModule();
  });

  it('should create an instance', () => {
    expect(referralSettingsModule).toBeTruthy();
  });
});
