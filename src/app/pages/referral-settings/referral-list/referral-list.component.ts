import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { map, switchMap, startWith, debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { Subscription, fromEvent } from 'rxjs';
import { HttpService } from '../../../services/http-service';
import { Globals } from '../../../Globals';

@Component({
  selector: 'app-referral-list',
  templateUrl: './referral-list.component.html',
  styleUrls: ['./referral-list.component.scss']
})
export class ReferralListComponent implements OnInit {
  // Table Settings
  // dataSource: referralInterface;
  // dataSource: MatTableDataSource<any>;
  displayedColumns = [];
  // dataSource: MatTableDataSource<any>;
  dataSource:any=[];
  private httpSub$: Subscription = null;
  isLoadingResults = false;
  totalUsers = 0;
  public globals = Globals;
  public translation: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('input') input: ElementRef;

  public users: Array<any> = [];

  constructor(public service: HttpService) { }

  //Set total records
  resultsLength: number = 10;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  inputString:string;
  dataToSend: any;
  userType:string='user';
  offset: number = 0;
  limit: number = 100;
  tabactive:boolean=true;
  totalRecords: number = 0;

  ngOnInit() {
    this.displayedColumns = ['number', 'photo','referredBy', 'referralId', 'referrals', 'referralsEarning','TotalJobs','completed','status'];

    // Fetch data for xcel sheet DRIVERS
    // this.getUsersExportsData(this.offset);

  }
  ngAfterViewInit() {

    this.httpSub$ = fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.inputString=this.input.nativeElement.value;
          this.getRefferals({ searchText: this.input.nativeElement.value });
        })
      )
      .subscribe();

      this.getRefferals({ searchText: this.input.nativeElement.value });
  }


  search(input) {
    this.isLoadingResults = true;
    let offset = ((this.paginator.pageIndex + 1) * this.paginator.pageSize) - this.paginator.pageSize;
    let limit = this.paginator.pageSize;
    let spListUrl=this.globals.urls.TradeAdmin.users.fetchUsersList;
    spListUrl = spListUrl.replace(':limit', limit.toString());
    spListUrl = spListUrl.replace(':offset', offset.toString());
    spListUrl = spListUrl.replace(':search_text',  input.searchText || this.inputString || '');
    spListUrl = spListUrl.replace(':status', input.statusType||'');
    spListUrl = spListUrl.replace(':jobTypeId','');
    this.service.getRequest(spListUrl)
      .pipe(
        map(res => {
          return res.data;
        })
      )
      .subscribe(
        data => {
          this.isLoadingResults = false;
          this.dataSource = data.users;
          this.totalRecords = data.users.length;
          this.resultsLength = data.count;
        },
        err => {
          this.isLoadingResults = false;
          this.service.showError(err.error.message);
        },
      )
  }
  switchType(type){
    this.userType=type;
    if(type== 'user'){
      this.tabactive=true;
    }
    else{
      this.tabactive=false;
    }
    this.getRefferals('');
  }

  getRefferals(input) {
    this.isLoadingResults = true;
    this.httpSub$ = this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {
          // let params = {
          //   offset: ((this.paginator.pageIndex + 1) * this.paginator.pageSize) - this.paginator.pageSize,
          //   limit: this.paginator.pageSize,
          //   searchText: this.input.nativeElement.value || null
          // };
          this.isLoadingResults = true;
          let offset = ((this.paginator.pageIndex + 1) * this.paginator.pageSize) - this.paginator.pageSize;
          let limit = this.paginator.pageSize;
          let spListUrl=this.globals.urls.TradeAdmin.Referrals.getReferrals
          spListUrl = spListUrl.replace(':limit', limit.toString());
          spListUrl = spListUrl.replace(':offset', offset.toString());
          spListUrl = spListUrl.replace(':search_text',  input.searchText || this.inputString || '');
          // spListUrl = spListUrl.replace(':status','');
          spListUrl = spListUrl.replace(':userType',this.userType);
          return this.service.getRequest(spListUrl);
        }),
        map(res => {
          return res.data;
        }),
      ).subscribe(
        data => {
          this.totalUsers = data.count;
          this.isLoadingResults = false;
          this.resultsLength = data.count;
          this.dataSource = data.userAccount ;
          this.totalRecords = data.userAccount.length;
        },
        err => {
          this.isLoadingResults = false;
          this.service.showError(err.error.message);
        }
      );
  }


  ngOnDestroy(): void {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }



// export interface referralInterface {
//   [index: number]: {
//     number: number;
//     photo: string;
//     name: string;
//     referralId: string;
//     referrals: number;
//     referralsEarning: number;
//   }
// }
}