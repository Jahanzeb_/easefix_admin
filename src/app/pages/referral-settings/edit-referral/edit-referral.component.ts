import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Globals } from '../../../Globals';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpService } from 'src/app/services/http-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-referral',
  templateUrl: './edit-referral.component.html',
  styleUrls: ['./edit-referral.component.scss']
})
export class EditReferralComponent implements OnInit, AfterViewInit {

  model: any = {};

  constructor(private service: HttpService, private router: Router) {
    this.model = {
      minSpJobToAvail: 0,
      minUserJobToAvail: 0,
      spReferralAmount: 0,
      userReferralAmount: 0
    }
  }
  public globals = Globals;
  isLoadingResults: boolean = false;

  Referral$: Subscription = null;
  ngOnInit() {
    // this.getReferralData();
  }

  ngAfterViewInit() {
    this.getReferralData();
  }

  addNewPeakHr(some) {
  }

  getReferralData() {
    this.isLoadingResults = true;
    this.Referral$ = this.service.getRequest(Globals.urls.TradeAdmin.Referrals.getSettingsData, {})
      .pipe(
        map(res => res.data),
    ).subscribe(
      data => {
        this.model = data;
        this.isLoadingResults = false;

      },
      err => {
        this.model = null;
        this.isLoadingResults = false;
        this.service.showError(err.error.message);
      }
    )
  }

  Save(form) {
    this.isLoadingResults = true;
    this.Referral$ = this.service.putRequest(Globals.urls.TradeAdmin.Referrals.setSettingsData, this.model)
      .pipe(
        map(res => res),
    ).subscribe(
      data => {
        this.isLoadingResults = false;
       this.service.showSuccess(data.message,'Referrals Updates')
       this.router.navigate(['/referral/users']);
      },
      err => {
        this.model = null;
        this.isLoadingResults = false;
        this.service.showError(err.error.message);
      }
    )
  }

}
