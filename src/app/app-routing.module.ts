
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from './pages/login/login.component';
import { LogoutComponent } from './pages/logout/logout.component';
import { PreloadSelectedModulesList } from './services/preload_selected_modules_list';
// import { AuthGuard } from './guards/auth-guard.service';

const routes: Routes = [

  // place: canActivate: [AuthGuard] when connecting full app
  { path: '', loadChildren: './pages/pages.module#PagesModule' },
  // { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  {
    path: 'password',
    loadChildren: './pages/password/password.module#PasswordModule',
  },
  { path: '**', redirectTo: 'dashboard' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { initialNavigation: 'enabled' })],  //initialNavigation: 'enabled' for removing flick
  exports: [RouterModule],
})
export class AppRoutingModule {
}
