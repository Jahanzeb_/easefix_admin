import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({ name: 'dateTimeFormatIsoPipe' })

// convert ISO into date time format
export class dateTimeFormatIsoPipe implements PipeTransform {
    transform(value): string {
        return moment(value).format('hh:mm A, DD MMMM, YYYY ');
    }
}