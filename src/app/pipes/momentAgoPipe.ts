
import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({ name: 'momentAgoPipe' })
export class momentAgoPipe implements PipeTransform {
  transform(value): string {
    return moment(value).fromNow();
  }
}