import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({ name: 'dateFormatIsoPipe' })

// convert ISO into date format
export class dateFormatIsoPipe implements PipeTransform {
    transform(value): string {
        return moment(value).format('DD-MM-YYYY');
    }
}