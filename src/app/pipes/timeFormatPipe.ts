import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({ name: 'timeFormatPipe' })
export class timeFormatPipe implements PipeTransform {
    transform(): string {
        return moment().format('hh:mm A');
    }
}