import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({ name: 'dateTimeFormatPipe' })
export class dateTimeFormatPipe implements PipeTransform {
    transform(value: number): string {
        return moment(value * 1000).format('hh:mm A, DD MMMM, YYYY ');
    }
}

