import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({ name: 'dateFormatPipe' })
export class dateFormatPipe implements PipeTransform {
    transform(): string {
        return moment().format('DD-MM-YYYY');
    }
}