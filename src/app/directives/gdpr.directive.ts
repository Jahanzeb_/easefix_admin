// import { Directive } from '@angular/core';
import { Directive, ElementRef, Input,Output,Renderer2, OnInit,EventEmitter } from '@angular/core';
import { HttpService } from 'src/app/services/http-service';
import { Subscription } from 'rxjs';
import { Globals } from '../Globals';
// import { Globals } from '../../../../Globals';
@Directive({
  selector: '[appGdpr]'
})
export class GdprDirective implements OnInit {
  globals = Globals;
  // Observables/Subscriptions
httpSub$: Subscription = null;
  @Input() dataModel: any;
  @Output() returnedData:EventEmitter <any> = new EventEmitter();
  constructor(private httpService: HttpService,) { }


  ngOnInit() {
     console.log(this.dataModel)
    // let shadowStr = `${ this.appShadowX } ${ this.appShadowY } ${ this.appShadowBlur } ${ this.appShadow }`;
    // this.renderer.setStyle(this.elem.nativeElement, 'box-shadow', shadowStr);
    this.httpSub$ =this.httpService.postRequest(this.globals.urls.gdprLoging,this.dataModel)
    .subscribe(
      res => {
        // this.previewData=res.data
        console.log(res);
        this.returnedData.emit({ action: 'gdprDataReturned', data:res.data});
      },
      err => {
        // this.isLoadingResults = false;
        this.httpService.showError(err);
      }
    );
    // console.log(this.dataModel)
    // this.returnedData.emit({ action: 'gdprDataReturned', data:this.dataModel });
  }

}
