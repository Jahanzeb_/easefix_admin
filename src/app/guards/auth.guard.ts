import { Injectable } from "@angular/core";

import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";

import { Observable, of } from "rxjs";
import { Globals } from "../Globals";
import { HttpService } from "../services/http-service";
import { map, catchError } from "rxjs/operators";

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private httpService: HttpService, private _router: Router) { }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        const user= JSON.parse(localStorage.getItem('TradeMen-admin-data'));
        // const token= JSON.parse(sessionStorage.getItem('Aha-User-data')).spId;
        if(user){
            return true;
        }
        else{
        return this.httpService.getRequest(Globals.urls.authentication.getCurrentUser)
            .pipe(
                map(user => {
                    if (user && user['response'] === 200 && user['data']) {
                        localStorage.setItem('TradeMen-admin-data', JSON.stringify(user['data']));
                        return true;
                    } else {
                        this._router.navigate(['/login']);
                        return false;
                    }
                }),
                catchError(() => {
                    this._router.navigate(['/login']);
                    return of(false);
                })
            );
        }
    }
}