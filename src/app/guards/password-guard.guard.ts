import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { Observable, of } from "rxjs";
import { Globals } from "../Globals";
import { HttpService } from "../services/http-service";
import { map, catchError } from "rxjs/operators";

@Injectable()
export class PasswordGuardGuard implements CanActivate {
  constructor(private httpService: HttpService, private _router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.httpService.getRequest(Globals.urls.authentication.getCurrentUser, {} , false)
      .pipe(
        map(user => {
          if (user && user['response'] === 200 && user['data'].account) {
            // if user is already logged in dont show him forgot reset page
            this._router.navigate(['/dashboard']);
            return false;
          } else {
            return true;
          } 
        }),
        catchError(() => {
          // if user is not logged in show him forget or reset password page
          return of(true);
        })
      );
  }
}
